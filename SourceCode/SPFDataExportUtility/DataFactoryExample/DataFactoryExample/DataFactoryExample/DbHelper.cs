﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace DataFactoryExample
{
    public static class DbHelper
    {

        public static DataSet ReadDataFromOracle(string connectionString, string queryString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand command = new OracleCommand(queryString, connection);
                OracleDataAdapter da = new OracleDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

        public static DataSet ReadDataFromSql(string connectionString, string queryString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
        }

        public static void SetupForSql(string connectionString, string queryString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.ExecuteNonQuery();
            }
        }

        public static void SetupForOracle(string connectionString, string queryString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                OracleCommand command = new OracleCommand(queryString, connection);
                command.ExecuteNonQuery();
            }
        }

        public static string FormConnectionStringForOracle(string userName,string password,string DataSourceOrTNSName)
        {

          return  string.Format("User ID = {0}; Password = {1}; Data Source = {2};", userName, password, DataSourceOrTNSName);
            
        }

        public static string FormConnectionStringForSql(string dataSource, string initialCatalog)
        {

            return string.Format("Data Source ={0}; Initial Catalog = {1}; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False", dataSource, initialCatalog);

        }

    }
}
