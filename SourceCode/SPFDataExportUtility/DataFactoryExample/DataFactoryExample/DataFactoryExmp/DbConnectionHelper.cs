﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;

namespace DataFactoryExmp
{
   public static class DbConnectionHelper
    {
      public  static DbConnection CreateDbConnection(
          string providerName, string connectionString)
        {
            // Assume failure.
            DbConnection connection = null;
            DbProviderFactory factory = null;

            DataSet ds = new DataSet();

            // Create the DbProviderFactory and DbConnection.
            if (connectionString != null)
            {
                try
                {
                    var test = DbProviderFactories.GetFactoryClasses();
                     factory =
                        DbProviderFactories.GetFactory(providerName);

                    connection = factory.CreateConnection();
                    connection.ConnectionString = connectionString;
                }
                catch (Exception ex)
                {
                    // Set the connection to null if it was created.
                    if (connection != null)
                    {
                        connection = null;
                    }
                    Console.WriteLine(ex.Message);
                }
            }









            using (DbCommand cmd = connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "select * from " + "PROJ";
                cmd.CommandTimeout = 10;
                using (DbDataAdapter da = factory.CreateDataAdapter())
                {
                    
                    da.SelectCommand = cmd;
                    da.Fill(ds);
                }
            }

            // Return the connection.
            return connection;
        }
    }
}
