﻿using DataFactoryExample;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBExecuteHelper
{
    public enum DataBaseType
    {
        SQLServer,
        Oracle

    }

    public enum DBOperationType
    {
        QueryData,
        InsertOrUpdate

    }
    public class DBExecuteHelper
    {

        public DbType dbType { get; set; }

        public DBOperationType DBOperationType { get; set; }

        public string userName { get; set; }

        public string password { get; set; }

        public string dataSourceORCL { get; set; }

        public string dataSourceForSQL { get; set; }

        public string initialCatalog { get; set; }

        public DbConnection SourceConn { get; set; }
        public DbConnection TargetConn { get; set; }

        public DBExecuteHelper()
        {

        }

        internal void InsertProgressStatus(DataBaseType oracle, DbConnection oraTargetConn, string strREGISTEREDTOOLS, string strPUBLISHEDOCUMENTS, string fAILEDDOCUMENTS, string strPUBDOCSWITHNOVIEWFILE, string strNOOPENDOCUMENTS, string strProject, string strDelivery)
        {
            if (oracle == DataBaseType.Oracle)
            {
                DbHelper.InsertProgressStatusORCL((Oracle.DataAccess.Client.OracleConnection)oraTargetConn,  strREGISTEREDTOOLS,  strPUBLISHEDOCUMENTS,  fAILEDDOCUMENTS,  strPUBDOCSWITHNOVIEWFILE,  strNOOPENDOCUMENTS,  strProject,  strDelivery); 
            }
            if (oracle == DataBaseType.SQLServer)
            {

                DbHelper.InsertProgressStatusSql((SqlConnection)oraTargetConn,  strREGISTEREDTOOLS,  strPUBLISHEDOCUMENTS,  fAILEDDOCUMENTS,  strPUBDOCSWITHNOVIEWFILE,  strNOOPENDOCUMENTS,  strProject,  strDelivery); 

            }

            
        }

        //static void Main(string[] args)
        //{


        //    var dbType = DataBaseType.Oracle;
        //    var dBOperationType = DBOperationType.QueryData;
        //    string userName = "ERM";
        //    string password = "ERM";
        //    string dataSource = "hyd-std17-servicepack";
        //    string dataSourceForSQL = @"(localdb)\MSSQLLocalDB";
        //    string initialCatalog = "OdeToFood";
        //    string query = "SELECT * FROm TAB";



        //    if(dbType == DataBaseType.Oracle)
        //    {
        //        if(userName==null || password==null|| dataSource==null)
        //        {
        //            Console.WriteLine("Oracle Connection string not properly provided");
        //        }
        //    }
        //    if (dbType == DataBaseType.SQLServer)
        //    {
        //        if (dataSourceForSQL == null || initialCatalog == null )
        //        {
        //            Console.WriteLine("SQL Connection string not properly provided");
        //        }
        //    }

        //    var result = ExecuteQuery(dbType, dBOperationType, query, userName, password, dataSource, null, null);
        //    var result1 = ExecuteQuery(dbType, dBOperationType, query, null, null, null, dataSourceForSQL, initialCatalog);





        //    //   string queryString = "SELECT proj_no,proj_ID,descr FROM PROJ";
        //    //var ds= DbHelper.ReadDataFromOracle("User ID = ERM; Password = ERM; Data Source = hyd-std17-servicepack;", queryString);


        //    //var sqlConn = @"Data Source = (localdb)\MSSQLLocalDB; Initial Catalog = OdeToFood; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False";
        //    var oracleconn = "User ID = ERM; Password = ERM; Data Source = hyd-std17-servicepack;";
        //    //string sqlQueryString = "SELECT * FROM dbo.Restaurants";

        //    //var ds1 = DbHelper.ReadDataFromSql(sqlConn, sqlQueryString);
        //    //string sqlQueryStringforSetup = "CREATE TABLE dept( deptno int not null primary key, dname varchar(50) not null,location varchar(50) not null) ";
        //    //DbHelper.SetupForSql(sqlConn, sqlQueryStringforSetup);


        //    //string insertsql = "insert into dept values (10,'Accounting','New York')";
        //    //DbHelper.SetupForSql(sqlConn, insertsql);

        //    string sqlQueryStringforSetup = "CREATE TABLE dept_test( deptno int not null primary key, dname varchar(50) not null,location varchar(50) not null) ";
        //    DbHelper.SetupForOracle(oracleconn, sqlQueryStringforSetup);


        //    string insertsql = "insert into dept_test values (10,'Accounting','New York')";
        //    DbHelper.SetupForOracle(oracleconn, insertsql);
        //    Console.WriteLine("OK");

        //}

        public void ExportSchemaObject(DataBaseType dbType, string strDELIVERYID, string strPROJECT, string strQuery, string strTable, string strClassification, DbConnection oraSourceConn, DbConnection oraTargetConn, string gstrFileName)
        {
            if(dbType==DataBaseType.Oracle)
            {
                DbHelper.ExportSchemaObjectORCL(strDELIVERYID, strPROJECT,  strQuery,  strTable,  strClassification, ( Oracle.DataAccess.Client.OracleConnection)oraSourceConn, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn,  gstrFileName);
            }
            if (dbType == DataBaseType.SQLServer)
            {
                
                    DbHelper.ExportSchemaObjectSqlChanged(strDELIVERYID, strPROJECT, strQuery, strTable, strClassification, (SqlConnection)oraSourceConn, (SqlConnection)oraTargetConn, gstrFileName);
                //DbHelper.ExportSchemaObjectSql(strDELIVERYID, strPROJECT, strQuery, strTable, strClassification, (SqlConnection)oraSourceConn, (SqlConnection)oraTargetConn, gstrFileName);
            }
        }
         

        public double CountOfSQLQueryResult(DataBaseType dbType, string strSQL, DbConnection SourceConn)
        {
            if (dbType == DataBaseType.Oracle)
            {
                return DbHelper.CountOfSQLQueryResultORCL(strSQL, (Oracle.DataAccess.Client.OracleConnection)SourceConn);
            }
            if (dbType == DataBaseType.SQLServer)
            {
                return DbHelper.CountOfSQLQueryResultSql(strSQL, (SqlConnection)SourceConn);
            }
            return 0;
        }
        public static DataSet ExecuteQuery(DataBaseType dbType,DBOperationType dBOperationType, string query, string userNameForOrcl=null, string passwordForOrcl=null, string dataSourceForOrcl=null, string dataSourceForSQL=null,string initialCatalogForSQL=null)
        {
            string ConnectionString;
            switch (dbType)
            {

                case DataBaseType.Oracle:
                    {
                        ConnectionString = DbHelper.FormConnectionStringForOracle(userNameForOrcl, passwordForOrcl, dataSourceForOrcl);
                        if (ConnectionString != null)
                        {
                            switch (dBOperationType)
                            {
                                case DBOperationType.QueryData:
                                    {
                                        return DbHelper.ReadDataFromOracle(ConnectionString, query);
                                        
                                    }

                                case DBOperationType.InsertOrUpdate:
                                    {
                                        DbHelper.SetupForOracle(ConnectionString, query);
                                        return null;                                       

                                    }
                                default:
                                    {
                                        return null;                                      
                                    }
                            }

                        }
                        break;
                    }

                case DataBaseType.SQLServer:
                    {
                        ConnectionString = DbHelper.FormConnectionStringForSql(dataSourceForSQL, initialCatalogForSQL);
                        if (ConnectionString != null)
                        {
                            switch (dBOperationType)
                            {
                                case DBOperationType.QueryData:
                                    {
                                        return DbHelper.ReadDataFromSql(ConnectionString, query);
                                       
                                    }

                                case DBOperationType.InsertOrUpdate:
                                    {
                                        DbHelper.SetupForSql(ConnectionString, query);
                                        return null;                                     

                                    }
                                default:
                                    {
                                        return null;
                                      
                                    }
                            }

                        }
                        break;
                    }

                default:
                    {
                        break;
                    }

            }
            return null;
        }
        public  DataSet QueryData(DataBaseType databasetype, DbConnection targetConn, string StrQuery)
        {
            if (databasetype == DataBaseType.Oracle)
            {
                return DbHelper.ReadDataFromOracleWithActiveConnection( (Oracle.DataAccess.Client.OracleConnection)targetConn,StrQuery);
            }
            if (databasetype == DataBaseType.SQLServer)
            {
                return DbHelper.ReadDataFromSqlWithActiveConnection((SqlConnection)targetConn,StrQuery);
            }
           

          
            return null;
        }

        internal string CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn(DataBaseType databasetype, DbConnection oraTargetConn, string strProjectName, string strDELIVERYID, string strSO_CDW)
        {
            if (databasetype == DataBaseType.Oracle)
            {
                return DbHelper.CreateCorrelatedTablesSQL_DistinctTagNameForOracle(strProjectName,  strDELIVERYID,  strSO_CDW, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn);
            }
           else
            {
                return DbHelper.CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn( strProjectName,  strDELIVERYID,  strSO_CDW, (SqlConnection)oraTargetConn);
            }
        }

        internal string InsertCorrelatedTablesSQL_DistinctTagName(DataBaseType databasetype, DbConnection oraTargetConn, string strProjectName, string strDELIVERYID, string strSO_CDW)
        {
            if (databasetype == DataBaseType.Oracle)
            {
                return DbHelper.InsertCorrelatedTablesSQL_DistinctTagNameForOracle(strProjectName, strDELIVERYID, strSO_CDW, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn);
            }
            else
            {
                return DbHelper.InsertCorrelatedTablesSQL_DistinctTagNameForSQLSERVER(strProjectName, strDELIVERYID, strSO_CDW, (SqlConnection)oraTargetConn);
            }
        }

        public bool TableExists(DataBaseType databasetype, DbConnection oraTargetConn, string strTable)
        {
           if(databasetype==DataBaseType.Oracle)
            {
             return   DbHelper.OracleTableExists(strTable, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn);
            }
            if (databasetype == DataBaseType.SQLServer)
            {
                return DbHelper.SqlTableExists(strTable, (SqlConnection)oraTargetConn);
            }
            return false;

        }

        internal string CreateSOAndCDWInconsistencyTableSQL(DataBaseType sQLServer, DbConnection sqlTargetConn, string strProjectName, string strDELIVERYID,  bool isCdw, string strDynTable)
        {
            if (sQLServer == DataBaseType.Oracle)
            {
                return DbHelper.CreateSOAndCDWInconsistencyTableSQLForOracle((OracleConnection)sqlTargetConn, strProjectName, strDELIVERYID,  isCdw, strDynTable);

            }
            else
            {
                return DbHelper.CreateSOAndCDWInconsistencyTableSQLForSqlServer((SqlConnection)sqlTargetConn, strProjectName, strDELIVERYID,  isCdw, strDynTable);

            }
        }

        internal void InsertSOAndCDWInconsistencyTableSQL(DataBaseType sQLServer, DbConnection sqlTargetConn, string strDynTable, string strProjectName, string strDELIVERYID, bool iscdw)
        {
            if (sQLServer == DataBaseType.Oracle)
            {
                 DbHelper.InsertSOAndCDWInconsistencyTableForOracle((OracleConnection)sqlTargetConn, strDynTable, strProjectName, strDELIVERYID,iscdw);

            }
            else
            {
                 DbHelper.InsertSOAndCDWInconsistencyTableFOrSQLServer((SqlConnection)sqlTargetConn, strDynTable, strProjectName, strDELIVERYID,iscdw);

            }
        }

        public bool DataExists(DataBaseType databasetype, DbConnection oraTargetConn, string StrQuery)
        {
            if (databasetype == DataBaseType.Oracle)
            {
                return DbHelper.OracleDataExists(StrQuery, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn);
            }
            if (databasetype == DataBaseType.SQLServer)
            {
                return DbHelper.SqlDataExists(StrQuery, (SqlConnection)oraTargetConn);
            }
            return false;

        }
        public void InsertData(DataBaseType databasetype,string strSQL, DbConnection targetConn)
        {
            if (databasetype == DataBaseType.Oracle)
            {
                DbHelper.ExecuteQueryOracle(strSQL, (Oracle.DataAccess.Client.OracleConnection)targetConn);
            }
            if (databasetype == DataBaseType.SQLServer )
            {
                DbHelper.ExecuteQuerySql(strSQL, (SqlConnection)targetConn);
            }
        }

        public double ProgressStatus(DataBaseType databasetype, string strSQL, bool blnTarget, DbConnection oraSourceConn, DbConnection oraTargetConn)
        {
            if (databasetype == DataBaseType.Oracle)
            {
                return DbHelper.ProgressStatusORCL(strSQL, blnTarget, (Oracle.DataAccess.Client.OracleConnection)oraSourceConn, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn);
            }
            return 0;
        }

        public string GetPlant(DataBaseType databasetype, string strProject, string strDelivery, DbConnection oraTargetConn)
        {
            if (databasetype == DataBaseType.Oracle)
            {                
                return DbHelper.GetPlantORCL(strProject, strDelivery, (Oracle.DataAccess.Client.OracleConnection)oraTargetConn);
            }
            if (databasetype == DataBaseType.SQLServer)
            {
                return DbHelper.GetPlantSql(strProject, strDelivery, (SqlConnection)oraTargetConn);
            }
            return string.Empty;
        }
    }
}
