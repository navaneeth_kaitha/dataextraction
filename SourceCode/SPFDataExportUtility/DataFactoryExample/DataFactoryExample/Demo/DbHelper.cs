﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace DataFactoryExample
{
    public static class DbHelper
    {
        public static DataSet ReadDataFromOracle(string connectionString, string queryString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                OracleCommand command = new OracleCommand(queryString, connection);
                OracleDataAdapter da = new OracleDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }
        public static DataSet ReadDataFromOracleWithActiveConnection(OracleConnection connection, string queryString)
        {

            OracleCommand command = new OracleCommand(queryString, connection);
            OracleDataAdapter da = new OracleDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;


        }

        public static bool OracleTableExists(string strTable, OracleConnection oarconn) // schemaobjects
        {
            using (OracleCommand cmdProj = new OracleCommand())
            {
                string strSQL;
                if(oarconn.State==ConnectionState.Closed)
                {
                    oarconn.Open();
                }
                cmdProj.Connection = oarconn;
                strSQL = "select tname from tab where upper(tname)='" + strTable.ToUpper().Trim() + "'";
                cmdProj.CommandText = strSQL;
                OracleDataReader dReader = cmdProj.ExecuteReader();
                if (dReader.HasRows)
                {
                    oarconn.Close();
                    return true;
                }                   
                    
                else
                {
                    oarconn.Close();
                    return false;

                }
                   

            }

        }

        internal static void InsertProgressStatusSql(SqlConnection oraTargetConn, string strREGISTEREDTOOLS, string strPUBLISHEDOCUMENTS, string fAILEDDOCUMENTS, string strPUBDOCSWITHNOVIEWFILE, string strNOOPENDOCUMENTS, string strProject, string strDelivery)
        {
            using (SqlCommand cmdProj = new SqlCommand())
            {
                string strSQL;
                oraTargetConn.Open();

                cmdProj.Connection = oraTargetConn;
                strSQL = @" insert into PROJECTDATAPROGRESS
(REGISTEREDTOOLS,PUBLISHEDOCUMENTS,FAILEDDOCUMENTS,PUBDOCSWITHNOVIEWFILE,NOOPENDOCUMENTS,zadproject,deliveryid)
values
('" + strREGISTEREDTOOLS + "','" + strPUBLISHEDOCUMENTS + "','" + fAILEDDOCUMENTS + "','" + strPUBDOCSWITHNOVIEWFILE + "','" + strNOOPENDOCUMENTS + "','" + strProject + "','" + strDelivery + "')";
                cmdProj.CommandText = strSQL;

                cmdProj.ExecuteNonQuery();
                oraTargetConn.Close();

            }
        }

        internal static void InsertProgressStatusORCL(OracleConnection oraTargetConn, string strREGISTEREDTOOLS, string strPUBLISHEDOCUMENTS, string fAILEDDOCUMENTS, string strPUBDOCSWITHNOVIEWFILE, string strNOOPENDOCUMENTS, string strProject, string strDelivery)
        {

            using (OracleCommand cmdProj = new OracleCommand())
            {
                string strSQL;
                oraTargetConn.Open();

                cmdProj.Connection = oraTargetConn;
                strSQL = @" insert into PROJECTDATAPROGRESS
(REGISTEREDTOOLS,PUBLISHEDOCUMENTS,FAILEDDOCUMENTS,PUBDOCSWITHNOVIEWFILE,NOOPENDOCUMENTS,zadproject,deliveryid)
values
('" + strREGISTEREDTOOLS + "','" + strPUBLISHEDOCUMENTS + "','" + fAILEDDOCUMENTS + "','" + strPUBDOCSWITHNOVIEWFILE + "','" + strNOOPENDOCUMENTS + "','" + strProject + "','" + strDelivery + "')";
                cmdProj.CommandText = strSQL;

                cmdProj.ExecuteNonQuery();
                oraTargetConn.Close();

            }

        }

        public static bool SqlTableExists(string strTable, SqlConnection oarconn) // schemaobjects
        {
            try
            {
                using (SqlCommand cmdProj = new SqlCommand())
                {
                    string strSQL;
                    oarconn.Open();
                    cmdProj.Connection = oarconn;
                    strSQL = "SELECT TABLE_NAME FROM information_schema.tables  where upper(TABLE_NAME)='" + strTable.ToUpper().Trim() + "'";
                    cmdProj.CommandText = strSQL;
                    SqlDataReader dReader = cmdProj.ExecuteReader();
                    if (dReader.HasRows)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

            finally
            {
                oarconn.Close();

            }



        }
        public static bool OracleDataExists(string strQuery, OracleConnection oarconn) // schemaobjects
        {
            using (OracleCommand cmdProj = new OracleCommand())
            {
                string strSQL;
                cmdProj.Connection = oarconn;
                strSQL = strQuery.Trim();
                cmdProj.CommandText = strSQL;
                if (oarconn.State==ConnectionState.Closed )
                {
                    oarconn.Open();
                }

                OracleDataReader dReader = cmdProj.ExecuteReader();
                if (dReader.HasRows)
                    return true;
                else
                    return false;

            }

        }
        public static bool SqlDataExists(string strQuery, SqlConnection oarconn) // schemaobjects
        {
            try
            {
                using (SqlCommand cmdProj = new SqlCommand())
                {
                    string strSQL;
                    oarconn.Open();
                    cmdProj.Connection = oarconn;
                    strSQL = strQuery.Trim();
                    cmdProj.CommandText = strSQL;
                    SqlDataReader dReader = cmdProj.ExecuteReader();
                    if (dReader.HasRows)
                        return true;
                    else
                        return false;

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                oarconn.Close();
            }

        }

        public static double CountOfSQLQueryResultORCL(string strSQL, OracleConnection oraSourceConn)  // schemaobjects
        {
            using (var cmdProj = new OracleCommand())
            {
                cmdProj.Connection = oraSourceConn;
                strSQL = "select count (*) from (" + strSQL + ")";
                cmdProj.CommandText = strSQL;
                double dblRowCount = (double)cmdProj.ExecuteScalar();
                return dblRowCount;
            }

        }
        public static double CountOfSQLQueryResultSql(string strSQL, SqlConnection sourceConn)  // schemaobjects
        {
            try
            {
                using (var cmdProj = new SqlCommand())
                {
                    sourceConn.Open();
                    cmdProj.Connection = sourceConn;
                    strSQL = "select count (*) from (" + strSQL + ")";
                    cmdProj.CommandText = strSQL;
                    double dblRowCount = (double)cmdProj.ExecuteScalar();
                    return dblRowCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                sourceConn.Close();
            }


        }

        public static void ExportSchemaObjectORCL(string strDELIVERYID, string strPROJECT, string strQuery, string strTable, string strClassification, OracleConnection oraSourceConn, OracleConnection oraTargetConn, string gstrFileName)
        {
            OracleCommandBuilder cmdTargetBldr;

            OracleCommand cmdSource = new OracleCommand();
            OracleDataReader drSource;
            string strSQLSource = "";
            string strTempSQLSource = "";


            strSQLSource = strQuery;
            cmdSource.Connection = oraSourceConn;

            strSQLSource = strQuery;

            cmdSource.CommandText = strSQLSource;

            OracleDataAdapter objSourceOracleDataAdapter = new OracleDataAdapter();
            DataTable objSourceDatatable = new DataTable();
            objSourceOracleDataAdapter.SelectCommand = cmdSource;
            objSourceOracleDataAdapter.Fill(objSourceDatatable);

            drSource = cmdSource.ExecuteReader();


            OracleDataAdapter objTargetAdapter;
            DataSet objTargetDS;
            OracleCommand cmdTarget = new OracleCommand();
            DataRow dataRowTarget;

            cmdTarget.Connection = oraTargetConn;

            objTargetAdapter = new OracleDataAdapter();
            cmdTarget.CommandText = "select * from " + strTable;
            objTargetAdapter.SelectCommand = cmdTarget;
            // 

            DataTable dt = new DataTable(strTable);

            int iBatchUpdate = 0;
            for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
            {
                DataColumn colOBID = new DataColumn();
                if (drSource.GetName(iColCounter) == "TAGCREATIONDATE")
                {
                    DataColumn colCreationDate = new DataColumn();
                    colCreationDate.DataType = typeof(DateTime);
                    colCreationDate.ColumnName = "TAGCREATIONDATE";
                    dt.Columns.Add(colCreationDate);
                }
                else
                {
                    colOBID.DataType = drSource.GetName(iColCounter).GetType();
                    colOBID.ColumnName = drSource.GetName(iColCounter);
                    dt.Columns.Add(colOBID);
                }
            }
            DataColumn colDelivery = new DataColumn();
            colDelivery.DataType = typeof(string);
            colDelivery.ColumnName = "DELIVERYID";
            dt.Columns.Add(colDelivery);
            DataColumn colProject = new DataColumn();
            colProject.DataType = typeof(string);
            colProject.ColumnName = "PROJECTNAME";
            dt.Columns.Add(colProject);
            colProject = new DataColumn();
            colProject.DataType = typeof(DateTime);
            colProject.ColumnName = "ZADDATESTAMP";
            dt.Columns.Add(colProject);

            DataColumn colFileExistsInVault = new DataColumn();
            colFileExistsInVault.DataType = typeof(string);
            colFileExistsInVault.ColumnName = "FileExistsInVault";
            dt.Columns.Add(colFileExistsInVault);

            string strFieldName;
            double intRow = 0;
            double iRowSize = 0;
            iRowSize = drSource.RowSize;
            while (drSource.Read())
            {
                dataRowTarget = dt.NewRow();


                for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                {
                    strFieldName = drSource.GetName(iColCounter);
                    if (strFieldName == "TAGCREATIONDATE")
                    {
                        string strCreationdate = "";
                        strCreationdate = drSource[strFieldName].ToString().Substring(0, 10);
                        dataRowTarget["TAGCREATIONDATE"] = strCreationdate; // 
                    }
                    else if (strFieldName == "FileExistsInVault")
                        dataRowTarget[strFieldName] = "False";
                    else if (drSource[strFieldName] != null)
                        dataRowTarget[strFieldName] = drSource[strFieldName];
                }

                if (strTable.ToUpper() == "SPFVIEWFILES" | strTable.ToUpper() == "FilesNotInVault".ToUpper())
                {
                    string strFilePath = "";
                    string strFileName = "";
                    if (!DBNull.Value.Equals(drSource["SPFLocalDirectory"]))
                        strFilePath = drSource["SPFLocalDirectory"].ToString();
                    dataRowTarget["FileExistsInVault"] = "Missing";
                    if (!DBNull.Value.Equals(drSource["SPFRemoteFileName"]))
                        strFileName = drSource["SPFRemoteFileName"].ToString();
                    if (strFileName != "" & strFilePath != "")
                    {
                        if (FileExist(strFilePath + @"\", strFileName) == true)
                            dataRowTarget["FileExistsInVault"] = "Exists";
                    }
                }
                string strCurrentDate;
                strCurrentDate = DateTime.Now.ToString();
                dataRowTarget["DELIVERYID"] = strDELIVERYID; // month
                dataRowTarget["PROJECTNAME"] = strPROJECT; // PROJECT
                intRow = intRow + 1;
                iBatchUpdate = iBatchUpdate + 1;

                dt.Rows.Add(dataRowTarget);
                if (iBatchUpdate == 1000000)
                {
                    WriteLog("Loaded " + intRow, gstrFileName);

                    objTargetDS = new DataSet();
                    objTargetDS.Tables.Add(dt);
                    objTargetAdapter.Fill(objTargetDS, strTable);
                    cmdTargetBldr = new OracleCommandBuilder(objTargetAdapter);

                    objTargetAdapter.Update(objTargetDS, strTable);
                    objTargetDS.AcceptChanges();
                    updateLoadStatusOrcl("ZDelivery", "In Progress", strClassification, intRow.ToString(), strTable, strPROJECT, strDELIVERYID, oraTargetConn);
                    objTargetDS.Tables.Remove(dt);
                    dt = new DataTable(strTable);
                    for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                    {
                        DataColumn colOBID = new DataColumn();
                        if (drSource.GetName(iColCounter) == "TAGCREATIONDATE")
                        {
                            DataColumn colCreationDate = new DataColumn();
                            colCreationDate.DataType = typeof(DateTime);
                            colCreationDate.ColumnName = "TAGCREATIONDATE";
                            dt.Columns.Add(colCreationDate);
                        }
                        else
                        {
                            colOBID.DataType = drSource.GetName(iColCounter).GetType();
                            colOBID.ColumnName = drSource.GetName(iColCounter);
                            dt.Columns.Add(colOBID);
                        }
                    }

                    colDelivery = new DataColumn();
                    colDelivery.DataType = typeof(string);
                    colDelivery.ColumnName = "DELIVERYID";
                    dt.Columns.Add(colDelivery);
                    colProject = new DataColumn();
                    colProject.DataType = typeof(string);
                    colProject.ColumnName = "PROJECTNAME";
                    dt.Columns.Add(colProject);
                    colProject = new DataColumn();
                    colProject.DataType = typeof(DateTime);
                    colProject.ColumnName = "ZADDATESTAMP";
                    dt.Columns.Add(colProject);

                    colFileExistsInVault = new DataColumn();
                    colFileExistsInVault.DataType = typeof(string);
                    colFileExistsInVault.ColumnName = "FileExistsInVault";
                    dt.Columns.Add(colFileExistsInVault);


                    // 
                    iBatchUpdate = 0;
                }
            }
            if (iBatchUpdate > 0)
            {
                objTargetDS = new DataSet();
                objTargetDS.Tables.Add(dt);
                objTargetAdapter.Fill(objTargetDS, strTable);
                cmdTargetBldr = new OracleCommandBuilder(objTargetAdapter);

                objTargetAdapter.Update(objTargetDS, strTable);
                objTargetDS.AcceptChanges();
                WriteLog("Loaded " + intRow, gstrFileName);
                updateLoadStatusOrcl("ZDelivery", "COMPLETED", strClassification, intRow.ToString(), strTable, strPROJECT, strDELIVERYID, oraTargetConn);
            }
        }

        internal static void InsertSOAndCDWInconsistencyTableFOrSQLServer(SqlConnection sqlTargetConn, string strDynTable, string strProject, string strDelivery, bool iscdw)
        {
            string strTable = "";
            if (iscdw)
            {
                strTable = "CDW_TAGINCONSITENCYREPORT";
            }
            else
            {
                strTable = "SO_TAGINCONSITENCYREPORT";
            }

            string cTeWithRowNumberQuery = @"WITH added_row_number AS (
  SELECT
    *,
    ROW_NUMBER() OVER(PARTITION BY Tag_Name,propertydef ORDER BY Tag_Name,propertydef DESC) AS row_number
  FROM " + strTable + @" where  Tag_Name is not null and TAG_NAME <> ''
)";

            string strSQL = "";
            string strInsertSQL = "";
            string strDomainFields = "";
            string strWhere;


            strWhere = " where a.projectname='" + strProject + "' AND a.DELIVERYID='" + strDelivery + "' AND  a.row_number = 1;";
            strInsertSQL = CreateSOAndCDWInconsistencyTableInnerSqlForSqlServer(strProject, strDelivery, ref strDomainFields, sqlTargetConn, iscdw);
            strInsertSQL = strInsertSQL + strWhere;
            strSQL = cTeWithRowNumberQuery + "insert into " + strDynTable + " (CLASSIFICATION,projectname,deliveryid,Tag_Name,propertydef,LatestValueDomain," + strDomainFields + ")  " + strInsertSQL;
            // return strSQL;
            using (var cmdSource = new SqlCommand())
            {
                if (sqlTargetConn.State == ConnectionState.Closed)

                    sqlTargetConn.Open();
                cmdSource.CommandTimeout = 0;
                cmdSource.Connection = sqlTargetConn;
                cmdSource.CommandText = strSQL;

                cmdSource.ExecuteNonQuery();
                sqlTargetConn.Close();
            }
        }

        internal static void InsertSOAndCDWInconsistencyTableForOracle(OracleConnection sqlTargetConn, string strDynTable, string strProject, string strDelivery, bool iscdw)
        {
           
                string strTable = "";
                string strSQL = "";
                string strInsertSQL = "";
                string strDomainFields = "";
                string strWhere;
                if (iscdw)
                {
                    strTable = "CDW_TAGINCONSITENCYREPORT";
                }
                else
                {
                    strTable = "SO_TAGINCONSITENCYREPORT";
                }

                strWhere = " where projectname='" + strProject + "' AND DELIVERYID='" + strDelivery + "'"; // and CLASSIFICATION='" & strClassification & "'"
                strInsertSQL = CreateSOAndCDWInconsistencyTableInnerSqlForOracle(strProject, strDelivery, ref strDomainFields, sqlTargetConn, iscdw);
                strInsertSQL = strInsertSQL + strWhere;
                strSQL = "insert into " + strDynTable + " (CLASSIFICATION,projectname,deliveryid,Tag_Name,propertydef,LatestValueDomain," + strDomainFields + ")  " + strInsertSQL;
                // return strSQL;
                using (var cmdSource = new OracleCommand())
                {
                    if (sqlTargetConn.State == ConnectionState.Closed)
                        sqlTargetConn.Open();
                    cmdSource.Connection = sqlTargetConn;
                    cmdSource.CommandText = strSQL;

                    cmdSource.ExecuteNonQuery();
                    sqlTargetConn.Close();
                }

            
        }

        private static string CreateSOAndCDWInconsistencyTableInnerSqlForOracle(string strProject, string strDelivery,ref string strDomainFields, OracleConnection sqlTargetConn, bool iscdw)
        {
            using (var cmdSource = new OracleCommand())
            {
              
                string strTable = "";
              
               
               
                if (iscdw)
                {
                    strTable = "CDW_TAGINCONSITENCYREPORT";
                }
                else
                {
                    strTable = "SO_TAGINCONSITENCYREPORT";
                }

                string strSQLSource = "";
                string strQuery = "select distinct domain from " + strTable;
                strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
                if (sqlTargetConn.State == ConnectionState.Closed)
                    sqlTargetConn.Open();
                cmdSource.Connection = sqlTargetConn;
                cmdSource.CommandText = strSQLSource;
               
               var drSource = cmdSource.ExecuteReader();
                string strDomain = "";
                int intRowCounter = 0;
                string strPivotDomainFields = "";
            
               var strSQL = "select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,";
                while (drSource.Read())
                {
                    intRowCounter = intRowCounter + 1;
                    if (!DBNull.Value.Equals(drSource["Domain"]))

                    {
                        strDomain = drSource["Domain"].ToString();
                        if (strDomainFields == "")
                        {
                            strDomainFields = strDomain;
                            strPivotDomainFields = "'" + strDomain + "' as " + strDomain;
                        }
                        else
                        {
                            strDomainFields = strDomainFields + "," + strDomain;
                            strPivotDomainFields = strPivotDomainFields + ",'" + strDomain + "' as " + strDomain;
                        }
                    }
                }
                drSource.Close();
                sqlTargetConn.Close();
                strSQL = " select CLASSIFICATION,projectname,deliveryid,Tag_Name,propertydef,LatestValueDomain," + strDomainFields + @" from
                     (
                     select a.CLASSIFICATION,a.projectname,a.deliveryid,a.ZADDATESTAMP,a.Tag_Name, a.propertydef,a.domain,a.propertyvalueDesc,
                     b.svalue || '  :-  ' ||  b.Domain as LatestValueDomain
                      from  "  // PID,ELE,EQD,PTF_EQPL_P,IIC,PD,SP3D,EQL,IML
            + strTable + @" a 
                     inner join   
                    (
                      select CLASSIFICATION,projectname,deliveryid,ISLATEST,domain,Tag_Name,Tag_Class,propertydef as Def , propertyvalueDesc as svalue 
                     from  " + strTable + @" 
                      where ISLATEST='True' and projectname ='" + strProject + "' and deliveryid='" + strDelivery + @"' and Tag_Name is not null ) b
                     on a.propertydef=b.def and a.Tag_Name=b.Tag_Name and a.CLASSIFICATION=b.CLASSIFICATION
                    where a.projectname ='" + strProject + "' and a.deliveryid='" + strDelivery + @"' and  a.Tag_Name is not null
                    order by 5,6
                    )
                     pivot
                     (
                     max(propertyvalueDesc) for domain in (" + strPivotDomainFields + @") 
                    )";
                return strSQL;
            }

        }
        private static string CreateSOAndCDWInconsistencyTableInnerSqlForSqlServer(string strProject, string strDelivery, ref string strDomainFields, SqlConnection sqlTargetConn, bool iscdw)
        {
            using (var cmdSource = new SqlCommand())
            {

                string strTable = "";

                if (iscdw)
                {
                    strTable = "CDW_TAGINCONSITENCYREPORT";
                }
                else
                {
                    strTable = "SO_TAGINCONSITENCYREPORT";
                }

                string strSQLSource = "";
                string strQuery = "select distinct domain from " + strTable;
                strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
                if (sqlTargetConn.State == ConnectionState.Closed)
                    sqlTargetConn.Open();
                cmdSource.Connection = sqlTargetConn;
                cmdSource.CommandText = strSQLSource;

                var drSource = cmdSource.ExecuteReader();
                string strDomain = "";
                string strDomainTableAlias = "";
                int intRowCounter = 0;
                string strPivotDomainFields = "";
                string strSelectFieldsFormedFromOuterJoinTables = "";
                string strOuterJoinExpressionForDomains = "";
                string latestDataSelectCaseWhenStatements = "";

                var strSQL = "select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,";
                while (drSource.Read())
                {
                    intRowCounter = intRowCounter + 1;
                    if (!DBNull.Value.Equals(drSource["Domain"]))

                    {
                        strDomain = drSource["Domain"].ToString();
                        strDomainTableAlias = strDomain + "domian";
                        if (strDomainFields == "")
                        {
                            strDomainFields = strDomain;
                            strPivotDomainFields = "[" + strDomain + "] as " + strDomain;
                            strSelectFieldsFormedFromOuterJoinTables = strDomain + "domian.PROPERTYVALUE as " + strDomain;
                        }
                        else
                        {
                            strDomainFields = strDomainFields + "," + strDomain;
                            strPivotDomainFields = strPivotDomainFields + ",[" + strDomain + "] as " + strDomain;
                            strSelectFieldsFormedFromOuterJoinTables = strSelectFieldsFormedFromOuterJoinTables+","+ strDomain + "domian.PROPERTYVALUE as " + strDomain;
                        }

                        strOuterJoinExpressionForDomains = strOuterJoinExpressionForDomains + @" left outer join CDW_TAGINCONSITENCYREPORT "+ strDomainTableAlias + " on " 
                            + " ("+ strDomainTableAlias + ".PROPERTYDEF=a.PROPERTYDEF  and "+

                      strDomainTableAlias+".Tag_Name=a.Tag_Name and "+strDomainTableAlias+ ".projectname ='" + strProject + "' and " + strDomainTableAlias+ ".deliveryid='" + strDelivery + "' and  " + strDomainTableAlias+ ".domain = '" + strDomain + "')";

                        latestDataSelectCaseWhenStatements = latestDataSelectCaseWhenStatements +
                            "  WHEN " + strDomainTableAlias + ".ISLATEST='True'  THEN  concat( " + strDomainTableAlias + ".PROPERTYVALUE,'  :-  '," + strDomainTableAlias + ".Domain)";
                    }
                }
                drSource.Close();
                sqlTargetConn.Close();



                string latestDataSelectCase = @" CASE "+latestDataSelectCaseWhenStatements+
                    @" ELSE ' '
END AS LatestValueDomain";
                  

                strSQL = @" select a.CLASSIFICATION,a.projectname,a.deliveryid,a.Tag_Name, a.propertydef,"
					
                    +latestDataSelectCase+"," + strSelectFieldsFormedFromOuterJoinTables + @"  
                      from  added_row_number a " + strOuterJoinExpressionForDomains;

                return strSQL;
            }

        }

        internal static string CreateSOAndCDWInconsistencyTableSQLForSqlServer(SqlConnection sqlTargetConn, string strProject, string strDelivery,  bool isCdw, string strDynTable)
        {

            using (var cmdSource = new SqlCommand())
            {

                string strTable = "";



                if (isCdw)
                {
                    strTable = "CDW_TAGINCONSITENCYREPORT";
                }
                else
                {
                    strTable = "SO_TAGINCONSITENCYREPORT";
                }
                if (sqlTargetConn.State == ConnectionState.Closed)
                    sqlTargetConn.Open();


                string strQuery = "select distinct domain from " + strTable;
                var strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
                cmdSource.Connection = sqlTargetConn;
                cmdSource.CommandText = strSQLSource;

                SqlDataReader drSource = cmdSource.ExecuteReader();
                string domainColumns = string.Empty;
                int intRowCounter = 0;

                while (drSource.Read())
                {
                    intRowCounter = intRowCounter + 1;


                    if (!DBNull.Value.Equals(drSource["Domain"]))

                    {
                        domainColumns = domainColumns + ", " + drSource["Domain"] + " VARCHAR(100 )";


                    }
                }

                drSource.Close();
                sqlTargetConn.Close();







                string strCreateTableSQL = " Create table " + strDynTable + @" (
CLASSIFICATION VARCHAR(1000 ), 
	projectname VARCHAR(1000 ), 
	deliveryid VARCHAR(1000 ), 
	Tag_Name VARCHAR(1000 ), 
	propertydef VARCHAR(1000 ), 
	LatestValueDomain VARCHAR(2000 )"
                              + domainColumns + ")";















                return strCreateTableSQL;
            }

        }

        internal static string CreateSOAndCDWInconsistencyTableSQLForOracle(OracleConnection sqlTargetConn, string strProject, string strDelivery,  bool isCdw, string strDynTable)
        {
            using (var cmdSource = new OracleCommand())
            {
             
                string strTable = "";
              
                

                if (isCdw)
                {
                    strTable = "CDW_TAGINCONSITENCYREPORT";
                }
                else
                {
                    strTable = "SO_TAGINCONSITENCYREPORT";
                }
                if(sqlTargetConn.State==ConnectionState.Closed)
                sqlTargetConn.Open();

              
                string strQuery = "select distinct domain from " + strTable;
               var strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
                cmdSource.Connection = sqlTargetConn;
                cmdSource.CommandText = strSQLSource;

                OracleDataReader drSource = cmdSource.ExecuteReader();
                string domainColumns = string.Empty;
                int intRowCounter = 0;
           
                while (drSource.Read())
                {
                    intRowCounter = intRowCounter + 1;                  


                    if (!DBNull.Value.Equals(drSource["Domain"]))

                    {
                        domainColumns = domainColumns + ", " + drSource["Domain"] + " VARCHAR(1000 )";


                    }
                }

                drSource.Close();
                sqlTargetConn.Close();




               


                string strCreateTableSQL = " Create table " + strDynTable + @" (
CLASSIFICATION VARCHAR(1000 ), 
	projectname VARCHAR(1000 ), 
	deliveryid VARCHAR(1000 ), 
	Tag_Name VARCHAR(1000 ), 
	propertydef VARCHAR(1000 ), 
	LatestValueDomain VARCHAR(2000 )"
                              + domainColumns + ")";














           
                return strCreateTableSQL;
            }

        }

        internal static string InsertCorrelatedTablesSQL_DistinctTagNameForSQLSERVER(string strProject, string strDelivery, string strCDW_or_SO, SqlConnection oraTargetConn)

        {
            string strTable = ""; // SO_CORP7512_HF_4
            string strMainTable = "";
            if (strCDW_or_SO == "SO")
            {
                strTable = "SO_COR" + strProject;
                strMainTable = "SO_correlatedtags";
            }
            else if (strCDW_or_SO == "CDW")
            {
                strTable = "CDW_COR" + strProject;
                strMainTable = "CDW_correlatedtags";
            }

            string strWhere = "";
            strWhere = " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            SqlDataReader drSource;
            var cmdSource = new SqlCommand();

            var objSourceDatatable = new DataTable();
            string strDomainFields = "";
            List<string> domainNames = new List<string>();
            string strDomainCase = "";
            string strDomain = "";
            string strSQLSource = "";
            string strQuery = "select distinct domain from " + strMainTable;
            strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            if(oraTargetConn.State==ConnectionState.Closed)
            oraTargetConn.Open();
            cmdSource.Connection = oraTargetConn;
            cmdSource.CommandText = strSQLSource;

            drSource = cmdSource.ExecuteReader();
            while (drSource.Read())
            {
                if (!DBNull.Value.Equals(drSource["Domain"]))
                {
                    strDomain = (string)drSource["Domain"];
                    if (string.IsNullOrEmpty(strDomainCase))
                    {
                        strDomainFields = strDomain;
                        domainNames.Add(strDomain);
                      

                        strDomainCase = "CASE WHEN(o.domain= '" + strDomain + "' OR exists  (SELECT NULL from " + strMainTable + " where Tag_Name=o.Tag_Name  and domain='" + strDomain + "')) THEN 'Y' ELSE 'N' END AS "+ strDomain + " ";

                      
                    }
                    else
                    {
                        strDomainCase = strDomainCase + "," + "CASE WHEN(o.domain= '" + strDomain + "' OR exists  (SELECT NULL from " + strMainTable + " where Tag_Name=o.Tag_Name  and domain='" + strDomain + "')) THEN 'Y' ELSE 'N' END AS " + strDomain + " ";
                        strDomainFields = strDomainFields + "," + strDomain;
                        domainNames.Add(strDomain);
                    }
                }
            }
            oraTargetConn.Close();
            drSource.Close();

            string strInsertSQL = " Insert into " + strTable + "  (Tag_Name,Tag_Class,TAG_TYPE,TAG_SUBTYPE,projectname,deliveryid,CLASSIFICATION," + strDomainFields + @" ) VALUES  ";
                            
            string selectQueryFortheInsertValues = @"SELECT t.Tag_Name, t.Tag_Class, t.TAG_TYPE, t.TAG_SUBTYPE, t.projectname, t.deliveryid,t.CLASSIFICATION," + strDomainFields + @"
                                FROM (
                                     SELECT o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE,o.projectname,o.deliveryid,o.CLASSIFICATION," + strDomainCase + @"  , ROW_NUMBER() OVER (PARTITION BY o.Tag_Name ORDER BY o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE) rn
                                            FROM " + strMainTable + " o " + strWhere + @"
                                   ) t
                                         WHERE rn = 1"; 

            GenerateInsertScriptsForCorelatedData( selectQueryFortheInsertValues,  strInsertSQL, oraTargetConn, domainNames);
            return null;
        }

        private static void GenerateInsertScriptsForCorelatedData(string selectQueryFortheInsertValues, string strInsertSQL, SqlConnection oraTargetConn, List<string> domainNames)
        {
            using (SqlCommand cmdSource = new SqlCommand())
            {

                if (oraTargetConn.State == ConnectionState.Closed)
                    oraTargetConn.Open();
                cmdSource.Connection = oraTargetConn;
                StringBuilder insertForData = new StringBuilder();

                insertForData.AppendLine(strInsertSQL);
                cmdSource.CommandText = selectQueryFortheInsertValues;
                cmdSource.CommandTimeout = 1200;
                var drSource = cmdSource.ExecuteReader();
                int batch = 0;

                while (drSource.Read())
                {
                    string va = "";

                  if (batch==0)
                    {
                         va =  @"( '" + drSource["Tag_Name"].ToString().Replace("'", "''") + "','" + drSource["Tag_Class"].ToString().Replace("'", "''") + "','" + drSource["TAG_TYPE"].ToString().Replace("'", "''") + "','" + drSource["TAG_SUBTYPE"].ToString().Replace("'", "''") + "','" + drSource["projectname"].ToString().Replace("'", "''") + "','" + drSource["deliveryid"].ToString().Replace("'", "''") + "','" + drSource["CLASSIFICATION"].ToString().Replace("'", "''") + "'";
                    }
                  else
                    {
                         va = @",( '" + drSource["Tag_Name"].ToString().Replace("'", "''") + "','" + drSource["Tag_Class"].ToString().Replace("'", "''") + "','" + drSource["TAG_TYPE"].ToString().Replace("'", "''") + "','" + drSource["TAG_SUBTYPE"].ToString().Replace("'", "''") + "','" + drSource["projectname"].ToString().Replace("'", "''") + "','" + drSource["deliveryid"].ToString().Replace("'", "''") + "','" + drSource["CLASSIFICATION"].ToString().Replace("'", "''") + "'";
                    }
                    batch++;

                    foreach (var item in domainNames)
                    {
                        va = va + ",'" + drSource[item]+"'";
                    }
                    va = va + ")";
                    insertForData.AppendLine(va);
                    if(batch==999)
                    {
                        ExecuteInsertStatement(oraTargetConn, insertForData);
                         insertForData = new StringBuilder();

                        insertForData.AppendLine(strInsertSQL);
                        batch = 0;
                    }
                  
                }

                if (batch > 0)
                {
                    ExecuteInsertStatement(oraTargetConn, insertForData);
                }
                oraTargetConn.Close();
                drSource.Close();
            }
        }

        private static void ExecuteInsertStatement(SqlConnection oraTargetConn, StringBuilder insertForData)
        {
            try
            {
                using (SqlCommand testcommand = new SqlCommand())
                {
                    testcommand.CommandText = insertForData.ToString();
                    testcommand.CommandType = CommandType.Text;
                    testcommand.CommandTimeout = 0;
                    SqlConnection tst = new SqlConnection(oraTargetConn.ConnectionString);
                    tst.Open();
                    testcommand.Connection = tst;

                    var result = testcommand.ExecuteNonQuery();
                    tst.Close();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        internal static string InsertCorrelatedTablesSQL_DistinctTagNameForOracle(string strProject, string strDelivery, string strCDW_or_SO, OracleConnection oraTargetConn)

        {
            string strTable = ""; // SO_CORP7512_HF_4
            string strMainTable = "";
            if (strCDW_or_SO == "SO")
            {
                strTable = "SO_COR" + strProject;
                strMainTable = "SO_correlatedtags";
            }
            else if (strCDW_or_SO == "CDW")
            {
                strTable = "CDW_COR" + strProject;
                strMainTable = "CDW_correlatedtags";
            }

            string strWhere = "";
            strWhere = " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            OracleDataReader drSource;
            var cmdSource = new OracleCommand();
            var objSourceOracleDataAdapter = new OracleDataAdapter();
            var objSourceDatatable = new DataTable();
            string strDomainFields = "";
            string strDomainCase = "";
            string strDomain = "";
            string strSQLSource = "";
            string strQuery = "select distinct domain from " + strMainTable;
            strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            if (oraTargetConn.State==ConnectionState.Closed)
            {
                oraTargetConn.Open();
            }
            cmdSource.Connection = oraTargetConn;
            cmdSource.CommandText = strSQLSource;

            drSource = cmdSource.ExecuteReader();
            while (drSource.Read())
            {
                if (!DBNull.Value.Equals(drSource["Domain"]))
                {
                    strDomain = (string)drSource["Domain"];
                    if (string.IsNullOrEmpty(strDomainCase))
                    {
                        strDomainFields = strDomain;
                        strDomainCase = "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " + strMainTable + " where Tag_Name=o.Tag_Name  and domain='" + strDomain + "') THEN 'Y' ELSE 'N' END AS " + strDomain + " ";
                    }
                    else
                    {
                        strDomainCase = strDomainCase + "," + "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " + strMainTable + "  where Tag_Name=o.Tag_Name  And domain='" + strDomain + "') THEN 'Y' ELSE 'N' END AS " + strDomain + " ";
                        strDomainFields = strDomainFields + "," + strDomain;
                    }
                }
            }

            string strInsertSQL = " Insert into " + strTable + "  (Tag_Name,Tag_Class,TAG_TYPE,TAG_SUBTYPE,projectname,deliveryid,CLASSIFICATION," + strDomainFields + @" )
                            SELECT t.*
                                FROM (
                                     SELECT o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE,o.projectname,o.deliveryid,o.CLASSIFICATION," + strDomainCase + @"  , ROW_NUMBER() OVER (PARTITION BY o.Tag_Name ORDER BY o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE) rn
                                            FROM " + strMainTable + " o " + strWhere + @"
                                   ) t
                                         WHERE rn = 1";
            
            return strInsertSQL;
        }



        internal static string CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn(string strProject, string strDelivery, string strCDW_or_SO, SqlConnection oraTargetConn)
        {

            string strTable = ""; // SO_CORP7512_HF_4
            string strMainTable = "";
            if (strCDW_or_SO == "SO")
            {
                strTable = "SO_COR" + strProject;
                strMainTable = "SO_correlatedtags";
            }
            else if (strCDW_or_SO == "CDW")
            {
                strTable = "CDW_COR" + strProject;
                strMainTable = "CDW_correlatedtags";
            }

            string strWhere = "";
            strWhere = " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            SqlDataReader drSource;
            var cmdSource = new SqlCommand();

            string domainColumns = string.Empty;
            string strSQLSource = "";
            string strQuery = "select distinct domain from " + strMainTable;
            strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            if(oraTargetConn.State==ConnectionState.Closed)
            oraTargetConn.Open();
            cmdSource.Connection = oraTargetConn;
            cmdSource.CommandText = strSQLSource;

            drSource = cmdSource.ExecuteReader();
            while (drSource.Read())
            {
                if (!DBNull.Value.Equals(drSource["Domain"]))

                {
                    domainColumns = domainColumns + ", " + drSource["Domain"] + " VARCHAR(100 )";


                }
            }
            oraTargetConn.Close();
            drSource.Close();


            string strInsertSQL = " Create table " + strTable + @" (
TAG_NAME VARCHAR(1000 ), 
	TAG_CLASS VARCHAR(1000 ), 
	TAG_TYPE VARCHAR(1000 ), 
	TAG_SUBTYPE VARCHAR(1000 ), 
	PROJECTNAME VARCHAR(1000 ), 
	DELIVERYID VARCHAR(1000 ), 
	CLASSIFICATION VARCHAR(2000 )"
                          + domainColumns + ")";


            return strInsertSQL;
        }



        internal static string CreateCorrelatedTablesSQL_DistinctTagNameForOracle(string strProject, string strDelivery, string strCDW_or_SO, OracleConnection oraTargetConn)
        {

            string strTable = ""; // SO_CORP7512_HF_4
            string strMainTable = "";
            if (strCDW_or_SO == "SO")
            {
                strTable = "SO_COR" + strProject;
                strMainTable = "SO_correlatedtags";
            }
            else if (strCDW_or_SO == "CDW")
            {
                strTable = "CDW_COR" + strProject;
                strMainTable = "CDW_correlatedtags";
            }
            if (oraTargetConn.State==ConnectionState.Closed)
            {
                oraTargetConn.Open();
            }

            string strWhere = "";
            strWhere = " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            OracleDataReader drSource;
            var cmdSource = new OracleCommand();
            var objSourceOracleDataAdapter = new OracleDataAdapter();
            var objSourceDatatable = new DataTable();
            string strDomainFields = "";
            string strDomainCase = "";
            string strDomain = "";
            string strSQLSource = "";
            string strQuery = "select distinct domain from " + strMainTable;
            strSQLSource = strQuery + " where Projectname ='" + strProject + "' and deliveryid='" + strDelivery + "'";
            cmdSource.Connection = oraTargetConn;
            cmdSource.CommandText = strSQLSource;
            objSourceOracleDataAdapter.SelectCommand = cmdSource;
            objSourceOracleDataAdapter.Fill(objSourceDatatable);
            drSource = cmdSource.ExecuteReader();
            while (drSource.Read())
            {
                if (!DBNull.Value.Equals(drSource["Domain"]))

                {
                    strDomain = (string)drSource["Domain"];
                    if (string.IsNullOrEmpty(strDomainCase))
                    {
                        strDomainFields = strDomain;
                        strDomainCase = "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " + strMainTable + " where Tag_Name=o.Tag_Name  and domain='" + strDomain + "') THEN 'Y' ELSE 'N' END AS " + strDomain + " ";
                    }
                    else
                    {
                        strDomainCase = strDomainCase + "," + "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " + strMainTable + "  where Tag_Name=o.Tag_Name  And domain='" + strDomain + "') THEN 'Y' ELSE 'N' END AS " + strDomain + " ";
                        strDomainFields = strDomainFields + "," + strDomain;
                    }
                }
            }

            string strInsertSQL = " Create table " + strTable + @"  as 
                            SELECT t.*
                                FROM (
                                     SELECT o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE,o.projectname,o.deliveryid,o.CLASSIFICATION," + strDomainCase + @"  , ROW_NUMBER() OVER (PARTITION BY o.Tag_Name ORDER BY o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE) rn
                                            FROM " + strMainTable + " o " + strWhere + @"
                                   ) t
                                         WHERE rn = 1 and 1=0";
            return strInsertSQL;


        }

        public static void ExportSchemaObjectSql(string strDELIVERYID, string strPROJECT, string strQuery, string strTable, string strClassification, SqlConnection oraSourceConn, SqlConnection oraTargetConn, string gstrFileName)
        {

            using (SqlDataAdapter objSourceOracleDataAdapter = new SqlDataAdapter())
            {
                using (SqlCommand cmdSource = new SqlCommand())
                {
                    using (SqlCommand cmdTarget = new SqlCommand())
                    {

                        using (var objTargetAdapter = new SqlDataAdapter())
                        {

                            string strSQLSource = "";
                            string strTempSQLSource = "";


                            strSQLSource = strQuery;
                            cmdSource.Connection = oraSourceConn;

                            strSQLSource = strQuery;

                            cmdSource.CommandText = strSQLSource;


                            DataTable objSourceDatatable = new DataTable();
                            objSourceOracleDataAdapter.SelectCommand = cmdSource;
                            objSourceOracleDataAdapter.Fill(objSourceDatatable);

                            SqlDataReader drSource = cmdSource.ExecuteReader();


                            DataSet objTargetDS;

                            DataRow dataRowTarget;

                            cmdTarget.Connection = oraTargetConn;


                            cmdTarget.CommandText = "select * from " + strTable;
                            objTargetAdapter.SelectCommand = cmdTarget;
                            // 

                            DataTable dt = new DataTable(strTable);

                            int iBatchUpdate = 0;
                            for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                            {
                                DataColumn colOBID = new DataColumn();
                                if (drSource.GetName(iColCounter) == "TAGCREATIONDATE")
                                {
                                    DataColumn colCreationDate = new DataColumn();
                                    colCreationDate.DataType = typeof(DateTime);
                                    colCreationDate.ColumnName = "TAGCREATIONDATE";
                                    dt.Columns.Add(colCreationDate);
                                }
                                else
                                {
                                    colOBID.DataType = drSource.GetName(iColCounter).GetType();
                                    colOBID.ColumnName = drSource.GetName(iColCounter);
                                    dt.Columns.Add(colOBID);
                                }
                            }
                            DataColumn colDelivery = new DataColumn();
                            colDelivery.DataType = typeof(string);
                            colDelivery.ColumnName = "DELIVERYID";
                            dt.Columns.Add(colDelivery);
                            DataColumn colProject = new DataColumn();
                            colProject.DataType = typeof(string);
                            colProject.ColumnName = "PROJECTNAME";
                            dt.Columns.Add(colProject);
                            colProject = new DataColumn();
                            colProject.DataType = typeof(DateTime);
                            colProject.ColumnName = "ZADDATESTAMP";
                            dt.Columns.Add(colProject);

                            DataColumn colFileExistsInVault = new DataColumn();
                            colFileExistsInVault.DataType = typeof(string);
                            colFileExistsInVault.ColumnName = "FileExistsInVault";
                            dt.Columns.Add(colFileExistsInVault);

                            string strFieldName;
                            double intRow = 0;

                            while (drSource.Read())
                            {
                                dataRowTarget = dt.NewRow();


                                for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                                {
                                    strFieldName = drSource.GetName(iColCounter);
                                    if (strFieldName == "TAGCREATIONDATE")
                                    {
                                        string strCreationdate = "";
                                        strCreationdate = drSource[strFieldName].ToString().Substring(0, 10);
                                        dataRowTarget["TAGCREATIONDATE"] = strCreationdate; // 
                                    }
                                    else if (strFieldName == "FileExistsInVault")
                                        dataRowTarget[strFieldName] = "False";
                                    else if (drSource[strFieldName] != null)
                                        dataRowTarget[strFieldName] = drSource[strFieldName];
                                }

                                if (strTable.ToUpper() == "SPFVIEWFILES" | strTable.ToUpper() == "FilesNotInVault".ToUpper())
                                {
                                    string strFilePath = "";
                                    string strFileName = "";
                                    if (!DBNull.Value.Equals(drSource["SPFLocalDirectory"]))
                                        strFilePath = drSource["SPFLocalDirectory"].ToString();
                                    dataRowTarget["FileExistsInVault"] = "Missing";
                                    if (!DBNull.Value.Equals(drSource["SPFRemoteFileName"]))
                                        strFileName = drSource["SPFRemoteFileName"].ToString();
                                    if (strFileName != "" & strFilePath != "")
                                    {
                                        if (FileExist(strFilePath + @"\", strFileName) == true)
                                            dataRowTarget["FileExistsInVault"] = "Exists";
                                    }
                                }
                                string strCurrentDate;
                                strCurrentDate = DateTime.Now.ToString();
                                dataRowTarget["DELIVERYID"] = strDELIVERYID; // month
                                dataRowTarget["PROJECTNAME"] = strPROJECT; // PROJECT
                                intRow = intRow + 1;
                                iBatchUpdate = iBatchUpdate + 1;

                                dt.Rows.Add(dataRowTarget);
                                if (iBatchUpdate == 10000000)
                                {
                                    WriteLog("Loaded " + intRow, gstrFileName);

                                    objTargetDS = new DataSet();
                                    objTargetDS.Tables.Add(dt);
                                    objTargetAdapter.Fill(objTargetDS, strTable);
                                    var cmdTargetBldr = new SqlCommandBuilder(objTargetAdapter);

                                    objTargetAdapter.Update(objTargetDS, strTable);
                                    objTargetDS.AcceptChanges();
                                    updateLoadStatusSql("ZDelivery", "In Progress", strClassification, intRow.ToString(), strTable, strPROJECT, strDELIVERYID, oraTargetConn);
                                    objTargetDS.Tables.Remove(dt);
                                    dt = new DataTable(strTable);
                                    for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                                    {
                                        DataColumn colOBID = new DataColumn();
                                        if (drSource.GetName(iColCounter) == "TAGCREATIONDATE")
                                        {
                                            DataColumn colCreationDate = new DataColumn();
                                            colCreationDate.DataType = typeof(DateTime);
                                            colCreationDate.ColumnName = "TAGCREATIONDATE";
                                            dt.Columns.Add(colCreationDate);
                                        }
                                        else
                                        {
                                            colOBID.DataType = drSource.GetName(iColCounter).GetType();
                                            colOBID.ColumnName = drSource.GetName(iColCounter);
                                            dt.Columns.Add(colOBID);
                                        }
                                    }

                                    colDelivery = new DataColumn();
                                    colDelivery.DataType = typeof(string);
                                    colDelivery.ColumnName = "DELIVERYID";
                                    dt.Columns.Add(colDelivery);
                                    colProject = new DataColumn();
                                    colProject.DataType = typeof(string);
                                    colProject.ColumnName = "PROJECTNAME";
                                    dt.Columns.Add(colProject);
                                    colProject = new DataColumn();
                                    colProject.DataType = typeof(DateTime);
                                    colProject.ColumnName = "ZADDATESTAMP";
                                    dt.Columns.Add(colProject);

                                    colFileExistsInVault = new DataColumn();
                                    colFileExistsInVault.DataType = typeof(string);
                                    colFileExistsInVault.ColumnName = "FileExistsInVault";
                                    dt.Columns.Add(colFileExistsInVault);


                                    // 
                                    iBatchUpdate = 0;
                                }
                            }
                            if (iBatchUpdate > 0)
                            {
                                objTargetDS = new DataSet();
                                objTargetDS.Tables.Add(dt);
                                objTargetAdapter.Fill(objTargetDS, strTable);
                                var cmdTargetBldr = new SqlCommandBuilder(objTargetAdapter);

                                objTargetAdapter.Update(objTargetDS, strTable);
                                objTargetDS.AcceptChanges();
                                WriteLog("Loaded " + intRow, gstrFileName);
                                updateLoadStatusSql("ZDelivery", "COMPLETED", strClassification, intRow.ToString(), strTable, strPROJECT, strDELIVERYID, oraTargetConn);
                            }

                        }
                        //SqlCommandBuilder cmdTargetBldr;



                    }

                }


            }

        }

        public static void ExportSchemaObjectSqlChanged(string strDELIVERYID, string strPROJECT, string strQuery, string strTable, string strClassification, SqlConnection oraSourceConn, SqlConnection oraTargetConn, string gstrFileName)
        {
            SqlDataReader drSource = null;
            try
            {
                using (SqlCommand cmdSource = new SqlCommand())
                {

                    string strSQLSource = "";

                    strSQLSource = strQuery;
                    oraSourceConn.Open();
                    cmdSource.Connection = oraSourceConn;

                    strSQLSource = strQuery;

                    cmdSource.CommandText = strSQLSource;
                    cmdSource.CommandTimeout = 1200;
                    drSource = cmdSource.ExecuteReader();


                    DataRow dataRowTarget;
                    // 

                    DataTable dt = new DataTable(strTable);

                    int iBatchUpdate = 0;
                    for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                    {
                        DataColumn colOBID = new DataColumn();
                        if (drSource.GetName(iColCounter) == "TAGCREATIONDATE")
                        {
                            DataColumn colCreationDate = new DataColumn();
                            colCreationDate.DataType = typeof(DateTime);
                            colCreationDate.ColumnName = "TAGCREATIONDATE";
                            dt.Columns.Add(colCreationDate);
                        }
                        else
                        {
                            colOBID.DataType = drSource.GetName(iColCounter).GetType();
                            colOBID.ColumnName = drSource.GetName(iColCounter);
                            dt.Columns.Add(colOBID);
                        }
                    }
                    DataColumn colDelivery = new DataColumn();
                    colDelivery.DataType = typeof(string);
                    colDelivery.ColumnName = "DELIVERYID";
                    dt.Columns.Add(colDelivery);
                    DataColumn colProject = new DataColumn();
                    colProject.DataType = typeof(string);
                    colProject.ColumnName = "PROJECTNAME";
                    dt.Columns.Add(colProject);
                    //colProject = new DataColumn();
                    //colProject.DataType = typeof(DateTime);
                    //colProject.ColumnName = "ZADDATESTAMP";
                    //dt.Columns.Add(colProject);
                    if (strTable.ToUpper() == "SPFVIEWFILES" || strTable.ToUpper() == "FilesNotInVault".ToUpper())
                    {
                        DataColumn colFileExistsInVault = new DataColumn();
                        colFileExistsInVault.DataType = typeof(string);
                        colFileExistsInVault.ColumnName = "FileExistsInVault";
                        dt.Columns.Add(colFileExistsInVault);

                    }


                    string strFieldName;
                    double intRow = 0;

                    while (drSource.Read())
                    {
                        dataRowTarget = dt.NewRow();


                        for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                        {
                            strFieldName = drSource.GetName(iColCounter);
                            if (strFieldName == "TAGCREATIONDATE")
                            {
                                string strCreationdate = "";
                                strCreationdate = drSource[strFieldName].ToString().Substring(0, 10);
                                dataRowTarget["TAGCREATIONDATE"] = strCreationdate; // 
                            }
                            else if (strFieldName == "FileExistsInVault")
                                dataRowTarget[strFieldName] = "False";
                            else if (drSource[strFieldName] != null)
                                dataRowTarget[strFieldName] = drSource[strFieldName];
                        }

                        if (strTable.ToUpper() == "SPFVIEWFILES" || strTable.ToUpper() == "FilesNotInVault".ToUpper())
                        {
                            string strFilePath = "";
                            string strFileName = "";
                            if (!DBNull.Value.Equals(drSource["SPFLocalDirectory"]))
                                strFilePath = drSource["SPFLocalDirectory"].ToString();
                            dataRowTarget["FileExistsInVault"] = "Missing";
                            if (!DBNull.Value.Equals(drSource["SPFRemoteFileName"]))
                                strFileName = drSource["SPFRemoteFileName"].ToString();
                            if (strFileName != "" & strFilePath != "")
                            {
                                if (FileExist(strFilePath + @"\", strFileName) == true)
                                    dataRowTarget["FileExistsInVault"] = "Exists";
                            }
                        }
                        string strCurrentDate;
                        strCurrentDate = DateTime.Now.ToString();
                        dataRowTarget["DELIVERYID"] = strDELIVERYID; // month
                        dataRowTarget["PROJECTNAME"] = strPROJECT; // PROJECT
                                                                   // dataRowTarget["ZADDATESTAMP"] = strCurrentDate; // PROJECT
                        intRow = intRow + 1;
                        iBatchUpdate = iBatchUpdate + 1;

                        dt.Rows.Add(dataRowTarget);
                        if (iBatchUpdate == 999)
                        {
                            WriteLog("Loaded " + intRow, gstrFileName);
                            //BulkInsertToDataBase(dt, oraTargetConn);
                            BulkInsertToDataBaseWithInsertStatement(dt, oraTargetConn);


                            updateLoadStatusSql("ZDelivery", "In Progress", strClassification, intRow.ToString(), strTable, strPROJECT, strDELIVERYID, oraTargetConn);

                            return;
                            dt = new DataTable(strTable);
                            for (var iColCounter = 0; iColCounter <= drSource.FieldCount - 1; iColCounter++)
                            {
                                DataColumn colOBID = new DataColumn();
                                if (drSource.GetName(iColCounter) == "TAGCREATIONDATE")
                                {
                                    DataColumn colCreationDate = new DataColumn();
                                    colCreationDate.DataType = typeof(DateTime);
                                    colCreationDate.ColumnName = "TAGCREATIONDATE";
                                    dt.Columns.Add(colCreationDate);
                                }
                                else
                                {
                                    colOBID.DataType = drSource.GetName(iColCounter).GetType();
                                    colOBID.ColumnName = drSource.GetName(iColCounter);
                                    dt.Columns.Add(colOBID);
                                }
                            }
                            colDelivery = new DataColumn();
                            colDelivery.DataType = typeof(string);
                            colDelivery.ColumnName = "DELIVERYID";
                            dt.Columns.Add(colDelivery);
                            colProject = new DataColumn();
                            colProject.DataType = typeof(string);
                            colProject.ColumnName = "PROJECTNAME";
                            dt.Columns.Add(colProject);

                            //colDelivery = new DataColumn();
                            //colDelivery.DataType = typeof(string);
                            //colDelivery.ColumnName = "DELIVERYID";
                            //dt.Columns.Add(colDelivery);

                            //colProject = new DataColumn();
                            //colProject.DataType = typeof(string);
                            //colProject.ColumnName = "PROJECTNAME";
                            //dt.Columns.Add(colProject);

                            //colProject = new DataColumn();
                            //colProject.DataType = typeof(DateTime);
                            //colProject.ColumnName = "ZADDATESTAMP";
                            //dt.Columns.Add(colProject);

                            //colFileExistsInVault = new DataColumn();
                            //colFileExistsInVault.DataType = typeof(string);
                            //colFileExistsInVault.ColumnName = "FileExistsInVault";
                            //dt.Columns.Add(colFileExistsInVault);


                            // 
                            iBatchUpdate = 0;
                        }
                    }
                    if (iBatchUpdate > 0)
                    {
                        BulkInsertToDataBaseWithInsertStatement(dt, oraTargetConn);
                        //BulkInsertToDataBase(dt, oraTargetConn);

                        WriteLog("Loaded " + intRow, gstrFileName);
                        updateLoadStatusSql("ZDelivery", "COMPLETED", strClassification, intRow.ToString(), strTable, strPROJECT, strDELIVERYID, oraTargetConn);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oraSourceConn.Close();
                if (drSource != null)
                    drSource.Close();

            }



        }
        public static void BulkInsertToDataBase(DataTable table, SqlConnection targetConnection)
        {

            SqlBulkCopy objbulk = new SqlBulkCopy(targetConnection);
            objbulk.DestinationTableName = table.TableName;

            foreach (DataColumn column in table.Columns)
            {
                objbulk.ColumnMappings.Add(column.ColumnName, column.ColumnName);
            }

            SqlCommand testcommand = new SqlCommand();

            testcommand.CommandText = "select * from " + table.TableName;
            testcommand.Connection = targetConnection;



            SqlDataReader testforcheck = testcommand.ExecuteReader();

            for (var iColCounter = 0; iColCounter <= testforcheck.FieldCount - 1; iColCounter++)
            {
                var strFieldName = testforcheck.GetName(iColCounter);
                if (table.Columns.Contains(strFieldName))
                {

                }
                else
                {
                    var error = "";
                }
            }
            targetConnection.Open();

            objbulk.WriteToServer(table);


        }
        public static void BulkInsertToDataBaseWithInsertStatement(DataTable table, SqlConnection targetConnection)
        {


            String query = "Insert into " + table.TableName + " (";

            foreach (DataColumn column in table.Columns)
            {
                query = query + column.ColumnName + ",";

            }
            query = query + "ZADDATESTAMP";

            //query = query.TrimEnd(',');

            query = query + ") VALUES    ";
            StringBuilder insertstatement = new StringBuilder(query);
            int rows = table.Rows.Count;
            foreach (DataRow row in table.Rows)
            {
                query = "( ";
                foreach (DataColumn column in table.Columns)
                {
                    if (column.DataType == typeof(string))
                    {
                        var value = row[column.ColumnName].ToString();
                        query = query + "'" + value.Replace("'", "''") + "',";
                    }
                    else
                    {
                        query = query + "'" + row[column.ColumnName] + "',";
                    }


                }

                DateTime time = DateTime.Now;
                string format = "yyyy-MM-dd HH:mm:ss";

                query = query + "'" + time.ToString(format) + "'";
                //  query = query.TrimEnd(',');
                rows--;
                if (rows == 0)
                {
                    query = query + ");";
                }
                else
                {
                    query = query + "),";
                }

                insertstatement.AppendLine(query);
            }
            try
            {
                using (SqlCommand testcommand = new SqlCommand())
                {
                    testcommand.CommandText = insertstatement.ToString();
                    testcommand.CommandTimeout = 0;
                    targetConnection.Open();
                    testcommand.Connection = targetConnection;

                    var result = testcommand.ExecuteNonQuery();

                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                targetConnection.Close();
            }


        }
        internal static double ProgressStatusORCL(string strSQL, bool blnTarget, OracleConnection oraSourceConn, OracleConnection oraTargetConn)
        {
            using (var cmdProj = new OracleCommand())
            {
                var dt = new DataTable();
                if (blnTarget == true)
                {
                    cmdProj.Connection = oraTargetConn;
                }
                else
                {
                    cmdProj.Connection = oraSourceConn;
                }

                cmdProj.CommandText = strSQL;

                var reader = cmdProj.ExecuteReader();
                while (reader.Read())
                {
                    var testdata1 = reader[0];

                }
                reader.Close();

                var testdata = cmdProj.ExecuteScalar();

                return (double)testdata;

            }

        }

        public static void ExecuteQueryOracle(string strSQL, OracleConnection oraTargetConn) // schemaobjects
        {
            using (OracleCommand cmd = new OracleCommand(strSQL, oraTargetConn))
            {
                try
                {
                    if (oraTargetConn.State==ConnectionState.Closed)
                        oraTargetConn.Open();
                    cmd.ExecuteNonQuery();
                  
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }         
          


        }

        public static void ExecuteQuerySql(string strSQL, SqlConnection oraTargetConn) // schemaobjects
        {
            using (var cmdProj = new SqlCommand())
            {
                if (oraTargetConn.State == ConnectionState.Closed)
                    oraTargetConn.Open();
                cmdProj.Connection = oraTargetConn;
                cmdProj.CommandText = strSQL;
                cmdProj.CommandTimeout = 0;
                cmdProj.ExecuteNonQuery();
                oraTargetConn.Close();
                cmdProj.Dispose();
            }


        }
        public static string GetPlantORCL(string strProject, string strDelivery, OracleConnection oraTargetConn)   // schemaobjects
        {
            using (var cmdProj = new OracleCommand())
            {
                string strPlant = "";
                var dt = new DataTable();

                cmdProj.Connection = oraTargetConn;
                var strSQL = "select PLANTNAME from ZDELIVERY where PROJID='" + strProject + "' and DELIVERYNAME='" + strDelivery + "'";
                cmdProj.CommandText = strSQL;
                OracleDataReader dReader = cmdProj.ExecuteReader();
                while (dReader.Read())
                {
                    if (!DBNull.Value.Equals(dReader["PLANTNAME"]))
                    {
                        strPlant = (string)dReader["PLANTNAME"];
                    }
                }

                return strPlant;
            }

        }

        public static string GetPlantSql(string strProject, string strDelivery, SqlConnection oraTargetConn)   // schemaobjects
        {
            using (var cmdProj = new SqlCommand())
            {
                string strPlant = "";
                var dt = new DataTable();

                cmdProj.Connection = oraTargetConn;
                var strSQL = "select PLANTNAME from ZDELIVERY where PROJID='" + strProject + "' and DELIVERYNAME='" + strDelivery + "'";
                cmdProj.CommandText = strSQL;
                SqlDataReader dReader = cmdProj.ExecuteReader();
                while (dReader.Read())
                {
                    if (!DBNull.Value.Equals(dReader["PLANTNAME"]))
                    {
                        strPlant = (string)dReader["PLANTNAME"];
                    }
                }

                return strPlant;
            }

        }

        public static void updateLoadStatusOrcl(string strTable, string strStatus, string strClassification, string strLASTINSERTEDROWNUM, string strLASTTABLEINSERTED, string strProject, string strDelivery, OracleConnection oraTargetConn)
        {

            OracleCommand cmdProj = new OracleCommand();
            string strSQL;
            cmdProj.Connection = oraTargetConn;
            if (oraTargetConn.State != ConnectionState.Open)
                oraTargetConn.Open();
            strSQL = " update " + strTable + " set DELIVERY_STATUS  ='" + strStatus + "', LASTINSERTEDCLASSIFICATION  ='" + strClassification + "', LASTTABLEINSERTED  ='" + strLASTTABLEINSERTED + "', LASTINSERTEDROWNUM  ='" + strLASTINSERTEDROWNUM + "' where " + " PROJID='" + strProject + "' and DELIVERYNAME='" + strDelivery + "'";
            cmdProj.CommandText = strSQL;
            cmdProj.ExecuteNonQuery();
            cmdProj.Dispose();
        }

        public static void updateLoadStatusSql(string strTable, string strStatus, string strClassification, string strLASTINSERTEDROWNUM, string strLASTTABLEINSERTED, string strProject, string strDelivery, SqlConnection oraTargetConn)
        {
            try
            {
                SqlCommand cmdProj = new SqlCommand();
                string strSQL;
                oraTargetConn.Open();
                cmdProj.Connection = oraTargetConn;
                strSQL = " update " + strTable + " set DELIVERY_STATUS  ='" + strStatus + "', LASTINSERTEDCLASSIFICATION  ='" + strClassification + "', LASTTABLEINSERTED  ='" + strLASTTABLEINSERTED + "', LASTINSERTEDROWNUM  ='" + strLASTINSERTEDROWNUM + "' where " + " PROJID='" + strProject + "' and DELIVERYNAME='" + strDelivery + "'";
                cmdProj.CommandText = strSQL;
                cmdProj.ExecuteNonQuery();
                cmdProj.Dispose();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                oraTargetConn.Close();
            }

        }

        public static void WriteLog(string strLog, string gstrFileName)
        {
            StreamWriter swDataExportLog = File.AppendText(gstrFileName);
            swDataExportLog.WriteLine(DateTime.Now + " " + strLog);
            swDataExportLog.Close();
        }

        private static bool FileExist(string strFilePath, string strFileName)
        {



            if (System.IO.File.Exists(strFilePath + strFileName))
                return true;
            else
                return false;
        }



        public static DataSet ReadDataFromSql(string connectionString, string queryString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
        }
        public static DataSet ReadDataFromSqlWithActiveConnection(SqlConnection connection, string queryString)
        {

            SqlCommand command = new SqlCommand(queryString, connection);
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return ds;

        }

        public static void SetupForSql(string connectionString, string queryString)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand command = new SqlCommand(queryString, connection);
                command.ExecuteNonQuery();
            }
        }

        public static void SetupForOracle(string connectionString, string queryString)
        {
            using (OracleConnection connection = new OracleConnection(connectionString))
            {
                connection.Open();
                OracleCommand command = new OracleCommand(queryString, connection);
                command.ExecuteNonQuery();
            }
        }

        public static string FormConnectionStringForOracle(string userName, string password, string DataSourceOrTNSName)
        {

            return string.Format("User ID = {0}; Password = {1}; Data Source = {2};", userName, password, DataSourceOrTNSName);

        }

        public static string FormConnectionStringForSql(string dataSource, string initialCatalog)
        {

            return string.Format("Data Source ={0}; Initial Catalog = {1}; Integrated Security = True; Connect Timeout = 30; Encrypt = False; TrustServerCertificate = False; ApplicationIntent = ReadWrite; MultiSubnetFailover = False", dataSource, initialCatalog);

        }

    }
}
