﻿using ClosedXML.Excel;
using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
   public class ExcelFileContent
    {
        public int HeaderRowNumber { get; set; }
        public IXLRow[] Rows { get; set; }

        public IXLColumn[] Columns { get; set; }

        private IXLWorksheet _sheet;
        public ExcelFileContent(IXLWorksheet sheet)
        {
            _sheet = sheet;
        }
        public List<string> ColumnNames { get { return _columnNames; } }
        public List<List<string>> Rowsdata { get { return data; } }
        private List<string> _columnNames;
        private List<List<string>> data;

        public List<string> getColumnNames()
        {
            if(_columnNames==null)
            {
                 _columnNames = new List<string>();

                for (int i = 1; i <=Columns.Count(); i++)
                {
                    var colname = _sheet.Cell(HeaderRowNumber, i).GetFormattedString().Replace(" ", "_");
                    if(string.IsNullOrEmpty(colname) ||string.IsNullOrWhiteSpace(colname))
                    {
                        return _columnNames;
                    }
                    _columnNames.Add(colname);
                }
              
            }
            return _columnNames;

        }

        public List<List<string>> ReadFileDataAsRows()
        {
             data = new List<List<string>>();

            for (int i= (HeaderRowNumber+1);i<=Rows.Count();i++)
            {
                List<string> row = new List<string>();
                for (int j = 1; j <= Columns.Count(); j++)
                {
                    row.Add(_sheet.Cell(i, j).GetFormattedString());
                }
                if(row.All(x=>string.IsNullOrEmpty(x) || string.IsNullOrWhiteSpace(x)))
                    {
                    return data;
                }
                data.Add(row);

            }
            return data;
        }

        internal string FormTableCreateStatementSQL()
        {
            string strCreateTableSQL = @" Create table ExcelImportData ( ";

            for(int i=0;i<ColumnNames.Count-1;i++)
            {
                strCreateTableSQL = strCreateTableSQL + ColumnNames[i] + " VARCHAR(1000),";
            }
            strCreateTableSQL = strCreateTableSQL + ColumnNames[ColumnNames.Count - 1] + " VARCHAR(1000))";


            return strCreateTableSQL;
        }

        internal void AddDataToTable(OracleConnection oraTargetConn, SqlConnection sqlTargetConn)
        {
            string csvColumnNames = "";
            for (int i = 0; i < ColumnNames.Count - 1; i++)
            {
                csvColumnNames = csvColumnNames + ColumnNames[i] + ",";
            }
            csvColumnNames = csvColumnNames + ColumnNames[ColumnNames.Count - 1];            

            string strInsertSQLPrefix = " Insert into ExcelImportData   (" + csvColumnNames + " ) VALUES  ";



            List<string> eachRowDataValues = new List<string>();


       

             


            foreach (var row in data)
            {
                string va = "(";
               
                 
                
                for (int col = 0; col < ColumnNames.Count - 1; col++)
                {
                    va = va + "'" + row[col] + "'" + ",";
                }
                va = va + "'"+row[ColumnNames.Count - 1] +"'"+")" ;

                eachRowDataValues.Add(va);
              
            }     

                if (eachRowDataValues.Count > 0)
                {
                ExecuteInsertStatementForExcelData(oraTargetConn, sqlTargetConn, strInsertSQLPrefix, eachRowDataValues);
                }

        }

        private void ExecuteInsertStatementForExcelData(OracleConnection oraTargetConn, SqlConnection sqlTargetConn, string strInsertSQLPrefix, List<string> insertForData)
        {
            if(sqlTargetConn!=null)
            {
                ExecuteInsertStatementForExcelDataForSqlDataSource(sqlTargetConn,  strInsertSQLPrefix,  insertForData);
            }
         else
            {
                ExecuteInsertStatementForExcelDataForOracleDataSource(oraTargetConn, strInsertSQLPrefix, insertForData);
            }
        }
        private  void ExecuteInsertStatementForExcelDataForSqlDataSource(SqlConnection sqlTargetConn, string strInsertSQLPrefix, List<string> insertForData)
        {
            var cmdText = GenerateCmdTextForSql(strInsertSQLPrefix, insertForData);

            ExecuteForSqlDataSource(sqlTargetConn, cmdText);
        }

        private string GenerateCmdTextForSql(string strInsertSQLPrefix, List<string> insertForData)
        {
            StringBuilder completeSet = new StringBuilder();

            completeSet.AppendLine(strInsertSQLPrefix);

            for (int i = 0; i < insertForData.Count-1; i++)
            {
                completeSet.AppendLine(insertForData[i] + ",");
            }
            completeSet.AppendLine(insertForData[insertForData.Count] );

            return completeSet.ToString();
        }

        private static void ExecuteForSqlDataSource(SqlConnection sqlTargetConn, string cmdText)
        {
            try
            {
                using (SqlCommand testcommand = new SqlCommand())
                {
                    testcommand.CommandText = cmdText;
                    testcommand.CommandType = CommandType.Text;
                    testcommand.CommandTimeout = 0;
                    SqlConnection tst = new SqlConnection(sqlTargetConn.ConnectionString);
                    tst.Open();
                    testcommand.Connection = tst;

                    var result = testcommand.ExecuteNonQuery();
                    tst.Close();

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }

        private void ExecuteInsertStatementForExcelDataForOracleDataSource(OracleConnection oraTargetConn, string strInsertSQLPrefix, List<string> insertForData)
        {
            
             var cmdTexts = GenerateCmdTextForOracleInsertInBatch( strInsertSQLPrefix,  insertForData);

            foreach (var cmdText in cmdTexts)
            {
                ExecuteForOracleDataSource(oraTargetConn, cmdText);

            }         
        }

        private List<string> GenerateCmdTextForOracleInsertInBatch( string strInsertSQLPrefix, List<string> lists )
        {
            List<string> finalcmdTexts = new List<string>();

           

            for (int i = 0; i < lists.Count; i++)
            {
                finalcmdTexts.Add(strInsertSQLPrefix + lists[i] );
            }

            return finalcmdTexts;


        }

        private static void ExecuteForOracleDataSource(OracleConnection oraTargetConn, string cmdText)
        {
            try
            {
                using (OracleCommand testcommand = new OracleCommand(cmdText, oraTargetConn))
                {
                    
                    testcommand.CommandType = CommandType.Text;
                    testcommand.CommandTimeout = 0;
                    if (oraTargetConn.State != ConnectionState.Open)
                    {
                        oraTargetConn.Open();
                    }                 
                    

                    var result = testcommand.ExecuteNonQuery();
                    

                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {

            }
        }
    }
}
