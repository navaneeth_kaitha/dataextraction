﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClosedXML.Excel;

namespace Demo
{
   public class ExcelImportHelper
    {
        public string ExcelFilePath { get; set; }

        public ExcelImportHelper()
        {

        }


        public ExcelFileContent OpenExcelFileDialog()
        {
            OpenFileDialog of = new OpenFileDialog();
            of.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";

            if(of.ShowDialog()==DialogResult.OK)

            {
                ExcelFilePath = of.FileName;

             return  ReadExcelFileContent();
            }
            return null;
        }

        private ExcelFileContent ReadExcelFileContent()
        {
           using(var file= new FileStream(ExcelFilePath,FileMode.Open,FileAccess.Read,FileShare.ReadWrite))
            {
                using(var workBook=new XLWorkbook(file))
                {
                    IXLWorksheet sheet =workBook.Worksheet(1);

                   
                  ExcelFileContent content = new ExcelFileContent(sheet);
                    content.Columns = sheet.ColumnsUsed().ToArray();
                    content.Rows = sheet.RowsUsed().ToArray();
                    content.HeaderRowNumber = 2;
                    content.getColumnNames();
                    content.ReadFileDataAsRows();
                    return content;

                }

            }
        }
    }
}
