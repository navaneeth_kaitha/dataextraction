﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using Oracle.DataAccess.Client;
using DBExecuteHelper;
using System.Data.Common;
using System.Data.SqlClient;
using System.Data;

namespace Demo
{
    public class ExecuteSQLs
    {
        
        private DBExecuteHelper.DBExecuteHelper dbExecuteHelper;
        
        

        public SqlConnection SqlSourceConn { get;  set; }
        public SqlConnection SqlTargetConn { get; set; }
        public OracleConnection oraTargetConn { get; set; }
        public OracleConnection oraSourceConn { get; set; }


        public ExecuteSQLs()
        {
            dbExecuteHelper = new DBExecuteHelper.DBExecuteHelper();
        }
       
        public void InsertProgressStatus(string strREGISTEREDTOOLS, string strPUBLISHEDOCUMENTS, string FAILEDDOCUMENTS, string strPUBDOCSWITHNOVIEWFILE, string strNOOPENDOCUMENTS, string strProject, string strDelivery)
        {
            if (oraTargetConn != null)
                dbExecuteHelper.InsertProgressStatus(DataBaseType.Oracle, oraTargetConn,  strREGISTEREDTOOLS,  strPUBLISHEDOCUMENTS,  FAILEDDOCUMENTS,  strPUBDOCSWITHNOVIEWFILE,  strNOOPENDOCUMENTS,  strProject,  strDelivery);
            else
                dbExecuteHelper.InsertProgressStatus(DataBaseType.SQLServer, SqlTargetConn,  strREGISTEREDTOOLS,  strPUBLISHEDOCUMENTS,  FAILEDDOCUMENTS,  strPUBDOCSWITHNOVIEWFILE,  strNOOPENDOCUMENTS,  strProject,  strDelivery);


           
        }





        private  void ExecuteQueryStringWithTargetConnection(string strSQL)
        {
            if (oraTargetConn!=null)
                dbExecuteHelper.InsertData(DataBaseType.Oracle, strSQL, oraTargetConn);
            else
                dbExecuteHelper.InsertData(DataBaseType.SQLServer, strSQL, SqlTargetConn);
        }

        public void UpdateTagTypeDescIn_so_opedmsinstrdocrel(bool CDWChecked,bool SoChecked)
        {
            string strSQL;
            string strTable = "";
            if (CDWChecked )
            {
                strTable = "CDW_CORRELATEDTAGS";
            }
            else
            {
                strTable = "SO_CORRELATEDTAGS";
            }


            strSQL = @"merge into so_opedmsinstrdocrel
                   using
                    (select tag_name,TAG_TYPE,TAG_SUBTYPE from " + strTable + @")a
                    on
                    (a.tag_name=so_opedmsinstrdocrel.instrument)
                    when matched then update set so_opedmsinstrdocrel.Tagtypedesc=a.TAG_SUBTYPE";


            ExecuteQueryStringWithTargetConnection(strSQL);

            strSQL = @" merge into so_opedmsinstrdocrel
                    using
                    (select tag_name,TAG_TYPE,TAG_SUBTYPE from " + strTable + @")a
                    on
                    (a.tag_name=so_opedmsinstrdocrel.instrument)
                    when matched then update set so_opedmsinstrdocrel.Tagtype=a.TAG_TYPE";

            ExecuteQueryStringWithTargetConnection(strSQL);

            strSQL = @"  merge into so_opedmsEQPdocrel
                    using
                    (select tag_name,TAG_TYPE,TAG_SUBTYPE from " + strTable + @")a
                    on
                    (a.tag_name=so_opedmsEQPdocrel.equipment)
                    when matched then update set so_opedmsEQPdocrel.Tagtypedesc=a.TAG_SUBTYPE";

            ExecuteQueryStringWithTargetConnection(strSQL);

            strSQL = @"  merge into so_opedmsEQPdocrel
                    using
                    (select tag_name,TAG_TYPE,TAG_SUBTYPE from " + strTable + @")a
                    on
                    (a.tag_name=so_opedmsEQPdocrel.equipment)
                    when matched then update set so_opedmsEQPdocrel.Tagtype=a.TAG_TYPE";
            ExecuteQueryStringWithTargetConnection(strSQL);




            strSQL = @"  merge into so_opedmsEQPdocrel
                    using
                    (select Document,SPFDOCTYPE from  OPEDMS_DOCUMENT)a
                    on
                    (a.Document=so_opedmsEQPdocrel.DOCUMENT)
                    when matched then update set so_opedmsEQPdocrel.DOCTYPE=a.SPFDOCTYPE";

            ExecuteQueryStringWithTargetConnection(strSQL);

            strSQL = @"  merge into so_opedmsEQPdocrel
                    using
                    (select Document,SPFDOCUMENTCATEGORY from  OPEDMS_DOCUMENT)a
                    on
                    (a.Document=so_opedmsEQPdocrel.DOCUMENT)
                    when matched then update set so_opedmsEQPdocrel.DOCCATEGORY=a.SPFDOCUMENTCATEGORY";

            ExecuteQueryStringWithTargetConnection(strSQL);


            strSQL = @"  merge into so_opedmsEQPdocrel
                    using
                    (select Document,SPFDOCSUBTYPE from  OPEDMS_DOCUMENT)a
                    on
                    (a.Document=so_opedmsEQPdocrel.DOCUMENT)
                    when matched then update set so_opedmsEQPdocrel.DOCSUBTYPE=a.SPFDOCSUBTYPE";

            ExecuteQueryStringWithTargetConnection(strSQL);






            strSQL = @"  merge into so_opedmsinstrdocrel
                    using
                    (select Document,SPFDOCTYPE from  OPEDMS_DOCUMENT)a
                    on
                    (a.Document=so_opedmsEQPdocrel.DOCUMENT)
                    when matched then update set so_opedmsEQPdocrel.DOCTYPE=a.SPFDOCTYPE";

            ExecuteQueryStringWithTargetConnection(strSQL);

            strSQL = @"  merge into so_opedmsinstrdocrel
                    using
                    (select Document,SPFDOCUMENTCATEGORY from  OPEDMS_DOCUMENT)a
                    on
                    (a.Document=so_opedmsEQPdocrel.DOCUMENT)
                    when matched then update set so_opedmsEQPdocrel.DOCCATEGORY=a.SPFDOCUMENTCATEGORY";

            ExecuteQueryStringWithTargetConnection(strSQL);


            strSQL = @"  merge into so_opedmsinstrdocrel
                    using
                    (select Document,SPFDOCSUBTYPE from  OPEDMS_DOCUMENT)a
                    on
                    (a.Document=so_opedmsEQPdocrel.DOCUMENT)
                    when matched then update set so_opedmsEQPdocrel.DOCSUBTYPE=a.SPFDOCSUBTYPE";

            ExecuteQueryStringWithTargetConnection(strSQL);




        }

        public void UpdateEnumUIDwithName(string strTable, string strProject, string strDelivery)
        {
            string strSQL;
            if (oraTargetConn != null)
            {
                strSQL = " MERGE into " + strTable + " Using (" + " select distinct ZENUMS.objuid , ZENUMS.objname from ZENUMS where " + " Projectname='" + strProject + "' and deliveryid='" + strDelivery + "'" + " and ZENUMS.objdefuid in ('EnumEnum','EnumListType')) a " + " On (a.objuid=" + strTable + ".PROPERTYVALUE " + " and Projectname='" + strProject + "' and deliveryid='" + strDelivery + "')" + " When MATCHED Then UPDATE Set " + strTable + ".PROPERTYVALUEDesc = a.objname";
            }
            else
            {
                strSQL = @"
With T AS
(select distinct ZENUMS.objuid, ZENUMS.objname

    from ZENUMS where Projectname = '" + strProject + @"' and deliveryid ='" + strDelivery + @"' and ZENUMS.objdefuid in ('EnumEnum', 'EnumListType')
)

Update trget

SET trget.PROPERTYVALUEDesc = a.objname

FROM  " + strTable + @" trget
INNER JOIN T a on(a.objuid= trget.PROPERTYVALUE  and trget.Projectname= '" + strProject + "' and trget.deliveryid= '" + strDelivery + @"');";
            }
            if (oraTargetConn!=null)
                dbExecuteHelper.InsertData(DataBaseType.Oracle, strSQL, oraTargetConn);
            else
                dbExecuteHelper.InsertData(DataBaseType.SQLServer, strSQL, SqlTargetConn);
        }


        public void UpdateIsLatesttagCorrelation(string strTable, string strProject, string strDelivery, string strClassification) // schemaobjects
        {
            string strSQL;
            if (oraTargetConn != null)
                strSQL = "MERGE into " + strTable + " Using  ( " + " Select Tag_Name,PROPERTYDEF,CLASSIFICATION,max(tagcreationdate) As CreationDate from " + strTable + " where Projectname='" + strProject + "' and deliveryid='" + strDelivery + "'" + " group by Tag_Name,PROPERTYDEF)a " + " ON (" + strTable + ".Tag_Name = a.Tag_Name And " + strTable + ".propertydef = a.PROPERTYDEF And " + strTable + ".CLASSIFICATION =a.CLASSIFICATION And " + strTable + ".tagcreationdate=a.CreationDate And  " + strTable + ".Projectname ='" + strProject + "' and " + strTable + ".deliveryid='" + strDelivery + "'  ) " + " WHEN MATCHED THEN UPDATE SET " + strTable + ".islatest = 'True'";

            else
            { 
            strSQL = @"
With T AS
(Select Tag_Name, PROPERTYDEF, max(tagcreationdate) As CreationDate from " + strTable + " where Projectname = '" + strProject + "' and deliveryid = '" + strDelivery + @"' group by Tag_Name, PROPERTYDEF)

Update trget

SET trget.islatest = 'True'

FROM " + strTable + @" trget
INNER JOIN T a on (trget.Tag_Name = a.Tag_Name And
   trget .propertydef = a.PROPERTYDEF And trget.tagcreationdate= a.CreationDate And
   trget.Projectname = '" + strProject + "' and trget.deliveryid= '" + strDelivery + @"')";
        }

            if (oraTargetConn!=null)
                dbExecuteHelper.InsertData(DataBaseType.Oracle, strSQL, oraTargetConn);
            else
                dbExecuteHelper.InsertData(DataBaseType.SQLServer, strSQL, SqlTargetConn);
        }

        // ########
        public void CreateOracleTableWithSQL(string strQry)
        {
            if (oraTargetConn!=null)
                dbExecuteHelper.InsertData(DataBaseType.Oracle, strQry, oraTargetConn);
            else
                dbExecuteHelper.InsertData(DataBaseType.SQLServer, strQry, SqlTargetConn);
        }

        public void CreateOracleTable(string strTable, string strQry) // schemaobjects
        {
            strQry = "create table " + strTable + " as " + strQry; // " as select * from ZProjects where 1=0"

            if (oraTargetConn != null)
                dbExecuteHelper.InsertData(DataBaseType.Oracle, strQry, oraTargetConn);
            else
                dbExecuteHelper.InsertData(DataBaseType.SQLServer, strQry, SqlTargetConn);
        }
        public void InsertRecsToDataBase(string strSQL)
        {
            if (oraTargetConn != null)
                dbExecuteHelper.InsertData(DataBaseType.Oracle, strSQL, oraTargetConn);
            else
                dbExecuteHelper.InsertData(DataBaseType.SQLServer, strSQL, SqlTargetConn);
        }


        public double ProgressStatus(string strSQL, bool blnTarget)
        {
            if (oraTargetConn != null || oraSourceConn != null)
             return   ProgressStatusOrcl( strSQL, blnTarget);
            else
                return ProgressStatusSqlServer(strSQL, blnTarget);
         
        }

        private double ProgressStatusOrcl(string strSQL, bool blnTarget)
        {
            double dblRowCount = 0;
            OracleDataReader dReader = null/* TODO Change to default(_) if this is not a reference type */;
            OracleCommand cmdProj = new OracleCommand();
            DataTable dt = new DataTable();
            if (blnTarget == true)

                cmdProj.Connection = oraTargetConn;
            else
                cmdProj.Connection = oraSourceConn;

            cmdProj.CommandText = strSQL;
            dblRowCount = (double)cmdProj.ExecuteScalar();
            return dblRowCount;
        }

        private double ProgressStatusSqlServer(string strSQL, bool blnTarget)
        {
            double dblRowCount = 0;
            SqlDataReader dReader = null/* TODO Change to default(_) if this is not a reference type */;
            SqlCommand cmdProj = new SqlCommand();
            DataTable dt = new DataTable();
            if (blnTarget == true)
            {
                if (SqlTargetConn.State == ConnectionState.Closed)
                    SqlTargetConn.Open();
                cmdProj.Connection = SqlTargetConn;
            }
                
            else
            {
                if (SqlSourceConn.State == ConnectionState.Closed)
                    SqlSourceConn.Open();
                cmdProj.Connection = SqlSourceConn;
            }
                

            cmdProj.CommandText = strSQL;
            var test = cmdProj.ExecuteScalar();
            dblRowCount = (double)Decimal.Parse(test.ToString());
            if (blnTarget == true)
            {               
                    SqlTargetConn.Close();                
            }
            else
            {                
                    SqlSourceConn.Close();                
            }
            return dblRowCount;
        }

        public double CountOfSQLResult(string strSQL)
        {
            if (oraTargetConn != null)
                return dbExecuteHelper.CountOfSQLQueryResult(DataBaseType.Oracle, strSQL, oraSourceConn);
            else
                return dbExecuteHelper.CountOfSQLQueryResult(DataBaseType.SQLServer, strSQL, SqlSourceConn);
        }


        public bool DataBaseTableExists(string strTable) 
        {
            if (oraTargetConn != null)
                return dbExecuteHelper.TableExists(DataBaseType.Oracle, oraTargetConn, strTable);
            else
                return dbExecuteHelper.TableExists(DataBaseType.SQLServer, SqlTargetConn, strTable);
        }

        public void ExportSchemaObject(string strDELIVERYID, string strPROJECT, string strQuery, string strTable, string strClassification, string gstrFileName)
        {
            if (oraTargetConn != null && oraSourceConn !=null)
                dbExecuteHelper.ExportSchemaObject(DataBaseType.Oracle, strDELIVERYID, strPROJECT, strQuery, strTable, strClassification, oraSourceConn, oraTargetConn, gstrFileName);
            else
                dbExecuteHelper.ExportSchemaObject(DataBaseType.SQLServer, strDELIVERYID, strPROJECT, strQuery, strTable, strClassification, SqlSourceConn, SqlTargetConn, gstrFileName);
        }

        public string CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn(string strProjectName, string strDELIVERYID, string strSO_CDW)
        {
            if (oraTargetConn != null)
                return dbExecuteHelper.CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn(DataBaseType.Oracle, oraTargetConn, strProjectName, strDELIVERYID, strSO_CDW);
            else
                return dbExecuteHelper.CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn(DataBaseType.SQLServer, SqlTargetConn, strProjectName, strDELIVERYID, strSO_CDW);
        }
        public string InsertCorrelatedTablesSQL_DistinctTagName(string strProjectName, string strDELIVERYID, string strSO_CDW)
        {
            if (oraTargetConn != null)
                return dbExecuteHelper.InsertCorrelatedTablesSQL_DistinctTagName(DataBaseType.Oracle, oraTargetConn, strProjectName, strDELIVERYID, strSO_CDW);
            else
                return dbExecuteHelper.InsertCorrelatedTablesSQL_DistinctTagName(DataBaseType.SQLServer, SqlTargetConn, strProjectName, strDELIVERYID, strSO_CDW);
        }

        public string CreateSOAndCDWInconsistencyTableSQL(string strProjectName, string strDELIVERYID, Boolean isCdw, string strDynTable)
        {
            if (oraTargetConn != null)
                return dbExecuteHelper.CreateSOAndCDWInconsistencyTableSQL(DataBaseType.Oracle, oraTargetConn, strProjectName, strDELIVERYID,  isCdw, strDynTable);
            else
                return dbExecuteHelper.CreateSOAndCDWInconsistencyTableSQL(DataBaseType.SQLServer, SqlTargetConn, strProjectName, strDELIVERYID,  isCdw, strDynTable);



        }

        public void InsertSOAndCDWInconsistencyTableSQL(string strDynTable, string strProjectName, string strDELIVERYID,Boolean iscdw)
        {
            if (oraTargetConn != null)
                 dbExecuteHelper.InsertSOAndCDWInconsistencyTableSQL(DataBaseType.Oracle, oraTargetConn, strDynTable, strProjectName, strDELIVERYID,iscdw);
            else
                 dbExecuteHelper.InsertSOAndCDWInconsistencyTableSQL(DataBaseType.SQLServer, SqlTargetConn, strDynTable, strProjectName, strDELIVERYID,iscdw);
        }

        public  void ExportExcelToDB()
        {
            bool tableExists = DataBaseTableExists("ExcelImportData");

            ExcelImportHelper importHelper = new ExcelImportHelper();
           var excelSheetContent= importHelper.OpenExcelFileDialog();

            if(excelSheetContent!=null)
            {
                if (!tableExists)
                {
                    string tableCreationSQL = excelSheetContent.FormTableCreateStatementSQL();
                    CreateOracleTableWithSQL(tableCreationSQL);
                }

                excelSheetContent.AddDataToTable(oraTargetConn, SqlTargetConn);
            }
           



        }
    }

}
