﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Demo
{
    public class ReadSQLs
    {
        public void FetchQueriesFromXML(string gstrSQLPath, ref string strSQLID, ref string strSQL)
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(gstrSQLPath);
            XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/Query/SQLs");

            foreach (XmlNode node in nodes)
            {
                if (strSQLID == "")
                    strSQLID = node.SelectSingleNode("SQLID").InnerText;
                else
                    strSQLID = strSQLID + "|" + node.SelectSingleNode("SQLID").InnerText;

                if (strSQL == "")
                    strSQL = node.SelectSingleNode("SQLQuery").InnerText;
                else
                    strSQL = strSQL + "|" + node.SelectSingleNode("SQLQuery").InnerText;
            }
        }
    }
}
