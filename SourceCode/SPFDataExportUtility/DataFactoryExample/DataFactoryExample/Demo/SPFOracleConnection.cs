﻿using Oracle.DataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Demo
{
    public class SPFOracleConnection
    {
        public SPFOracleConnection(string userName,string passWord,string dataSource)
        {
            UserName = userName;
            PassWord = passWord;
            DataSource = dataSource;
        }

        public string UserName { get; }
        public string PassWord { get; }
        public string DataSource { get; }

        public OracleConnection EstablishSPFConnection(string strDataSource, string strUser, string strPwd)
        {
            string strOraDB;
            OracleConnection oraCon = default;
            strOraDB = "data source= " + strDataSource + "; user id= " + strUser + ";password = " + strPwd + ";";
            oraCon = new OracleConnection(strOraDB);
            // "user id=adgas_data;password=oracle;data source=adg"
            try
            {
                oraCon.Open();
                return oraCon;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
                if (oraCon.State == ConnectionState.Open)
                {
                    oraCon.Close();
                }
            }
            finally
            {
            }

            return oraCon;
        }

    }
}
