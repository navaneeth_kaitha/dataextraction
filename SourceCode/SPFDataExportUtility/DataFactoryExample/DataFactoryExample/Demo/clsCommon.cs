﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Text;
using System.Threading.Tasks;

using System.Xml;

using System.Security.Cryptography;
using Oracle.DataAccess.Client;
using System.Data.SqlClient;
using System.Data;

namespace Demo
{
   

    public class clsCommon
    {
       
         
        public OracleConnection OpenOracleConnection(string strDataSource, string strUser, string strPwd)
        {
            string strOraDB;
          // strDataSource="(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.13.147)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = SPF19C)))";
            OracleConnection oraCon = null/* TODO Change to default(_) if this is not a reference type */;
            strOraDB = "data source= " + strDataSource + "; user id= " + strUser + ";password = " + strPwd + ";";

            oraCon = new OracleConnection(strOraDB);

            try
            {
                oraCon.Open();

                return oraCon;
            }
            catch (Exception ex)
            {
               

                if (oraCon.State == ConnectionState.Open)
                {
                    oraCon.Close();
                }
                throw ex;
            }

            finally
            {
            }
            return oraCon;
        }
        public SqlConnection OpenSqlConnection(string strDataSource, string strInitialCatalog, string strUser, string strPwd)
        {
            string strsqlDB;
            SqlConnection sqlConn = null;


            strsqlDB = "Data Source=" + strDataSource + ";Initial Catalog=" + strInitialCatalog + ";Persist Security Info=True;User ID=" + strUser + ";Password=" + strPwd + "; MultipleActiveResultSets=True;";


            sqlConn = new SqlConnection(strsqlDB);

            try
            {
                sqlConn.Open();

                return sqlConn;
            }
            catch (Exception ex)
            {
               
                throw ex;

               
            }

            finally
            {
                sqlConn.Close();
            }
          
        }
        public void WriteLog(string strLog,string gstrFileName)
        {
            StreamWriter swDataExportLog =System.IO.File.AppendText(gstrFileName);
            swDataExportLog.WriteLine(DateTime.Now + " " + strLog);
            swDataExportLog.Close();
        }

        public string EncryptPassword(string pstrPassword)
        {
            byte[] clearBytes = Encoding.Unicode.GetBytes(pstrPassword);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes("ADRY2016SPF2016", new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    pstrPassword = Convert.ToBase64String(ms.ToArray());
                }
            }
            return pstrPassword;
        }

        public string DecryptPassword(string pstrPassword)
        {
            byte[] cipherBytes = Convert.FromBase64String(pstrPassword);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes("ADRY2016SPF2016", new byte[] { 0x49, 0x76, 0x61, 0x6E, 0x20, 0x4D, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    pstrPassword = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return pstrPassword;
        }

        public bool FetchSPFAPIConnections(string strSPFSystemName, ref string strSPFServer, ref string strSPFVirtualDirectory)
        {
            //String gstrSPFConfig = Application.StartupPath & "\SPFSystemConfig.xml";
            XmlDocument xmlDoc = new XmlDocument();
            bool blnRet = false;
            string strSPFSystem;

          //  xmlDoc.Load(gstrSPFConfig); // 
            XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/SPFSystems/SPFSystem");
            foreach (XmlNode node in nodes)
            {
                strSPFSystem = node.SelectSingleNode("Name").InnerText;
                if (strSPFSystem == strSPFSystemName)
                {
                    strSPFServer = node.SelectSingleNode("SpfServerName").InnerText;
                    strSPFVirtualDirectory = node.SelectSingleNode("SpfWebDirectory").InnerText;
                    blnRet = true;
                    return blnRet;
                }
            }
            return blnRet;
        }
    }

}
