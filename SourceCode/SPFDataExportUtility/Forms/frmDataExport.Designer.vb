﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmDataExport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.btnExport = New System.Windows.Forms.Button()
        Me.btlClose = New System.Windows.Forms.Button()
        Me.gbTargetDB = New System.Windows.Forms.GroupBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Txt_InitialCatalog = New System.Windows.Forms.TextBox()
        Me.CmbDataSourceType = New System.Windows.Forms.ComboBox()
        Me.LblDataSourceType = New System.Windows.Forms.Label()
        Me.lblTargetDBConn = New System.Windows.Forms.Label()
        Me.btnConnect = New System.Windows.Forms.Button()
        Me.lblTargetDBConnection = New System.Windows.Forms.Label()
        Me.lblTargetDBPwd = New System.Windows.Forms.Label()
        Me.lblTargetDBUser = New System.Windows.Forms.Label()
        Me.txtTargetDBConnection = New System.Windows.Forms.TextBox()
        Me.txtTargetDBPwd = New System.Windows.Forms.TextBox()
        Me.txtTargetDBUser = New System.Windows.Forms.TextBox()
        Me.btnAddProject = New System.Windows.Forms.Button()
        Me.gbProject = New System.Windows.Forms.GroupBox()
        Me.btnExcelToDB = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.RadCDW = New System.Windows.Forms.RadioButton()
        Me.RadSO = New System.Windows.Forms.RadioButton()
        Me.lblProject = New System.Windows.Forms.Label()
        Me.btnAddDelivery = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cboMonth = New System.Windows.Forms.ComboBox()
        Me.cboProject = New System.Windows.Forms.ComboBox()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.gbTargetDB.SuspendLayout()
        Me.gbProject.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnExport
        '
        Me.btnExport.Location = New System.Drawing.Point(280, 117)
        Me.btnExport.Margin = New System.Windows.Forms.Padding(2)
        Me.btnExport.Name = "btnExport"
        Me.btnExport.Size = New System.Drawing.Size(56, 23)
        Me.btnExport.TabIndex = 4
        Me.btnExport.Text = "Export"
        Me.btnExport.UseVisualStyleBackColor = True
        '
        'btlClose
        '
        Me.btlClose.Location = New System.Drawing.Point(604, 164)
        Me.btlClose.Margin = New System.Windows.Forms.Padding(2)
        Me.btlClose.Name = "btlClose"
        Me.btlClose.Size = New System.Drawing.Size(56, 23)
        Me.btlClose.TabIndex = 5
        Me.btlClose.Text = "Close"
        Me.btlClose.UseVisualStyleBackColor = True
        '
        'gbTargetDB
        '
        Me.gbTargetDB.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.gbTargetDB.Controls.Add(Me.Label3)
        Me.gbTargetDB.Controls.Add(Me.Txt_InitialCatalog)
        Me.gbTargetDB.Controls.Add(Me.CmbDataSourceType)
        Me.gbTargetDB.Controls.Add(Me.LblDataSourceType)
        Me.gbTargetDB.Controls.Add(Me.lblTargetDBConn)
        Me.gbTargetDB.Controls.Add(Me.btnConnect)
        Me.gbTargetDB.Controls.Add(Me.lblTargetDBConnection)
        Me.gbTargetDB.Controls.Add(Me.lblTargetDBPwd)
        Me.gbTargetDB.Controls.Add(Me.lblTargetDBUser)
        Me.gbTargetDB.Controls.Add(Me.txtTargetDBConnection)
        Me.gbTargetDB.Controls.Add(Me.txtTargetDBPwd)
        Me.gbTargetDB.Controls.Add(Me.txtTargetDBUser)
        Me.gbTargetDB.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.gbTargetDB.Location = New System.Drawing.Point(10, 14)
        Me.gbTargetDB.Name = "gbTargetDB"
        Me.gbTargetDB.Size = New System.Drawing.Size(307, 178)
        Me.gbTargetDB.TabIndex = 6
        Me.gbTargetDB.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(13, 90)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(70, 13)
        Me.Label3.TabIndex = 22
        Me.Label3.Text = "Initial Catalog"
        '
        'Txt_InitialCatalog
        '
        Me.Txt_InitialCatalog.Location = New System.Drawing.Point(105, 90)
        Me.Txt_InitialCatalog.Name = "Txt_InitialCatalog"
        Me.Txt_InitialCatalog.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.Txt_InitialCatalog.Size = New System.Drawing.Size(182, 20)
        Me.Txt_InitialCatalog.TabIndex = 21
        Me.Txt_InitialCatalog.Text = "RRWEST71"
        '
        'CmbDataSourceType
        '
        Me.CmbDataSourceType.FormattingEnabled = True
        Me.CmbDataSourceType.Location = New System.Drawing.Point(106, 149)
        Me.CmbDataSourceType.Name = "CmbDataSourceType"
        Me.CmbDataSourceType.Size = New System.Drawing.Size(121, 21)
        Me.CmbDataSourceType.TabIndex = 20
        Me.CmbDataSourceType.Text = "oracle"
        '
        'LblDataSourceType
        '
        Me.LblDataSourceType.AutoSize = True
        Me.LblDataSourceType.Location = New System.Drawing.Point(13, 149)
        Me.LblDataSourceType.Name = "LblDataSourceType"
        Me.LblDataSourceType.Size = New System.Drawing.Size(68, 13)
        Me.LblDataSourceType.TabIndex = 18
        Me.LblDataSourceType.Text = "Source Type"
        '
        'lblTargetDBConn
        '
        Me.lblTargetDBConn.AutoSize = True
        Me.lblTargetDBConn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTargetDBConn.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblTargetDBConn.Location = New System.Drawing.Point(17, 0)
        Me.lblTargetDBConn.Name = "lblTargetDBConn"
        Me.lblTargetDBConn.Size = New System.Drawing.Size(170, 13)
        Me.lblTargetDBConn.TabIndex = 17
        Me.lblTargetDBConn.Text = "Target Database Connection"
        '
        'btnConnect
        '
        Me.btnConnect.Location = New System.Drawing.Point(235, 149)
        Me.btnConnect.Name = "btnConnect"
        Me.btnConnect.Size = New System.Drawing.Size(56, 23)
        Me.btnConnect.TabIndex = 7
        Me.btnConnect.Text = "Connect"
        Me.btnConnect.UseVisualStyleBackColor = True
        '
        'lblTargetDBConnection
        '
        Me.lblTargetDBConnection.AutoSize = True
        Me.lblTargetDBConnection.Location = New System.Drawing.Point(13, 119)
        Me.lblTargetDBConnection.Name = "lblTargetDBConnection"
        Me.lblTargetDBConnection.Size = New System.Drawing.Size(61, 13)
        Me.lblTargetDBConnection.TabIndex = 5
        Me.lblTargetDBConnection.Text = "Connection"
        '
        'lblTargetDBPwd
        '
        Me.lblTargetDBPwd.AutoSize = True
        Me.lblTargetDBPwd.Location = New System.Drawing.Point(13, 55)
        Me.lblTargetDBPwd.Name = "lblTargetDBPwd"
        Me.lblTargetDBPwd.Size = New System.Drawing.Size(53, 13)
        Me.lblTargetDBPwd.TabIndex = 4
        Me.lblTargetDBPwd.Text = "Password"
        '
        'lblTargetDBUser
        '
        Me.lblTargetDBUser.AutoSize = True
        Me.lblTargetDBUser.Location = New System.Drawing.Point(13, 28)
        Me.lblTargetDBUser.Name = "lblTargetDBUser"
        Me.lblTargetDBUser.Size = New System.Drawing.Size(78, 13)
        Me.lblTargetDBUser.TabIndex = 3
        Me.lblTargetDBUser.Text = "Database User"
        '
        'txtTargetDBConnection
        '
        Me.txtTargetDBConnection.Location = New System.Drawing.Point(106, 119)
        Me.txtTargetDBConnection.Name = "txtTargetDBConnection"
        Me.txtTargetDBConnection.Size = New System.Drawing.Size(181, 20)
        Me.txtTargetDBConnection.TabIndex = 2
        Me.txtTargetDBConnection.Text = "SPF12C"
        '
        'txtTargetDBPwd
        '
        Me.txtTargetDBPwd.Location = New System.Drawing.Point(106, 55)
        Me.txtTargetDBPwd.Name = "txtTargetDBPwd"
        Me.txtTargetDBPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtTargetDBPwd.Size = New System.Drawing.Size(181, 20)
        Me.txtTargetDBPwd.TabIndex = 1
        Me.txtTargetDBPwd.Text = "oracle"
        '
        'txtTargetDBUser
        '
        Me.txtTargetDBUser.Location = New System.Drawing.Point(106, 28)
        Me.txtTargetDBUser.Name = "txtTargetDBUser"
        Me.txtTargetDBUser.Size = New System.Drawing.Size(181, 20)
        Me.txtTargetDBUser.TabIndex = 0
        Me.txtTargetDBUser.Text = "adgas_data"
        '
        'btnAddProject
        '
        Me.btnAddProject.Location = New System.Drawing.Point(258, 21)
        Me.btnAddProject.Name = "btnAddProject"
        Me.btnAddProject.Size = New System.Drawing.Size(78, 23)
        Me.btnAddProject.TabIndex = 8
        Me.btnAddProject.Text = "Add Project"
        Me.btnAddProject.UseVisualStyleBackColor = True
        '
        'gbProject
        '
        Me.gbProject.Controls.Add(Me.btnExcelToDB)
        Me.gbProject.Controls.Add(Me.GroupBox1)
        Me.gbProject.Controls.Add(Me.lblProject)
        Me.gbProject.Controls.Add(Me.btnExport)
        Me.gbProject.Controls.Add(Me.btnAddDelivery)
        Me.gbProject.Controls.Add(Me.Label2)
        Me.gbProject.Controls.Add(Me.Label1)
        Me.gbProject.Controls.Add(Me.btnAddProject)
        Me.gbProject.Controls.Add(Me.cboMonth)
        Me.gbProject.Controls.Add(Me.cboProject)
        Me.gbProject.Enabled = False
        Me.gbProject.Location = New System.Drawing.Point(317, 11)
        Me.gbProject.Margin = New System.Windows.Forms.Padding(2)
        Me.gbProject.Name = "gbProject"
        Me.gbProject.Padding = New System.Windows.Forms.Padding(2)
        Me.gbProject.Size = New System.Drawing.Size(343, 145)
        Me.gbProject.TabIndex = 10
        Me.gbProject.TabStop = False
        '
        'btnExcelToDB
        '
        Me.btnExcelToDB.Location = New System.Drawing.Point(58, 117)
        Me.btnExcelToDB.Margin = New System.Windows.Forms.Padding(2)
        Me.btnExcelToDB.Name = "btnExcelToDB"
        Me.btnExcelToDB.Size = New System.Drawing.Size(88, 23)
        Me.btnExcelToDB.TabIndex = 16
        Me.btnExcelToDB.Text = "Excel Import"
        Me.btnExcelToDB.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadCDW)
        Me.GroupBox1.Controls.Add(Me.RadSO)
        Me.GroupBox1.Location = New System.Drawing.Point(154, 80)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(180, 32)
        Me.GroupBox1.TabIndex = 12
        Me.GroupBox1.TabStop = False
        '
        'RadCDW
        '
        Me.RadCDW.AutoSize = True
        Me.RadCDW.Location = New System.Drawing.Point(92, 11)
        Me.RadCDW.Margin = New System.Windows.Forms.Padding(2)
        Me.RadCDW.Name = "RadCDW"
        Me.RadCDW.Size = New System.Drawing.Size(83, 17)
        Me.RadCDW.TabIndex = 1
        Me.RadCDW.TabStop = True
        Me.RadCDW.Text = "CDW Model"
        Me.RadCDW.UseVisualStyleBackColor = True
        '
        'RadSO
        '
        Me.RadSO.AutoSize = True
        Me.RadSO.Location = New System.Drawing.Point(5, 11)
        Me.RadSO.Margin = New System.Windows.Forms.Padding(2)
        Me.RadSO.Name = "RadSO"
        Me.RadSO.Size = New System.Drawing.Size(72, 17)
        Me.RadSO.TabIndex = 0
        Me.RadSO.TabStop = True
        Me.RadSO.Text = "SO Model"
        Me.RadSO.UseVisualStyleBackColor = True
        '
        'lblProject
        '
        Me.lblProject.AutoSize = True
        Me.lblProject.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblProject.ForeColor = System.Drawing.SystemColors.Highlight
        Me.lblProject.Location = New System.Drawing.Point(10, 0)
        Me.lblProject.Name = "lblProject"
        Me.lblProject.Size = New System.Drawing.Size(47, 13)
        Me.lblProject.TabIndex = 15
        Me.lblProject.Text = "Project"
        '
        'btnAddDelivery
        '
        Me.btnAddDelivery.Location = New System.Drawing.Point(258, 56)
        Me.btnAddDelivery.Name = "btnAddDelivery"
        Me.btnAddDelivery.Size = New System.Drawing.Size(78, 23)
        Me.btnAddDelivery.TabIndex = 14
        Me.btnAddDelivery.Text = "Add Delivery"
        Me.btnAddDelivery.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(10, 60)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Delivery"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 23)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(40, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Project"
        '
        'cboMonth
        '
        Me.cboMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboMonth.FormattingEnabled = True
        Me.cboMonth.Location = New System.Drawing.Point(58, 58)
        Me.cboMonth.Margin = New System.Windows.Forms.Padding(2)
        Me.cboMonth.Name = "cboMonth"
        Me.cboMonth.Size = New System.Drawing.Size(186, 21)
        Me.cboMonth.TabIndex = 5
        '
        'cboProject
        '
        Me.cboProject.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboProject.FormattingEnabled = True
        Me.cboProject.Location = New System.Drawing.Point(58, 23)
        Me.cboProject.Margin = New System.Windows.Forms.Padding(2)
        Me.cboProject.Name = "cboProject"
        Me.cboProject.Size = New System.Drawing.Size(186, 21)
        Me.cboProject.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btlClose)
        Me.Panel1.Controls.Add(Me.gbProject)
        Me.Panel1.Location = New System.Drawing.Point(4, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(670, 196)
        Me.Panel1.TabIndex = 16
        '
        'frmDataExport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(678, 204)
        Me.Controls.Add(Me.gbTargetDB)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmDataExport"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SPF Data Export"
        Me.gbTargetDB.ResumeLayout(False)
        Me.gbTargetDB.PerformLayout()
        Me.gbProject.ResumeLayout(False)
        Me.gbProject.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnExport As Button
    Friend WithEvents btlClose As Button
    Friend WithEvents gbTargetDB As GroupBox
    Friend WithEvents lblTargetDBConnection As Label
    Friend WithEvents lblTargetDBPwd As Label
    Friend WithEvents lblTargetDBUser As Label
    Friend WithEvents txtTargetDBConnection As TextBox
    Friend WithEvents txtTargetDBPwd As TextBox
    Friend WithEvents txtTargetDBUser As TextBox
    Friend WithEvents btnConnect As Button
    Friend WithEvents btnAddProject As Button
    Friend WithEvents gbProject As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cboMonth As ComboBox
    Friend WithEvents cboProject As ComboBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents RadCDW As RadioButton
    Friend WithEvents RadSO As RadioButton
    Friend WithEvents btnAddDelivery As Button
    Friend WithEvents lblTargetDBConn As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblProject As Label
    Friend WithEvents LblDataSourceType As Label
    Friend WithEvents CmbDataSourceType As ComboBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Txt_InitialCatalog As TextBox
    Friend WithEvents btnExcelToDB As Button
End Class
