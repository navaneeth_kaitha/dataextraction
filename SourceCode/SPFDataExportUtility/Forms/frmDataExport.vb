﻿Imports System.IO
Imports System.Xml
Imports System.IO.File
Imports System
'Imports Oracle.ManagedDataAccess.Client
'Imports Oracle.ManagedDataAccess.Types
Imports Microsoft.Office.Interop
Imports SPF.Client
Imports SPF.Client.Administration
Imports SPFDataExportUtility.SPF.Client.APIs
Imports Oracle.DataAccess.Client
Imports System.Globalization
Imports System.Text
Imports System.Data.SqlClient
Imports DBExecuteHelper
Imports Demo

Public Class frmDataExport

    Dim objReadSql As New ReadSQLs
    Dim objExecSql As New ExecuteSQLs
    Dim objOPEDMS As New ReadDataFromOperationEdms
    Private Sub frmDataExport_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        CmbDataSourceType.Items.Add("SQL Server")
        CmbDataSourceType.Items.Add("Oracle")
        Txt_InitialCatalog.Enabled = False
    End Sub
    Private Sub PopullateProjects()
        Dim cmdOrcl As New OracleCommand
        Dim cmdSql As New SqlCommand
        Dim adaProjOrcl As New OracleDataAdapter
        Dim adaProjSql As New SqlDataAdapter
        Dim dtProj As New DataTable
        Try

            'Dim dr As OracleDataReader

            Dim DS As New DataSet
            If Not IsNothing(oraTargetConn) Then
                cmdOrcl.Connection = oraTargetConn
                cmdOrcl.CommandText = "select PROJID,PROJNAME from zprojects "
                adaProjOrcl.SelectCommand = cmdOrcl
                adaProjOrcl.Fill(dtProj)
            Else
                If (SqlTargetConn.State = ConnectionState.Closed) Then
                    SqlTargetConn.Open()
                End If

                cmdSql.Connection = SqlTargetConn
                    cmdSql.CommandText = "select PROJID,PROJNAME from zprojects "
                    adaProjSql.SelectCommand = cmdSql
                    adaProjSql.Fill(dtProj)
                End If

                cboProject.DisplayMember = "PROJNAME"
            cboProject.ValueMember = "PROJID"
            cboProject.DataSource = dtProj
            'dr = cmd.ExecuteReader
            'cboProject.DataSource = New BindingSource(dr, Nothing)
        Catch ex As Exception

            Throw ex


        Finally
            If SqlTargetConn IsNot Nothing Then
                SqlTargetConn.Close()
            End If
            If oraTargetConn IsNot Nothing Then
                oraTargetConn.Close()
            End If
        End Try




    End Sub

    Private Sub PopullateDelivery()
        Try
            Dim cmdOrcl As New OracleCommand
            Dim adaProjOrcl As New OracleDataAdapter
            Dim cmdSql As New SqlCommand
            Dim adaProjSql As New SqlDataAdapter
            Dim dtProj As New DataTable
            'Dim dr As OracleDataReader


            Dim DS As New DataSet
            If Not IsNothing(oraTargetConn) Then
                cmdOrcl.Connection = oraTargetConn
                cmdOrcl.CommandText = "select DELIVERYNAME,DELIVERYDESC from zdelivery where PROJID='" & cboProject.SelectedValue & "' "
                adaProjOrcl.SelectCommand = cmdOrcl
                adaProjOrcl.Fill(dtProj)

            Else
                If (SqlTargetConn.State = ConnectionState.Closed) Then
                    SqlTargetConn.Open()
                End If


                cmdSql.Connection = SqlTargetConn
                cmdSql.CommandText = "select DELIVERYNAME,DELIVERYDESC from zdelivery where PROJID='" & cboProject.SelectedValue & "' "
                adaProjSql.SelectCommand = cmdSql
                adaProjSql.Fill(dtProj)

            End If

            cboMonth.DisplayMember = "DELIVERYNAME"
            cboMonth.ValueMember = "DELIVERYNAME"
            cboMonth.DataSource = dtProj
            'dr = cmd.ExecuteReader
            'cboProject.DataSource = New BindingSource(dr, Nothing)
        Catch ex As Exception
            Throw ex
        Finally
            If SqlTargetConn IsNot Nothing Then
                SqlTargetConn.Close()
            End If

        End Try



    End Sub



    Private Sub PopullateMonth()
        Dim cmd As New OracleCommand
        Dim adaDelivery As New OracleDataAdapter
        Dim dtDelivery As New DataTable
        'Dim dr As OracleDataReader

        Dim DS As New DataSet
        cmd.Connection = oraTargetConn
        cmd.CommandText = "select DELIVERYID,DELIVERYNAME from ZADDELIVERY "
        adaDelivery.SelectCommand = cmd
        adaDelivery.Fill(dtDelivery)
        cboMonth.DisplayMember = "DELIVERYNAME"
        cboMonth.ValueMember = "DELIVERYID"
        cboMonth.DataSource = dtDelivery
    End Sub
    'P.propertydef = P2.propertydef

    Private Sub LoadEnumValues()
        Dim strSQL As String = ""
        strSQL = "  Select Case distinct s.obid, s.objname, s.objuid, s.objdefuid, s.description, s.domainuid, 
                    pr.propertydefuid as def, pr.strvalue as svalue 
                     From schemaobj s
                    inner Join schemaobjpr pr on pr.objobid=s.obid
                    where s.terminationdate ='9999/12/31-23:59:59:999' and pr.terminationdate='9999/12/31-23:59:59:999'  
                    And s.objdefuid in ('EnumEnum','EnumListType')"

    End Sub
    Private Sub DeleteProjectDelivery(strDELIVERYID As String, strProjectName As String, strTable As String) 'schemaobjects
        Try
            If Not IsNothing(oraTargetConn) Then

                Dim cmdProj As New OracleCommand

                If oraTargetConn.State = ConnectionState.Closed Then
                    oraTargetConn.Open()
                End If

                cmdProj.Connection = oraTargetConn
                cmdProj.CommandText = "delete from " & strTable & "  where deliveryid='" & strDELIVERYID & "' and  projectname='" & strProjectName & "'"
                cmdProj.ExecuteNonQuery()
            Else
                Dim cmdProj As New SqlCommand
                If SqlTargetConn.State = ConnectionState.Closed Then
                    SqlTargetConn.Open()
                End If

                cmdProj.Connection = SqlTargetConn
                cmdProj.CommandText = "delete from " & strTable & "  where deliveryid='" & strDELIVERYID & "' and  projectname='" & strProjectName & "'"
                cmdProj.ExecuteNonQuery()

            End If
        Catch ex As Exception
            Throw ex

        Finally
            If Not IsNothing(oraTargetConn) Then

                oraTargetConn.Close()

            Else
                SqlTargetConn.Close()
            End If


        End Try



    End Sub
    Private Function SameProjectDelivery(strDELIVERYID As String, strProjectName As String) As Boolean

        Dim strQuery As String
        strQuery = "select * from schemaobjects where deliveryid='" & strDELIVERYID & "' and  projectname='" & strProjectName & "'"

        If Not IsNothing(oraTargetConn) Then
            Return dbExecuteHelper.DataExists(DataBaseType.Oracle, oraTargetConn, strQuery)
        Else
            Return dbExecuteHelper.DataExists(DataBaseType.SQLServer, SqlTargetConn, strQuery)
        End If

        'Dim dReader As OracleDataReader = Nothing
        'Dim cmdProj As New OracleCommand
        'cmdProj.Connection = oraTargetConn
        'strQuery = "select * from schemaobjects where deliveryid='" & strDELIVERYID & "' and  projectname='" & strProjectName & "'"
        'dReader = cmdProj.ExecuteReader()
        'If dReader.Read() Then
        '    Return True
        'Else
        '    Return False
        'End If


    End Function
    Private Sub DeleteDataForProjectAndDelivery(strDELIVERYID As String, strProjectName As String, strDynTable As String)

        objCls.WriteLog("Delete Existing Project Delivery..", gstrFileName)
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "ZENUMS")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "schemaobjects")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "MissingImpliedInterface")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "PUBLISHEDDOCUMENTSTATUS")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SPFVIEWFILES")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "FilesNotInVault")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "DocsWithoutMasterorRevs")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "CDW_OPEDMSEqpDocRel")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SO_OPEDMSEqpDocRel")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SO_OPEDMSiNSTRDocRel")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "CDW_OPEDMSiNSTRDocRel")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SCHEMADISPLAYNAME")
        '
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SPFWEBGL")

        DeleteProjectDelivery(strDELIVERYID, strProjectName, "DOCOWNINGGROUP")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "AUTHDOMAIN")

        ' DeleteProjectDelivery(strDELIVERYID, strProjectName, "DOCINSTRREL")
        'DeleteProjectDelivery(strDELIVERYID, strProjectName, "DOCEQUIPREL")

        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SO_CORRELATEDTAGS")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "SO_TAGINCONSITENCYREPORT")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "CDW_CORRELATEDTAGS")
        '
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "PROJECTDATAPROGRESS")
        strDynTable = strDynTable.Replace("-", "")
        DeleteProjectDelivery(strDELIVERYID, strProjectName, "CDW_TAGINCONSITENCYREPORT")

        strDynTable = "SO_CORRELATEDTAGS"
        strDynTable = strDynTable & strProjectName & strDELIVERYID
        strDynTable = "SO_COR"
        Dim strDynInConsTable As String = ""
        If RadCDW.Checked = True Then
            strDynTable = "CDW_COR" & strProjectName
            strDynInConsTable = "CDW_TAGINCONS" & strProjectName
        Else
            strDynTable = "SO_COR" & strProjectName
            strDynInConsTable = "CDW_TAGINCONS" & strProjectName
        End If
        strDynTable = strDynTable & strProjectName
        strDynTable = strDynTable.Replace("-", "")
        If objExecSql.DataBaseTableExists(strDynTable) Then
            DeleteProjectDelivery(strDELIVERYID, strProjectName, strDynTable)
        End If
        If objExecSql.DataBaseTableExists(strDynInConsTable) Then
            DeleteProjectDelivery(strDELIVERYID, strProjectName, strDynInConsTable)
        End If
    End Sub



    Private Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click

        Dim strDynTable As String = ""
        If RadCDW.Checked = False And RadSO.Checked = False Then
            MessageBox.Show("Please select the export is for CDW or SO")
            Exit Sub
        End If

        'Try

        Dim strProjectName As String = cboProject.SelectedValue
        Dim strDELIVERYID As String = cboMonth.SelectedValue
        Dim YesNo As DialogResult

        YesNo = MessageBox.Show("Do you want to proceed data extraction for Project " & strProjectName & " and Delivery " & strDELIVERYID, "", MessageBoxButtons.YesNo)
        If YesNo = DialogResult.No Then
            Exit Sub
        End If
        objCls.WriteLog(" Data Extraction started for Poject:" & strProjectName & " and Deivery:" & strDELIVERYID, gstrFileName)

        ReadSourceAndCreateConnection(strProjectName, strDELIVERYID)
        objCls.WriteLog("Connection to contractor SPF established..", gstrFileName)

        objExecSql.oraSourceConn = oraSourceConn
        objExecSql.oraTargetConn = oraTargetConn
        objExecSql.SqlSourceConn = SqlSourceConn
        objExecSql.SqlTargetConn = SqlTargetConn

        'CountOfSQLResul
        ' GoTo lblTest
        If SameProjectDelivery(strDELIVERYID, strProjectName) = True Then
            objCls.WriteLog("The Project of same delivery already loaded..", gstrFileName)
            Dim dResult As DialogResult
            dResult = MessageBox.Show("The Project of same delivery already loaded.Do you want to clear the existing one", "Data Extraction Utility", MessageBoxButtons.YesNo)
            If dResult = DialogResult.No Then
                objCls.WriteLog("Exit as the Project of same delivery already..", gstrFileName)
                Exit Sub
            ElseIf dResult = DialogResult.Yes Then

                DeleteDataForProjectAndDelivery(strDELIVERYID, strProjectName, strDynTable)



            End If

        End If

        Dim strInsertSQL As String = ""
        Dim strSQuery As String = ""
        Dim strSQueryID As String = ""
        objReadSql.FetchQueriesFromXML(gstrSQLPath, strSQueryID, strSQuery)
        objCls.WriteLog("SQLs Fetched from XML File ..", gstrFileName)
        Dim str As String
        Dim arrQuery() As String
        Dim arrQueryID() As String
        Dim arrClassification() As String

        Dim iCounter As Integer
        Dim strTable As String = ""
        Dim blnUpdCorrLatest As Boolean = False
        Dim blnInconsDynTable As Boolean = False
        Dim blnDynTable As Boolean = False
        Dim strClassification As String = ""
        arrQuery = strSQuery.Split("|")
        arrQueryID = strSQueryID.Split("|")

        For iCounter = 0 To arrQuery.Length - 1
            strTable = ""
            strClassification = ""
            objCls.WriteLog("Loading " & arrQueryID(iCounter) & "...", gstrFileName)
            arrClassification = arrQueryID(iCounter).Split(":")
            Select Case arrClassification(0) 'arrQueryID(iCounter)
                Case "Enums"

                    strTable = "ZENUMS"

                    Exit Select
                Case "OwningGroup"

                    strTable = "DOCOWNINGGROUP"

                    Exit Select
                Case "DisplayName"

                    strTable = "SCHEMADISPLAYNAME"

                    Exit Select

                Case "AuthDomain"

                    strTable = "AUTHDOMAIN"

                    Exit Select
                Case "SchemaObj"

                    strTable = "SCHEMAOBJECTS"
                    strClassification = arrClassification(1)
                    Exit Select
                Case "MissingRealizedInterface"
                    strTable = "MissingImpliedInterface"
                    Exit Select
                Case "PublishedDocProps"
                    strTable = "PUBLISHEDDOCUMENTSTATUS"
                    Exit Select
                Case "ViewFile"
                    strTable = "SPFVIEWFILES"
                    Exit Select

                Case "FilesNotInVault" 'FilesNotInVault
                    strTable = "FilesNotinVault"
                    Exit Select
                Case "WebGL"
                    strTable = "SPFWEBGL"
                    Exit Select
                Case "CDW_OPEDMSEqpDocRel"
                    strTable = "CDW_OPEDMSEqpDocRel"
                    If RadSO.Checked = True Then
                        strTable = ""
                    End If
                    Exit Select
                Case "CDW_OPEDMSInstrDocRel"
                    strTable = "CDW_OPEDMSInstrDocRel"
                    Exit Select
                    If RadSO.Checked = True Then
                        strTable = ""
                    End If
                Case "SO_OPEDMSEqpDocRel"
                    strTable = "SO_OPEDMSEQPDOCREL"
                    If RadCDW.Checked = True Then
                        strTable = ""
                    End If
                    Exit Select
                Case "SO_OPEDMSInstrDocRel"
                    strTable = "SO_OPEDMSINSTRDOCREL"
                    If RadCDW.Checked = True Then
                        strTable = ""
                    End If
                    Exit Select
                Case "DocumentsWithoutMasterorRevisions"
                    strTable = "DocsWithoutMasterorRevs"
                    Exit Select
                Case "SO_CORRELATEDTAGS"
                    strTable = "SO_CORRELATEDTAGS"
                    strClassification = arrClassification(1)
                    blnDynTable = True

                    strDynTable = "SO_COR" & strProjectName
                    strDynTable = strDynTable.Replace("-", "")
                    If RadCDW.Checked = True Then
                        strTable = ""
                    End If
                    Exit Select
                Case "SO_TAGINCONSITENCYREPORT"
                    strTable = "SO_TAGINCONSITENCYREPORT"
                    strClassification = arrClassification(1)
                    blnUpdCorrLatest = True
                    blnInconsDynTable = True
                    If RadCDW.Checked = True Then
                        strTable = ""
                        blnUpdCorrLatest = False
                        blnInconsDynTable = False
                    End If

                    Exit Select
                Case "CDW_CORRELATEDTAGS"
                    strClassification = arrClassification(1)
                    strTable = "CDW_CORRELATEDTAGS"

                    strDynTable = "CDW_COR" & strProjectName
                    strDynTable = strDynTable.Replace("-", "")
                    blnDynTable = True
                    If RadSO.Checked = True Then
                        strTable = ""
                    End If
                    Exit Select
                Case "CDW_TAGINCONSITENCYREPORT"
                    strTable = "CDW_TAGINCONSITENCYREPORT"
                    strClassification = arrClassification(1)
                    blnUpdCorrLatest = True
                    blnInconsDynTable = True
                    If RadSO.Checked = True Then
                        strTable = ""
                        blnUpdCorrLatest = False
                        blnInconsDynTable = False
                    End If
                    Exit Select
                Case Else

                    strTable = arrClassification(0)
            End Select


            If strTable <> "" Then
                objCls.WriteLog("Going to insert  " & arrQueryID(iCounter) & " in " & strTable & "...", gstrFileName)
                objExecSql.ExportSchemaObject(strDELIVERYID, strProjectName, arrQuery(iCounter), strTable, strClassification, gstrFileName)
                objCls.WriteLog("Loaded " & arrQueryID(iCounter) & " in " & strTable & "...", gstrFileName)
            End If




        Next



        '' Start Dynamic Table

        If blnInconsDynTable = True Then
            Dim strDomainFields1 As String = ""



            If RadCDW.Checked = True Then
                strDynTable = "CDW_TAGINCONS"
                strTable = "CDW_TAGINCONSITENCYREPORT"
            Else
                strDynTable = "SO_TAGINCONS"
                strTable = "SO_TAGINCONSITENCYREPORT"
            End If
            strDynTable = strDynTable & strProjectName
            strDynTable = strDynTable.Replace("-", "")
            If blnUpdCorrLatest = True And (strTable = "CDW_TAGINCONSITENCYREPORT" Or strTable = "SO_TAGINCONSITENCYREPORT") Then
                objCls.WriteLog(" Executing update script for the latest inconsistent tags in " & strTable & "...", gstrFileName)
                objExecSql.UpdateIsLatesttagCorrelation(strTable, strProjectName, strDELIVERYID, strClassification)
                objCls.WriteLog(" updated scrpt the latest inconsistent tags in " & strTable & "...", gstrFileName)
                objCls.WriteLog(" Executing update script forupdating enum values " & strTable & "...", gstrFileName)

                objExecSql.UpdateEnumUIDwithName(strTable, strProjectName, strDELIVERYID)
                objCls.WriteLog("  updated  enum values " & strTable & "...", gstrFileName)
                blnUpdCorrLatest = False
                If RadCDW.Checked = True Then
                    strDynTable = "CDW_TAGINCONS"
                Else
                    strDynTable = "SO_TAGINCONS"
                End If
                strDynTable = strDynTable & strProjectName
                strDynTable = strDynTable.Replace("-", "")


            End If
            If objExecSql.DataBaseTableExists(strDynTable) = False Then
                Dim tableCreateDDL As String = objExecSql.CreateSOAndCDWInconsistencyTableSQL(strProjectName, strDELIVERYID, RadCDW.Checked, strDynTable)
                objExecSql.InsertRecsToDataBase(tableCreateDDL)
                ' Dim strQry As String = CreateSOAndCDWInconsistencyTableSQL(strProjectName, strDELIVERYID, strDomainFields1) ' "select * from ZProjects where 1=0"
                'objExecSql.CreateOracleTable(strDynTable, strQry)
            End If
            objExecSql.InsertSOAndCDWInconsistencyTableSQL(strDynTable, strProjectName, strDELIVERYID, RadCDW.Checked)
            ' strInsertSQL = InsertSOAndCDWInconsistencyTableSQL(strDynTable, strProjectName, strDELIVERYID) ', strClassification
            ' objExecSql.InsertRecsToDataBase(strInsertSQL)
            blnInconsDynTable = False
        End If

        Dim strDomainFields As String = ""
        Dim strDomainQry As String = ""
        Dim strSO_CDW As String = ""
        If RadCDW.Checked = True Then
            strDynTable = "CDW_COR"
            strTable = "CDW_CORRELATEDTAGS"
            strSO_CDW = "CDW"
        Else
            strDynTable = "SO_COR"
            strTable = "SO_CORRELATEDTAGS"
            strSO_CDW = "SO"
        End If
        strDynTable = strDynTable & strProjectName
        strDynTable = strDynTable.Replace("-", "")
        strDomainQry = "select distinct domain from  " & strTable
        strDomainFields = ""
        If blnDynTable = True And strTable <> "" Then 'And RadSO.Checked = True 
            If objExecSql.DataBaseTableExists(strDynTable) = False Then

                'Dim strQry As String = CreateCorrelatedTablesSQL_DistinctTagName(strProjectName, strDELIVERYID, strSO_CDW)

                Dim tableCreateDDL As String = objExecSql.CreateCorrelatedInconsistancyTableWithDomainNamesAsColumn(strProjectName, strDELIVERYID, strSO_CDW)
                objExecSql.InsertRecsToDataBase(tableCreateDDL)
            End If
            strDomainFields = ""

            'strInsertSQL = InsertCorrelatedTablesSQL_DistinctTagName(strProjectName, strDELIVERYID, strSO_CDW)
            strInsertSQL = objExecSql.InsertCorrelatedTablesSQL_DistinctTagName(strProjectName, strDELIVERYID, strSO_CDW)
            objExecSql.InsertRecsToDataBase(strInsertSQL) 'Is taken care In the above statement 
            blnDynTable = False
        End If

        If RadCDW.Checked = True Then
            strDynTable = "CDW_COR"
            strTable = "CDW_TAGINCONSITENCYREPORT"
        Else
            strDynTable = "SO_COR"
            strTable = "SO_TAGINCONSITENCYREPORT"
        End If

        '' End Dynamic Table

        'Singleton Queru
        Dim strPlant As String = ""
        objCls.WriteLog("Fetching REGISTEREDTOOLS_Count", gstrFileName)
        Dim strSQL_REGISTEREDTOOLS As String
        Dim REGISTEREDTOOLS_Count As Double
        strSQL_REGISTEREDTOOLS = "select count(*) from pubobj where objdefuid = 'SPFTEFSignature'"
        REGISTEREDTOOLS_Count = objExecSql.ProgressStatus(strSQL_REGISTEREDTOOLS, False)

        objCls.WriteLog("Fetching PUBLISHEDOCUMENTS_Count", gstrFileName)
        Dim strSQL_PUBLISHEDOCUMENTS As String
        Dim PUBLISHEDOCUMENTS_Count As Double
        strSQL_PUBLISHEDOCUMENTS = "Select  count(*) from publisheddocumentstatus"
        PUBLISHEDOCUMENTS_Count = objExecSql.ProgressStatus(strSQL_PUBLISHEDOCUMENTS, True)
        objCls.WriteLog("Fetching FAILEDDOCUMENTS_Count", gstrFileName)
        Dim strSQL_FAILEDDOCUMENTS As String
        Dim FAILEDDOCUMENTS_Count As Double
        strSQL_FAILEDDOCUMENTS = "Select  count(*) from publisheddocumentstatus where DATA_LOAD_STATUS Like '%Failed%'"
        FAILEDDOCUMENTS_Count = objExecSql.ProgressStatus(strSQL_FAILEDDOCUMENTS, True)
        objCls.WriteLog("Fetching PUBDOCSWITHNOVIEWFILE_Count", gstrFileName)
        Dim strSQL_PUBDOCSWITHNOVIEWFILE As String
        Dim PUBDOCSWITHNOVIEWFILE_Count As Double
        strSQL_PUBDOCSWITHNOVIEWFILE = "Select  count(*) from SPFVIEWFILES where Filename Is null"
        PUBDOCSWITHNOVIEWFILE_Count = objExecSql.ProgressStatus(strSQL_PUBDOCSWITHNOVIEWFILE, True)
        objCls.WriteLog("Fetching NO of OPEN DOCUMENTS", gstrFileName)
        Dim strSQL_NOOPENDOCUMENTS As String
        Dim NOOPENDOCUMENTS As Double
        strSQL_NOOPENDOCUMENTS = "Select  count(*) from DOCOWNINGGROUP where owning_group<>'OPEN TO ALL'"
        NOOPENDOCUMENTS = objExecSql.ProgressStatus(strSQL_NOOPENDOCUMENTS, True)

        objCls.WriteLog("Inserting  Progress Status.", gstrFileName)
        objExecSql.InsertProgressStatus(REGISTEREDTOOLS_Count.ToString(), PUBDOCSWITHNOVIEWFILE_Count.ToString(), FAILEDDOCUMENTS_Count.ToString(), PUBDOCSWITHNOVIEWFILE_Count.ToString(), NOOPENDOCUMENTS.ToString(), strProjectName, strDELIVERYID)


        Dim StrBuilderTags As New StringBuilder

        objCls.WriteLog("Load OP EDMS", gstrFileName)
        Dim objSPFReq As SimpleRequest
        'OP EDMS Connection
        ' objSPFReq = objOPEDMS.login("")
        ' StrBuilderTags.Append("*")
        'OP EDMS data Fetch
        'objOPEDMS.GetEquipsFromOperationEdms(StrBuilderTags, objSPFReq)
        'objOPEDMS.GetInstrsFromOperationEdms(StrBuilderTags, objSPFReq)
        'objOPEDMS.GetDocsFromOperationEdms(StrBuilderTags, objSPFReq)
        'objOPEDMS.GetLinesFromOperationEdms(StrBuilderTags, objSPFReq)

        objCls.WriteLog(" Loaded from OP EDMS", gstrFileName)

        objExecSql.UpdateTagTypeDescIn_so_opedmsinstrdocrel(RadCDW.Checked, RadSO.Checked)

        Dim strClassType As String = "EquipmentType"
        Dim strDocTypeRange As String = ""
        Dim iDocTypeRow As Integer = 0
        Dim iDocTypeStarCol As Integer = 0
        Dim iDataRowStart As Integer = 0
        Dim iTagClassCol As Integer = 0
        Dim iTagTypeCol As Integer = 0
        Dim iTagTypeDescCol As Integer = 0

        objCls.WriteLog("  Load Load Excel Tag Type", gstrFileName)
        'Load TagTypes from Excel
        ReadXMLExcelConfig(strClassType, strDocTypeRange, iDocTypeRow, iDocTypeStarCol, iDataRowStart, iTagClassCol, iTagTypeCol, iTagTypeDescCol)
        objCls.WriteLog(" Loaded Load Excel Tag Type", gstrFileName)
        objCls.WriteLog(" Data Extraction Completed for Poject:   " & strProjectName & " And Deivery: " & strDELIVERYID, gstrFileName)

        objCls.WriteLog(" Loaded from Project EDMS", gstrFileName)

        MessageBox.Show("Data Exported completely")

    End Sub

    Private Shared Sub ReadSourceAndCreateConnection(strProjectName As String, strDELIVERYID As String)

        Dim strSQL As String
        Dim dsResult As DataSet
        Dim dtResult As DataTable
        Dim drResult As DataRow
        Dim strSPFUser As String = ""
        Dim strSPFPwd As String = ""
        Dim strSPFConn As String = ""
        Dim strSPFInitialCatalog As String = ""

        strSQL = "select * from ZDelivery where PROJID='" & strProjectName & "' and deliveryname='" & strDELIVERYID & "'"

        If Not IsNothing(oraTargetConn) Then

            dsResult = dbExecuteHelper.QueryData(DataBaseType.Oracle, oraTargetConn, strSQL)
        Else
            dsResult = dbExecuteHelper.QueryData(DataBaseType.SQLServer, SqlTargetConn, strSQL)
        End If

        dtResult = dsResult.Tables(0)

        drResult = dtResult.Rows(0)

        If Not IsNothing(drResult("SPFDBUSER")) Then
            strSPFUser = drResult("SPFDBUSER")
        End If
        If Not IsNothing(drResult("SPFDBPASSWORD")) Then
            strSPFPwd = drResult("SPFDBPASSWORD")
            strSPFPwd = objCls.DecryptPassword(strSPFPwd)
        End If
        If Not IsNothing(drResult("SPFDBCONNECTION")) Then
            strSPFConn = drResult("SPFDBCONNECTION")
        End If
        If Not IsDBNull(drResult("SPFDBInitalCatalog")) Then
            strSPFInitialCatalog = drResult("SPFDBInitalCatalog")
        End If





        'Dim strSPFUser As String = ""
        'Dim strSPFPwd As String = ""
        'Dim strSPFConn As String = ""
        'Dim objProjDR As OracleDataReader
        'objProjDR = objCls.FetchFromDBTable("ZDelivery", " where PROJID='" & strProjectName & "' and deliveryname='" & strDELIVERYID & "'")
        'While objProjDR.Read()



        '    If Not IsNothing(objProjDR.Item("SPFDBUSER")) Then
        '        strSPFUser = objProjDR.Item("SPFDBUSER")
        '    End If
        '    If Not IsNothing(objProjDR.Item("SPFDBPASSWORD")) Then
        '        strSPFPwd = objProjDR.Item("SPFDBPASSWORD")
        '        strSPFPwd = objCls.DecryptPassword(strSPFPwd)
        '    End If
        '    If Not IsNothing(objProjDR.Item("SPFDBCONNECTION")) Then
        '        strSPFConn = objProjDR.Item("SPFDBCONNECTION")
        '    End If
        'End While


        objCls.WriteLog("Connecting to contractor SPF ..", gstrFileName)
        'oraSourceConn = objCls.EstablishSPFConnection(strSPFConn, strSPFUser, strSPFPwd)

        If strSPFInitialCatalog = "" Then
            oraSourceConn = objCls.OpenOracleConnection(strSPFConn, strSPFUser, strSPFPwd)
        Else
            SqlSourceConn = objCls.OpenSqlConnection(strSPFConn, strSPFInitialCatalog, strSPFUser, strSPFPwd)
        End If

    End Sub

    Private Function CreateCorrelatedTablesSQL_DistinctTagName(strProject As String, strDelivery As String, strCDW_or_SO As String) As String
        Dim strTable As String = "" 'SO_CORP7512_HF_4
        Dim strMainTable As String = ""
        If strCDW_or_SO = "SO" Then
            strTable = "SO_COR" & strProject
            strMainTable = "SO_correlatedtags"
        ElseIf strCDW_or_SO = "CDW" Then
            strTable = "CDW_COR" & strProject
            strMainTable = "CDW_correlatedtags"
        End If
        Dim strWhere As String = ""
        strWhere = " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"

        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()
        Dim strDomainFields As String = ""
        Dim strDomainCase As String = ""
        Dim strDomain As String = ""

        Dim strSQLSource As String = ""
        Dim strQuery As String = "select distinct domain from " & strMainTable
        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)


        drSource = cmdSource.ExecuteReader()


        While drSource.Read()

            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainCase = "" Then
                    strDomainFields = strDomain
                    strDomainCase = "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " & strMainTable & " where Tag_Name=o.Tag_Name  and domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                Else
                    strDomainCase = strDomainCase & "," & "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " & strMainTable & "  where Tag_Name=o.Tag_Name  And domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                    strDomainFields = strDomainFields & "," & strDomain
                End If

            End If

        End While


        Dim strInsertSQL As String = " Create table " & strTable & "  as 
                            SELECT t.*
                                FROM (
                                     SELECT o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE,o.projectname,o.deliveryid,o.CLASSIFICATION," & strDomainCase &
                                     "  , ROW_NUMBER() OVER (PARTITION BY o.Tag_Name ORDER BY o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE) rn
                                            FROM " & strMainTable & " o " & strWhere & "
                                   ) t
                                         WHERE rn = 1 and 1=0"



        Return strInsertSQL
    End Function

    Private Function InsertCorrelatedTablesSQL_DistinctTagName(strProject As String, strDelivery As String, strCDW_or_SO As String) As String
        Dim strTable As String = "" 'SO_CORP7512_HF_4
        Dim strMainTable As String = ""
        If strCDW_or_SO = "SO" Then
            strTable = "SO_COR" & strProject
            strMainTable = "SO_correlatedtags"
        ElseIf strCDW_or_SO = "CDW" Then
            strTable = "CDW_COR" & strProject
            strMainTable = "CDW_correlatedtags"
        End If
        Dim strWhere As String = ""
        strWhere = " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"

        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()
        Dim strDomainFields As String = ""
        Dim strDomainCase As String = ""
        Dim strDomain As String = ""

        Dim strSQLSource As String = ""
        Dim strQuery As String = "select distinct domain from " & strMainTable
        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)


        drSource = cmdSource.ExecuteReader()


        While drSource.Read()

            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainCase = "" Then
                    strDomainFields = strDomain
                    strDomainCase = "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " & strMainTable & " where Tag_Name=o.Tag_Name  and domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                Else
                    strDomainCase = strDomainCase & "," & "CASE WHEN o.Tag_Name IN (SELECT  Tag_Name from " & strMainTable & "  where Tag_Name=o.Tag_Name  And domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                    strDomainFields = strDomainFields & "," & strDomain
                End If

            End If

        End While



        Dim strInsertSQL As String = " Insert into " & strTable & "  (Tag_Name,Tag_Class,TAG_TYPE,TAG_SUBTYPE,projectname,deliveryid,CLASSIFICATION," & strDomainFields & " )
                            SELECT t.*
                                FROM (
                                     SELECT o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE,o.projectname,o.deliveryid,o.CLASSIFICATION," & strDomainCase &
                                     "  , ROW_NUMBER() OVER (PARTITION BY o.Tag_Name ORDER BY o.Tag_Name,o.Tag_Class,o.TAG_TYPE,o.TAG_SUBTYPE) rn
                                            FROM " & strMainTable & " o " & strWhere & "
                                   ) t
                                         WHERE rn = 1"



        Return strInsertSQL
    End Function
    Private Function InsertSOCorrelatedTableSQL(strDynTable As String, strProject As String, strDelivery As String, strClassification As String, strDomainFields As String)
        Dim strSQL As String = ""
        Dim strInsertSQL As String = ""
        Dim strWhere As String
        strWhere = " projectname='" & strProject & "' AND DELIVERYID='" & strDelivery & "'"
        ' and CLASSIFICATION='" & strClassification & "'"
        strInsertSQL = CreateSOCorrelatedTableSQL().Replace("1=0", "")
        strInsertSQL = strInsertSQL & strWhere
        strSQL = "insert into " & strDynTable &
            " (Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION," & strDomainFields & ")  " & strInsertSQL 'select PROJID,PROJNAME from ZProjects"
        Return strSQL
    End Function

    Public Function InsertSOCorrelatedTblSQLwithDynamicDomain(strTable As String, strDynTable As String, strQuery As String, strProject As String, strDelivery As String, ByRef strDomainFields As String) As String
        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()
        Dim strPivotDomainFields As String = ""
        Dim strSQLSource As String = ""

        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)

        drSource = cmdSource.ExecuteReader()
        Dim strDomain As String = ""

        Dim intRowCounter As Integer = 0
        Dim strSQL As String = ""
        Dim strDomainCase As String = ""
        Dim strInsertSQL As String = ""
        ' strSQL = "select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,"


        While drSource.Read()
            intRowCounter = intRowCounter + 1
            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainFields = "" Then
                    strDomainFields = strDomain
                    strPivotDomainFields = "'" & strDomain & "' as " & strDomain
                    strDomainFields = strDomain
                    ' strDomainCase = "Case When Tag_Name In (Select  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  And domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "

                Else
                    strDomainFields = strDomainFields & "," & strDomain
                    strPivotDomainFields = strPivotDomainFields & ",'" & strDomain & "' as " & strDomain
                    'strDomainCase = strDomainCase & "," & "Case WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                    '  strDomainFields = strDomainFields & "," & strDomain

                End If

            End If

        End While

        strSQL = " SELECT  Tag_Name,Tag_Class,TAG_TYPE,TAG_SUBTYPE,projectname,deliveryid,CLASSIFICATION," & strDomainFields & " FROM
                    (
                    SELECT Tag_Name,Tag_Class,TAG_TYPE,TAG_SUBTYPE,projectname,deliveryid,CLASSIFICATION, domain,'Y' as Tags
                    FROM " & strTable &
                    " )
                    PIVOT 
                    (
                      max(Tags)
                      FOR domain   IN ("

        strSQL = strSQL + strPivotDomainFields + ") ) "

        Dim strWhere As String = ""
        strWhere = " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"

        strSQL = strSQL & strWhere & " ORDER BY 1"
        strInsertSQL = "insert into " & strDynTable &
            " (Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION," & strDomainFields & ",rn)  " & strSQL 'select PROJID,PROJNAME from ZProjects"
        Return strSQL
        'Return strSQL
    End Function

    Public Function InsertCDWCorrelatedTblSQLwithDynamicDomain(strDynTable As String, strQuery As String, strProject As String, strDelivery As String, ByRef strDomainFields As String) As String
        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()

        Dim strSQLSource As String = ""

        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)

        drSource = cmdSource.ExecuteReader()
        Dim strDomain As String = ""

        Dim intRowCounter As Integer = 0
        Dim strSQL As String = ""
        Dim strDomainCase As String = ""
        Dim strInsertSQL As String = ""

        strSQL = "select distinct CDWobject,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,"
        While drSource.Read()
            intRowCounter = intRowCounter + 1
            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainCase = "" Then
                    strDomainFields = strDomain
                    strDomainCase = "CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  And domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                Else
                    strDomainCase = strDomainCase & "," & "CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject   and domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                    strDomainFields = strDomainFields & "," & strDomain
                End If

            End If

        End While
        Dim strWhere As String = ""
        strWhere = " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        strSQL = strSQL & strDomainCase & " From CDW_correlatedtags CorTag " & strWhere
        strInsertSQL = "insert into " & strDynTable &
            " (CDWobject,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION," & strDomainFields & ")  " & strSQL 'select PROJID,PROJNAME from ZProjects"
        Return strSQL

    End Function
    Public Function CreatSOCorrelatedTblSQLwithDynamicDomain(strTable As String, strDynTable As String, strQuery As String, strProject As String, strDelivery As String, ByRef strDomainFields As String) As String
        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()

        Dim strSQLSource As String = ""
        Dim strPivotDomainFields As String = ""
        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)

        drSource = cmdSource.ExecuteReader()
        Dim strDomain As String = ""

        Dim intRowCounter As Integer = 0
        Dim strSQL As String = ""
        Dim strDomainCase As String = ""
        Dim strInsertSQL As String = ""

        strSQL = " SELECT * FROM
                    (
                    SELECT Tag_Name,Tag_Class,TAG_TYPE,TAG_SUBTYPE,projectname,deliveryid,CLASSIFICATION, domain,'Y' as Tags
                    FROM " & strTable &
                    " )
                    PIVOT 
                    (
                      max(Tags)
                      FOR domain   IN ("

        While drSource.Read()
            intRowCounter = intRowCounter + 1
            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainFields = "" Then
                    strDomainFields = strDomain
                    strPivotDomainFields = "'" & strDomain & "' as " & strDomain
                Else
                    strDomainFields = strDomainFields & "," & strDomain
                    strPivotDomainFields = strPivotDomainFields & ",'" & strDomain & "' as " & strDomain

                End If

            End If

        End While
        strSQL = strSQL + strPivotDomainFields + ") )where 1=0"

        Return strSQL
    End Function

    Public Function CreatCDWCorrelatedTblSQLwithDynamicDomain(strDynTable As String, strQuery As String, strProject As String, strDelivery As String, ByRef strDomainFields As String) As String
        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()

        Dim strSQLSource As String = ""

        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)


        drSource = cmdSource.ExecuteReader()
        Dim strDomain As String = ""

        Dim intRowCounter As Integer = 0
        Dim strSQL As String = ""
        Dim strDomainCase As String = ""
        Dim strInsertSQL As String = ""
        strSQL = "select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,"
        While drSource.Read()
            intRowCounter = intRowCounter + 1
            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainCase = "" Then
                    strDomainFields = strDomain
                    strDomainCase = "CASE WHEN Tag_Name IN (SELECT  Tag_Name from CDW_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                Else
                    strDomainCase = strDomainCase & "," & "Case WHEN Tag_Name IN (SELECT  Tag_Name from CDW_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='" & strDomain & "') THEN 'Y' ELSE 'N' END AS " & strDomain & " "
                    strDomainFields = strDomainFields & "," & strDomain
                End If

            End If

        End While
        strSQL = strSQL & strDomainCase & " From CDW_correlatedtags CorTag Where 1 = 0"

        Return strSQL
    End Function

    Public Function FetchDomainForCDWSOInconsistencyTable(strQuery As String, strProject As String, strDelivery As String, ByRef strDomainFields As String) As String
        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()
        Dim strSQLSource As String = ""

        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource
        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)
        drSource = cmdSource.ExecuteReader()
        Dim strDomain As String = ""
        Dim intRowCounter As Integer = 0
        Dim strSQL As String = ""
        Dim strDomainCase As String = ""
        Dim strInsertSQL As String = ""

        While drSource.Read()
            intRowCounter = intRowCounter + 1
            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainCase = "" Then
                    strDomainFields = strDomain
                Else
                    strDomainFields = strDomainFields & "," & strDomain
                End If
            End If
        End While
        Return strDomainFields
    End Function
    Private Function GenerateCreatSOCorrelatedTableSQL(strProject As String, strDelivery As String) As String
        Dim strSQL As String = ""
        Dim strDomainCase As String = ""
        Dim strDomain As String = ""
        strSQL = "Select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,"
        strDomainCase = "Case When Tag_Name In (Select  Tag_Name from SO_correlatedtags where Tag_Name= CorTag.Tag_Name And domain ='" & strDomain & "') THEN 'Y' ELSE 'N' END AS PID"

        Return strSQL
    End Function
    Private Function CreateSOCorrelatedTableSQL() As String
        Dim strSQL As String = ""
        strSQL = "select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='PID') THEN 'Y' ELSE 'N' END AS PID,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='IIC') THEN 'Y' ELSE 'N' END AS IIC,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='ELE') THEN 'Y' ELSE 'N' END AS ELE,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='SP3D') THEN 'Y' ELSE 'N' END AS SP3D,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='EQD') THEN 'Y' ELSE 'N' END AS EQD,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='PD') THEN 'Y' ELSE 'N' END AS PD,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='EQL') THEN 'Y' ELSE 'N' END AS EQL,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='IML') THEN 'Y' ELSE 'N' END AS IML,
                    CASE WHEN Tag_Name IN (SELECT  Tag_Name from SO_correlatedtags where Tag_Name=CorTag.Tag_Name  and domain='PTF_EQPL_P') THEN 'Y' ELSE 'N' END AS PTF_EQPL_P
                    FROM SO_correlatedtags CorTag where  1=0"
        Return strSQL
    End Function

    Private Function CreateSOAndCDWInconsistencyTableSQL(strProject As String, strDelivery As String, ByRef strDomainFields As String) As String
        Dim strSQL As String = ""
        Dim strTable As String = ""
        Dim drSource As OracleDataReader
        Dim cmdSource As New OracleCommand
        Dim objSourceOracleDataAdapter As New OracleDataAdapter()
        Dim objSourceDatatable As New DataTable()
        If RadCDW.Checked = True Then
            strTable = "CDW_TAGINCONSITENCYREPORT"
        ElseIf RadSO.Checked = True Then
            strTable = "SO_TAGINCONSITENCYREPORT"
        End If

        Dim strSQLSource As String = ""
        Dim strQuery As String = "select distinct domain from " & strTable
        strSQLSource = strQuery & " where Projectname ='" & strProject & "' and deliveryid='" & strDelivery & "'"
        cmdSource.Connection = oraTargetConn
        cmdSource.CommandText = strSQLSource

        objSourceOracleDataAdapter.SelectCommand = cmdSource
        objSourceOracleDataAdapter.Fill(objSourceDatatable)


        drSource = cmdSource.ExecuteReader()
        Dim strDomain As String = ""
        Dim intRowCounter As Integer = 0
        Dim strPivotDomainFields As String = ""

        Dim strInsertSQL As String = ""
        strSQL = "select distinct Tag_Name,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,"
        While drSource.Read()
            intRowCounter = intRowCounter + 1
            If Not IsDBNull(drSource.Item("Domain")) Then
                strDomain = drSource.Item("Domain")
                If strDomainFields = "" Then
                    strDomainFields = strDomain
                    strPivotDomainFields = "'" & strDomain & "' as " & strDomain
                Else
                    strDomainFields = strDomainFields & "," & strDomain
                    strPivotDomainFields = strPivotDomainFields & ",'" & strDomain & "' as " & strDomain
                End If

            End If

        End While

        strSQL = " select CLASSIFICATION,projectname,deliveryid,Tag_Name,propertydef,LatestValueDomain," & strDomainFields &  'PID,ELE,EQD,PTF_EQPL_P,IIC,PD,SP3D,EQL,IML
                    " from
                     (
                     select a.CLASSIFICATION,a.projectname,a.deliveryid,a.ZADDATESTAMP,a.Tag_Name, a.propertydef,a.domain,a.propertyvalueDesc,
                     b.svalue || '  :-  ' ||  b.Domain as LatestValueDomain
                      from  " & strTable & " a 
                     inner join   
                    (
                      select CLASSIFICATION,projectname,deliveryid,ISLATEST,domain,Tag_Name,propertydef as Def , propertyvalueDesc as svalue 
                     from  " & strTable & " 
                      where ISLATEST='True' and projectname ='" & strProject & "' and deliveryid='" & strDelivery & "' and Tag_Name is not null ) b
                     on a.propertydef=b.def and a.Tag_Name=b.Tag_Name and a.propertyvalueDesc<>b.svalue 
                    where a.projectname ='" & strProject & "' and a.deliveryid='" & strDelivery & "' and  a.Tag_Name is not null
                    order by 5,6
                    )
                     pivot
                     (
                     max(propertyvalueDesc) for domain in (" & strPivotDomainFields & ") 
                    ) where  1=0"
        Return strSQL
    End Function
    Private Function CreateCDWCorrelatedTableSQL() As String
        Dim strSQL As String = ""
        strSQL = "select distinct CDWobject,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='PID') THEN 'Y' ELSE 'N' END AS PID,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='IIC') THEN 'Y' ELSE 'N' END AS IIC,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='ELE') THEN 'Y' ELSE 'N' END AS ELE,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='SP3D') THEN 'Y' ELSE 'N' END AS SP3D,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='EQD') THEN 'Y' ELSE 'N' END AS EQD,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='PD') THEN 'Y' ELSE 'N' END AS PD,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='EQL') THEN 'Y' ELSE 'N' END AS EQL,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='IML') THEN 'Y' ELSE 'N' END AS IML,
                    CASE WHEN CDWobject IN (SELECT  CDWobject from CDW_correlatedtags where CDWobject=CorTag.CDWobject  and domain='PTF_EQPL_P') THEN 'Y' ELSE 'N' END AS PTF_EQPL_P                  
                    FROM CDW_correlatedtags CorTag where  1=0"
        Return strSQL
    End Function

    Private Function InsertSOAndCDWInconsistencyTableSQL(strDynTable As String, strProject As String, strDelivery As String) ', strClassification As String
        Dim strTable As String = ""
        Dim strSQL As String = ""
        Dim strInsertSQL As String = ""
        Dim strDomainFields As String = ""
        Dim strWhere As String
        If RadCDW.Checked = True Then
            strTable = "CDW_TAGINCONSITENCYREPORT"
        ElseIf RadSO.Checked = True Then
            strTable = "SO_TAGINCONSITENCYREPORT"
        End If
        strWhere = " projectname='" & strProject & "' AND DELIVERYID='" & strDelivery & "'" ' and CLASSIFICATION='" & strClassification & "'"
        strInsertSQL = CreateSOAndCDWInconsistencyTableSQL(strProject, strDelivery, strDomainFields).Replace("1=0", "")
        strInsertSQL = strInsertSQL & strWhere
        strSQL = "insert into " & strDynTable &
            " (CLASSIFICATION,projectname,deliveryid,Tag_Name,propertydef,LatestValueDomain," & strDomainFields & ")  " & strInsertSQL
        Return strSQL
    End Function

    Private Function InsertCDWCorrelatedTableSQL(strDynTable As String, strProject As String, strDelivery As String, strClassification As String)
        Dim strSQL As String = ""
        Dim strInsertSQL As String = ""
        Dim strWhere As String
        strWhere = " projectname='" & strProject & "' AND DELIVERYID='" & strDelivery & "' and CLASSIFICATION='" & strClassification & "'"
        If RadCDW.Checked = True Then
            strInsertSQL = CreateCDWCorrelatedTableSQL().Replace("1=0", "")
        Else
            strInsertSQL = CreateSOCorrelatedTableSQL().Replace("1=0", "")
        End If

        strInsertSQL = strInsertSQL & strWhere
        strSQL = "insert into " & strDynTable &
            " (CDWobject,Tag_Class,TAGTYPE,projectname,deliveryid,CLASSIFICATION,PID,IIC,ELE,SP3D,EQD,PD,EQL,IML,PTF_EQPL_P)  " & strInsertSQL 'select PROJID,PROJNAME from ZProjects"
        Return strSQL
    End Function
    Private Sub ProjEDMSdataCols(ByRef dt As DataTable)
        'DOCNAME
        Dim colDocName As New DataColumn
        colDocName.DataType = GetType(String)
        colDocName.ColumnName = "DOCNAME"
        dt.Columns.Add(colDocName)

        'DOCDESCRIPTION
        Dim colDocDesc As New DataColumn
        colDocDesc.DataType = GetType(String)
        colDocDesc.ColumnName = "DOCDESCRIPTION"
        dt.Columns.Add(colDocDesc)

        'DOCREV
        Dim colDocRev As New DataColumn
        colDocRev.DataType = GetType(String)
        colDocRev.ColumnName = "DOCREV"
        dt.Columns.Add(colDocRev)

        'DOCTYPE
        Dim colDocType As New DataColumn
        colDocType.DataType = GetType(String)
        colDocType.ColumnName = "DOCTYPE"
        dt.Columns.Add(colDocType)

        'DOCSubTYPE
        Dim colDocSubType As New DataColumn
        colDocSubType.DataType = GetType(String)
        colDocSubType.ColumnName = "DOCSUBTYPE"
        dt.Columns.Add(colDocSubType)
        'DOCISSUEPURPOSE
        Dim colDocIssuePurpose As New DataColumn
        colDocIssuePurpose.DataType = GetType(String)
        colDocIssuePurpose.ColumnName = "DOCISSUEPURPOSE"
        dt.Columns.Add(colDocIssuePurpose)

        'DOCPPROVALCODE
        Dim colDocApprovalCode As New DataColumn
        colDocApprovalCode.DataType = GetType(String)
        colDocApprovalCode.ColumnName = "DOCPPROVALCODE"
        dt.Columns.Add(colDocApprovalCode)
        'DELIVERYID
        Dim colDelivery As New DataColumn
        colDelivery.DataType = GetType(String)
        colDelivery.ColumnName = "DELIVERYID"
        dt.Columns.Add(colDelivery)
        'PROJECTNAME
        Dim colProject As New DataColumn
        colProject.DataType = GetType(String)
        colProject.ColumnName = "PROJECTNAME"
        dt.Columns.Add(colProject)
        'PROJECTNAME
        Dim colZadDateStamp As New DataColumn
        colZadDateStamp.DataType = GetType(Date)
        colZadDateStamp.ColumnName = "ZADDATESTAMP"
        dt.Columns.Add(colZadDateStamp)
    End Sub
    Private Sub DocStatusFromProjectEDMS(strDELIVERYID As String, strProject As String)

        Dim strTable As String = "PEDMS_DOCSTATUS"
        Dim dt As New DataTable(strTable)
        'dt = New DataTable(strTable)

        Dim objTargetAdapter As OracleDataAdapter
        Dim objTargetDS As New DataSet
        Dim cmdTarget As New OracleCommand
        Dim dataRowTarget As DataRow

        cmdTarget.Connection = oraTargetConn

        objTargetAdapter = New OracleDataAdapter
        cmdTarget.CommandText = "select * from " & strTable
        objTargetAdapter.SelectCommand = cmdTarget
        '

        ProjEDMSdataCols(dt)




        Dim strSPFSessionID As String
        Dim strSpfServerName As String = "" '"VMSPFAPP10"
        Dim strSpfWebDirectory As String = "" '"ZADPSServer"
        Dim strSpfUserName As String = Environment.UserName '"superuser1"
        Dim strSpfPassword As String = ""
        Dim lstrServerName As String = System.Configuration.ConfigurationManager.AppSettings.Get("PEDMSServerName")
        Dim lstrWebDirectory As String = System.Configuration.ConfigurationManager.AppSettings.Get("PEDMSWebDirectory")
        Dim Plant As String = System.Configuration.ConfigurationManager.AppSettings.Get("Plant")
        Dim Port As String = System.Configuration.ConfigurationManager.AppSettings.Get("Port")
        Dim IsSecure As String = System.Configuration.ConfigurationManager.AppSettings.Get("IsSecure")
        Dim lobjCustomLogin As ZADCOSPFLogin = New ZADCOSPFLogin(lstrServerName, lstrWebDirectory, Port, Environment.UserName, "", IsSecure)



        strSPFSessionID = lobjCustomLogin.GetSessionID("", Plant, strProject)

        '     Dim lobjServerReq As ServerRequest
        Dim lobjNodeList As Xml.XmlNodeList
        Dim strDocs As String = "" ' "A0-2300-C-1412_001,A0-2300-C-1413_001"

        Dim cmdQuery As New OracleCommand
        Dim drResult As OracleDataReader

        Dim strSQL As String = ""
        strSQL = "select distinct document from publisheddocumentstatus where projectname='" & strProject & "' And deliveryid ='" & strDELIVERYID & "'"
        cmdQuery.Connection = oraTargetConn

        cmdQuery.CommandText = strSQL
        drResult = cmdQuery.ExecuteReader()

        While drResult.Read()
            If strDocs = "" Then
                strDocs = drResult.Item(0)
            Else
                strDocs = strDocs & "," & drResult.Item(0)
            End If

        End While
        Dim mstrSecure As String = CStr(IIf(IsSecure, "https://", "http://"))
        Dim mstrSPEURL As String = mstrSecure & lstrServerName & "/" & lstrWebDirectory & "/ServerRequest.asmx"
        Dim lobjSPERequest As New SimpleRequest(mstrSPEURL, strSPFSessionID)

        lobjSPERequest.ServerAPI = "ZADGetDocStatus"


        lobjSPERequest.UserName = Environment.UserName

        lobjSPERequest.AddQueryElement("DocsToFind", strDocs)
        lobjSPERequest.AddQueryElement("ProjectToQuery", strProject)
        lobjSPERequest.Execute()
        lobjNodeList = lobjSPERequest.Response.SelectNodes("//ProjectInfo")

        Dim lobjNode As XmlNode
        Dim lstrDocName As String
        Dim lstrDocDescription As String
        Dim lstrDocRev As String
        Dim lstrDocType As String
        Dim lstrDocSubType As String
        Dim lstrIssuePurpose As String
        Dim lstrApprovalCode As String
        Dim iBatchUpdate As Integer = 0
        Dim intRow As Integer = 0

        Dim cmdTargetBldr As OracleCommandBuilder

        If Not IsNothing(lobjNodeList) Then
            Dim doc As New XmlDocument
            With lobjNodeList.GetEnumerator
                While .MoveNext
                    lobjNode = .Current


                    lstrDocName = lobjNode.SelectSingleNode("Name").InnerText.ToString
                    lstrDocDescription = lobjNode.SelectSingleNode("Description").InnerText.ToString
                    lstrDocRev = lobjNode.SelectSingleNode("Revision").InnerText.ToString
                    lstrDocType = lobjNode.SelectSingleNode("DocType").InnerText.ToString
                    lstrDocSubType = lobjNode.SelectSingleNode("SubType").InnerText.ToString
                    lstrIssuePurpose = lobjNode.SelectSingleNode("IssuePurpose").InnerText.ToString
                    lstrApprovalCode = lobjNode.SelectSingleNode("ApprovalCode").InnerText.ToString

                    dataRowTarget = dt.NewRow
                    dataRowTarget("DOCNAME") = lstrDocName
                    dataRowTarget("DOCDESCRIPTION") = lstrDocDescription
                    dataRowTarget("DOCREV") = lstrDocRev
                    dataRowTarget("DOCTYPE") = lstrDocType
                    dataRowTarget("DOCSUBTYPE") = lstrDocSubType
                    dataRowTarget("DOCISSUEPURPOSE") = lstrIssuePurpose
                    dataRowTarget("DOCPPROVALCODE") = lstrApprovalCode
                    dataRowTarget("DELIVERYID") = strDELIVERYID
                    dataRowTarget("PROJECTNAME") = strProject
                    Dim strCurrentDate As String
                    strCurrentDate = DateAndTime.Now
                    dataRowTarget("ZADDATESTAMP") = DateTime.ParseExact(strCurrentDate.Substring(0, 10), "MM/dd/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None).ToString("dd-MMM-yy")

                    intRow = intRow + 1
                    iBatchUpdate = iBatchUpdate + 1

                    dt.Rows.Add(dataRowTarget)
                    If iBatchUpdate = 1000000 Then

                        objTargetDS.Tables.Add(dt)

                        objTargetAdapter.Fill(objTargetDS, strTable)
                        cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

                        objTargetAdapter.Update(objTargetDS, strTable)
                        objTargetDS.AcceptChanges()
                        objTargetDS.Tables.Remove(dt)
                        dt = New DataTable(strTable)
                        ProjEDMSdataCols(dt)
                        iBatchUpdate = 0
                    End If


                End While

                If iBatchUpdate > 0 Then
                    objTargetDS.Tables.Add(dt)
                    objTargetAdapter.Fill(objTargetDS, strTable)
                    cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

                    objTargetAdapter.Update(objTargetDS, strTable)
                    objTargetDS.AcceptChanges()
                End If


            End With
        End If
    End Sub

    Private Sub cboProject_SelectedIndexChanged(sender As Object, e As EventArgs)

    End Sub

    Private Sub cboProject_SelectionChangeCommitted(sender As Object, e As EventArgs)

    End Sub


    Private Sub btlClose_Click(sender As Object, e As EventArgs) Handles btlClose.Click




        Me.Close()
    End Sub

    Private Sub btnConnect_Click(sender As Object, e As EventArgs) Handles btnConnect.Click


        objCls.WriteLog("Connecting to Target databae...", gstrFileName)
        If txtTargetDBConnection.Text = "" Or txtTargetDBUser.Text = "" Or txtTargetDBPwd.Text = "" Then
            objCls.WriteLog("Target databae Connection Failed...", gstrFileName)
            MessageBox.Show("Connection info not provided")
            Exit Sub
        End If


        ' Dim oraCon As New OleDb.OleDbConnection  'OracleConnection

        Dim strDataSource As String
        Dim strUser As String
        Dim strPwd As String
        Dim strInitialCatalog As String
        strDataSource = txtTargetDBConnection.Text
        strUser = txtTargetDBUser.Text
        strPwd = txtTargetDBPwd.Text
        DataSourceType = CmbDataSourceType.Text
        strInitialCatalog = Txt_InitialCatalog.Text

        Try




            objCls.WriteLog("Connecting Target DataBase..", gstrFileName)
            If DataSourceType = "Oracle" Then
                oraTargetConn = objCls.OpenOracleConnection(txtTargetDBConnection.Text, txtTargetDBUser.Text, txtTargetDBPwd.Text)

            Else
                If DataSourceType = "SQL Server" Then
                    SqlTargetConn = objCls.OpenSqlConnection(txtTargetDBConnection.Text, strInitialCatalog, txtTargetDBUser.Text, txtTargetDBPwd.Text)
                Else
                    MessageBox.Show("DataSource ..Connection info not provided")
                    Exit Sub
                End If

            End If

            objCls.WriteLog("Target DataBase connection established..", gstrFileName)


            If Not IsNothing(oraTargetConn) Or Not IsNothing(SqlTargetConn) Then
                gbProject.Enabled = True
                objCls.WriteLog("Popullating Projects..", gstrFileName)
                PopullateProjects()
                objCls.WriteLog("Projects Loaded..", gstrFileName)

                btnConnect.Enabled = False
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Data Extraction Utility")

        Finally

        End Try
    End Sub


    Private Sub btnAddProject_Click(sender As Object, e As EventArgs) Handles btnAddProject.Click
        If oraTargetConn Is Nothing And SqlTargetConn Is Nothing Then
            MessageBox.Show("ConnectionState to Temporary Database not Established")
            Exit Sub
        End If
        Dim lobjfrmProjects As frmProject
        lobjfrmProjects = New frmProject
        lobjfrmProjects.ShowDialog()

        PopullateProjects()
    End Sub


    Private Sub releaseObject(ByVal obj As Object)
        Try
            System.Runtime.InteropServices.Marshal.ReleaseComObject(obj)
            obj = Nothing
        Catch ex As Exception
            obj = Nothing
        Finally
            GC.Collect()
        End Try
    End Sub

    Private Function FileExist(strFilePath As String, strFileName As String) As Boolean 'strFileName="" strFilePath=Path\"
        If System.IO.File.Exists(strFilePath & strFileName) Then
            Return True
        Else
            Return False
        End If
    End Function
    '
    Private Sub ReadXMLExcelConfig(strClassType As String, ByRef strDocTypeRange As String, ByRef iDocTypeRow As Integer, ByRef iDocTypeStarCol As Integer, ByRef iDataRowStart As Integer, ByRef iTagClassCol As Integer, ByRef iTagTypeCol As Integer, ByRef iTagTypeDescCol As Integer)
        Dim xmlDoc As New XmlDocument()
        Dim strExcelPath As String = ""
        Dim strExcelSheetName As String = ""
        Dim strClassificationType As String = ""

        xmlDoc.Load(gstrExcelnfo) '
        Dim nodes As XmlNodeList = xmlDoc.DocumentElement.SelectNodes("/TagClassification/TagTypes")
        ' If strClassificationType = strClassType Then
        For Each node As XmlNode In nodes
            strClassificationType = node.SelectSingleNode("ClassType").InnerText
            strExcelPath = node.SelectSingleNode("ExcelPath").InnerText
            strExcelSheetName = node.SelectSingleNode("ExcelSheet").InnerText

            strDocTypeRange = node.SelectSingleNode("DocTypeRange").InnerText
            iDocTypeRow = Val(node.SelectSingleNode("DocTypeRow").InnerText)
            iDocTypeStarCol = Val(node.SelectSingleNode("DocTypeStarCol").InnerText)

            iDataRowStart = Val(node.SelectSingleNode("DataRowStart").InnerText)
            iTagClassCol = Val(node.SelectSingleNode("TagClassCol").InnerText)
            iTagTypeCol = Val(node.SelectSingleNode("TagTypeCol ").InnerText)
            iTagTypeDescCol = Val(node.SelectSingleNode("TagTypeDescCol").InnerText)
            'LoadExcelToDB(strExcelSheetName, strClassType, strDocTypeRange, iDocTypeRow, iDocTypeStarCol, iDataRowStart, iTagClassCol, iTagTypeCol, iTagTypeDescCol)
        Next

        Exit Sub
        'End If


    End Sub




    Public Sub LoadExcelToDB(strExcelSheetName As String, strClassType As String, strDocTypeRange As String, iDocTypeRow As Integer, iDocTypeStarCol As Integer, iDataRowStart As Integer, iTagClassCol As Integer, iTagTypeCol As Integer, iTagTypeDescCol As Integer)

        Dim strExcelPath As String = "D:\CECIL\DocTypesVsTagTypes-All.xlsx"



        Dim oExcelLoc As String = strExcelPath '

        Dim oExcelApp As Object = CreateObject("Excel.Application")
        Dim oWorkBook As Object = oExcelApp.Workbooks.Open(oExcelLoc)
        Dim oWorkSheet As Object = oWorkBook.Worksheets(strExcelSheetName)

        Dim strColTagClass As String = ""
        Dim strColTagType As String = ""
        Dim strColTagDesc As String = ""
        Dim strColDocType As String = ""
        Dim strColDocSubType As String = ""
        Dim strColOptionalMandatory As String = ""

        Dim strDocSubTypeCountColobDocType As String = ""

        Dim range1 As Excel.Range = oWorkSheet.Range(strDocTypeRange)
        Dim iRow As Integer = 0

        For Each cell1 As Excel.Range In range1.Cells

            If Not IsNothing(cell1.Value) Then

                If strDocSubTypeCountColobDocType = "" Then
                    strDocSubTypeCountColobDocType = cell1.MergeArea.Count.ToString() & ":" & cell1.Value
                Else
                    strDocSubTypeCountColobDocType = strDocSubTypeCountColobDocType & "," & cell1.MergeArea.Count.ToString() & ":" & cell1.Value
                End If
            End If
        Next


        Dim arrDocSubTypeCountColobDocType() As String
        Dim arrDocSubTypeCountDocType() As String
        Dim iDocSubTypeCount As Integer = 0

        arrDocSubTypeCountColobDocType = strDocSubTypeCountColobDocType.Split(",")

        Dim strDocType As String = ""
        range1 = oWorkSheet.UsedRange

        Dim objTargetDS As New DataSet
        Dim strTable As String = "TAGTYPEVSDOCTYPE"



        Dim objTargetAdapter As OracleDataAdapter

        Dim cmdTarget As New OracleCommand

        cmdTarget.Connection = oraTargetConn

        objTargetAdapter = New OracleDataAdapter
        cmdTarget.CommandText = "select * from " & strTable
        objTargetAdapter.SelectCommand = cmdTarget


        Dim dt As New DataTable(strTable)
        AddColumns(dt)


        For rCnt = 0 To range1.Rows.Count
            For cCnt = iDocTypeStarCol To range1.Columns.Count - 1
                iDocSubTypeCount = iDocTypeStarCol

                strColDocSubType = oWorkSheet.Cells(iDocTypeRow, cCnt).value
                If iTagClassCol <> 0 Then
                    strColTagClass = oWorkSheet.Cells(iDataRowStart + rCnt, iTagClassCol).value
                End If
                strColTagType = oWorkSheet.Cells(iDataRowStart + rCnt, iTagTypeCol).value
                strColTagDesc = oWorkSheet.Cells(iDataRowStart + rCnt, iTagTypeDescCol).value

                strColOptionalMandatory = oWorkSheet.Cells(iDataRowStart + rCnt, cCnt).value
                If strColOptionalMandatory Is Nothing Then
                    strColOptionalMandatory = ""
                End If
                If strColOptionalMandatory = "Y" Or strColOptionalMandatory.ToUpper = "O" Or strColOptionalMandatory = "ü" Then


                    For count = 0 To arrDocSubTypeCountColobDocType.Length - 1
                        arrDocSubTypeCountDocType = arrDocSubTypeCountColobDocType(count).Split(":")
                        strDocType = arrDocSubTypeCountDocType(1)
                        iDocSubTypeCount = iDocSubTypeCount + arrDocSubTypeCountDocType(0)
                        If cCnt < iDocSubTypeCount Then
                            strColDocType = strDocType
                            If strColOptionalMandatory = "ü" Then
                                strColOptionalMandatory = "O"
                            End If
                            InsertRecords(dt, strColTagClass, strColTagType, strColTagDesc, strColDocType, strColDocSubType, strColOptionalMandatory)
                            Exit For
                        End If


                    Next

                End If
                '

            Next
        Next
        objTargetDS.Tables.Add(dt)
        Dim cmdTargetBldr As OracleCommandBuilder
        objTargetAdapter.Fill(objTargetDS, strTable)
        cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

        objTargetAdapter.Update(objTargetDS, strTable)
        objTargetDS.AcceptChanges()

        oWorkBook.Close()

        'Release object references.
        releaseObject(oWorkSheet)
        releaseObject(oWorkBook)
        releaseObject(oExcelApp)




    End Sub

    Private Sub AddColumns(ByRef dt As DataTable)


        Dim ColTagClass As New DataColumn
        ColTagClass.DataType = GetType(String)
        ColTagClass.ColumnName = "TagClass"
        dt.Columns.Add(ColTagClass)

        Dim ColTagType As New DataColumn
        ColTagType.DataType = GetType(String)
        ColTagType.ColumnName = "TagType"
        dt.Columns.Add(ColTagType)

        Dim ColTagDesc As New DataColumn
        ColTagDesc.DataType = GetType(String)
        ColTagDesc.ColumnName = "TagDesc"
        dt.Columns.Add(ColTagDesc)

        Dim ColDocType As New DataColumn
        ColDocType.DataType = GetType(String)
        ColDocType.ColumnName = "DocType"
        dt.Columns.Add(ColDocType)

        Dim ColDocSubType As New DataColumn
        ColDocSubType.DataType = GetType(String)
        ColDocSubType.ColumnName = "DocSubType"
        dt.Columns.Add(ColDocSubType)

        Dim ColMandatoryorOtional As New DataColumn
        ColMandatoryorOtional.DataType = GetType(String)
        ColMandatoryorOtional.ColumnName = "MandatoryorOtional"
        dt.Columns.Add(ColMandatoryorOtional)



    End Sub
    Private Sub InsertRecords(ByRef dt As DataTable, strTagClass As String, strTagType As String, strTagDesc As String, strDocType As String, strDocSubType As String, strMandatoryorOtional As String) '

        Dim dataRowTarget As DataRow



        dataRowTarget = dt.NewRow
        dataRowTarget("TagClass") = strTagClass
        dataRowTarget("TagType") = strTagType
        dataRowTarget("TagDesc") = strTagDesc
        dataRowTarget("DocType") = strDocType
        dataRowTarget("DocSubType") = strDocSubType
        dataRowTarget("MandatoryorOtional") = strMandatoryorOtional

        dt.Rows.Add(dataRowTarget)



    End Sub

    Private Sub sampleoffset()
        Dim dblRecCount As Double = 0
        Dim dblRecCounter As Double = 0
        Dim strTempSQLSource As String = ""

        Dim cmdSource As New OracleCommand
        Dim drSource As OracleDataReader
        Dim strSQLSource As String = ""


        dblRecCount = objExecSql.CountOfSQLResult("select objname from zenums")
        strTempSQLSource = strSQLSource
        Dim intNo As Integer = 10
        Dim iStart As Double = 0


        While dblRecCounter <= dblRecCount
            If dblRecCounter = 0 Then
                iStart = 0
            Else
                iStart = 1
            End If



            strSQLSource = "select objuid,objname from zenums"
            cmdSource.Connection = oraSourceConn

            iStart = dblRecCounter + iStart
            strSQLSource = "select * from (" & strSQLSource & ") order by 1 OFFSET " & iStart & "  ROWS FETCH NEXT " & intNo & " ROWS ONLY"




            cmdSource.CommandText = strSQLSource

            drSource = cmdSource.ExecuteReader()

            Dim objSourceOracleDataAdapter As New OracleDataAdapter()
            Dim objSourceDatatable As New DataTable()
            objSourceOracleDataAdapter.SelectCommand = cmdSource
            objSourceOracleDataAdapter.Fill(objSourceDatatable)

            drSource = cmdSource.ExecuteReader()
            dblRecCounter = dblRecCounter + 10
        End While
    End Sub

    Private Sub btnAddDelivery_Click(sender As Object, e As EventArgs) Handles btnAddDelivery.Click
        If oraTargetConn Is Nothing And SqlTargetConn Is Nothing Then
            MessageBox.Show("ConnectionState to Temporary Database not Established")
            Exit Sub
        End If
        Dim lobjfrmProjects As frmDelivery
        lobjfrmProjects = New frmDelivery
        lobjfrmProjects.ShowDialog()

        PopullateProjects()
    End Sub

    Private Sub Button1_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, Color.LightSteelBlue, ButtonBorderStyle.Solid)
    End Sub

    Private Sub cboProject_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles cboProject.SelectedIndexChanged

    End Sub

    Private Sub cboProject_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboProject.SelectedValueChanged
        PopullateDelivery()
    End Sub
    Private Sub DynCorr(blnDynTable As Boolean, strProjectName As String, strDELIVERYID As String)
        Dim strDomainFields As String = ""
        Dim strDomainQry As String = ""
        Dim strDynTable As String = ""
        Dim strTable As String = ""
        If RadCDW.Checked = True Then
            strDynTable = "CDW_COR"
            strTable = "CDW_CORRELATEDTAGS"
        Else
            strDynTable = "SO_COR"
            strTable = "SO_CORRELATEDTAGS"
        End If
        strDynTable = strDynTable & strProjectName
        strDynTable = strDynTable.Replace("-", "")
        strDomainQry = "select distinct domain from  " & strTable
        strDomainFields = ""
        If blnDynTable = True And strTable <> "" Then
            If objExecSql.DataBaseTableExists(strDynTable) = False Then

                Dim strQry As String = CreatSOCorrelatedTblSQLwithDynamicDomain(strTable, strDynTable, strDomainQry, strProjectName, strDELIVERYID, strDomainFields) ' CreateSOCorrelatedTableSQL() ' "select * from ZProjects where 1=0"
                objExecSql.CreateOracleTable(strDynTable, strQry)
            End If
            Dim strInsertSQL As String = ""

            strDomainFields = ""
            strInsertSQL = InsertSOCorrelatedTblSQLwithDynamicDomain(strTable, strDynTable, strDomainQry, strProjectName, strDELIVERYID, strDomainFields)
            objExecSql.InsertRecsToDataBase(strInsertSQL)
            blnDynTable = False

        End If
    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles LblDataSourceType.Click

    End Sub

    Private Sub CmbDataSourceType_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CmbDataSourceType.SelectedIndexChanged


        If CmbDataSourceType.SelectedItem = "SQL Server" Then
            Txt_InitialCatalog.Enabled = True
        Else

            Txt_InitialCatalog.Enabled = False

        End If

    End Sub

    Private Sub btnExcelToDB_Click(sender As Object, e As EventArgs) Handles btnExcelToDB.Click
        objExecSql.oraSourceConn = oraSourceConn
        objExecSql.oraTargetConn = oraTargetConn
        objExecSql.SqlSourceConn = SqlSourceConn
        objExecSql.SqlTargetConn = SqlTargetConn
        objExecSql.ExportExcelToDB()
    End Sub
End Class