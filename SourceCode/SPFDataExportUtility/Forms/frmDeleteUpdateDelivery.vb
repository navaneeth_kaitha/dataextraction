﻿Imports Oracle.DataAccess.Client

Public Class frmDeleteUpdateDelivery
    Private Sub dgVwDelivery_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgVwDelivery.CellClick
        Try

            If e.ColumnIndex > -1 Then
                If dgVwDelivery.Rows.Count > 0 Then
                    If dgVwDelivery.Columns(e.ColumnIndex).Name = "btnDelete" Then

                        Dim result As DialogResult = MessageBox.Show("Confirm Delete?",
                              "Delete Delivery",
                              MessageBoxButtons.YesNo)

                        If (result = DialogResult.Yes) Then
                            Dim intSelRow As Integer = 0
                            Dim strDelName As String = ""
                            intSelRow = e.RowIndex
                            strDelName = dgVwDelivery.Rows(intSelRow).Cells("DELIVERY").Value
                            Dim cmdDel As New OracleCommand
                            cmdDel.Connection = oraTargetConn
                            cmdDel.CommandText = "Delete from ZDelivery where DELIVERYNAME='" & strDelName & "'"
                            cmdDel.ExecuteNonQuery()
                            dgVwDelivery.DataSource = Nothing
                            PopulateDeliveryList()
                        Else
                            Exit Sub
                        End If

                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete Delivery")
        Finally

        End Try
    End Sub

    Private Sub frmDeleteUpdateDelivery_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        With dgVwDelivery.ColumnHeadersDefaultCellStyle
            .BackColor = Color.Navy
            .ForeColor = Color.White
            .Font = New Font(dgVwDelivery.Font, FontStyle.Bold)
        End With
        Try
            PopulateDeliveryList()
            dgVwDelivery.Columns("Description").Width = 180
            dgVwDelivery.Columns("btnDelete").Width = 74
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Delete Delivery")
        End Try


    End Sub
    Sub PopulateDeliveryList()
        Try


            Dim cmdDlv As New OracleCommand
            Dim adaDlv As New OracleDataAdapter
            Dim dtDlv As New DataTable
            cmdDlv.Connection = oraTargetConn
            cmdDlv.CommandText = "select PROJID as ""Project Id"",DELIVERYNAME as ""Delivery"",DELIVERYDESC as ""Description"",SPFDBUSER as ""DB User"",SPFDBCONNECTION as ""DB Connection""from ZDelivery"
            adaDlv.SelectCommand = cmdDlv
            adaDlv.Fill(dtDlv)
            dgVwDelivery.DataSource = dtDlv
            'Adding button on each row
            Dim dgDelBtnCol As New DataGridViewButtonColumn

            dgDelBtnCol.Width = 60
            dgVwDelivery.Columns.Add(dgDelBtnCol)

            dgDelBtnCol.Name = "btnDelete"
            dgDelBtnCol.HeaderText = "Click to delete"
            dgDelBtnCol.Text = "Delete"
            dgDelBtnCol.UseColumnTextForButtonValue = True
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class