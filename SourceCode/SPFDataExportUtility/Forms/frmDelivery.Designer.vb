﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDelivery
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.txtSPFDBConnection = New System.Windows.Forms.TextBox()
        Me.txtSPFDBPwd = New System.Windows.Forms.TextBox()
        Me.txtSPFDBUser = New System.Windows.Forms.TextBox()
        Me.lblSPFConnection = New System.Windows.Forms.Label()
        Me.lblSPFPwd = New System.Windows.Forms.Label()
        Me.lblSPFUser = New System.Windows.Forms.Label()
        Me.lblProjectCode = New System.Windows.Forms.Label()
        Me.lblDelivery = New System.Windows.Forms.Label()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.cboProject = New System.Windows.Forms.ComboBox()
        Me.lblDeliveryDesc = New System.Windows.Forms.Label()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtDeliveryDesc = New System.Windows.Forms.TextBox()
        Me.txtDeliveryName = New System.Windows.Forms.TextBox()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.txtSPFDBInitalCatalog = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(367, 8)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(49, 23)
        Me.btnSearch.TabIndex = 42
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        Me.btnSearch.Visible = False
        '
        'txtSPFDBConnection
        '
        Me.txtSPFDBConnection.Location = New System.Drawing.Point(131, 180)
        Me.txtSPFDBConnection.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSPFDBConnection.Name = "txtSPFDBConnection"
        Me.txtSPFDBConnection.Size = New System.Drawing.Size(289, 20)
        Me.txtSPFDBConnection.TabIndex = 41
        '
        'txtSPFDBPwd
        '
        Me.txtSPFDBPwd.Location = New System.Drawing.Point(131, 146)
        Me.txtSPFDBPwd.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSPFDBPwd.Name = "txtSPFDBPwd"
        Me.txtSPFDBPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtSPFDBPwd.Size = New System.Drawing.Size(289, 20)
        Me.txtSPFDBPwd.TabIndex = 40
        '
        'txtSPFDBUser
        '
        Me.txtSPFDBUser.Location = New System.Drawing.Point(131, 112)
        Me.txtSPFDBUser.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSPFDBUser.Name = "txtSPFDBUser"
        Me.txtSPFDBUser.Size = New System.Drawing.Size(289, 20)
        Me.txtSPFDBUser.TabIndex = 39
        '
        'lblSPFConnection
        '
        Me.lblSPFConnection.AutoSize = True
        Me.lblSPFConnection.Location = New System.Drawing.Point(7, 185)
        Me.lblSPFConnection.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSPFConnection.Name = "lblSPFConnection"
        Me.lblSPFConnection.Size = New System.Drawing.Size(82, 13)
        Me.lblSPFConnection.TabIndex = 38
        Me.lblSPFConnection.Text = "SPF  DB Server"
        '
        'lblSPFPwd
        '
        Me.lblSPFPwd.AutoSize = True
        Me.lblSPFPwd.Location = New System.Drawing.Point(8, 148)
        Me.lblSPFPwd.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSPFPwd.Name = "lblSPFPwd"
        Me.lblSPFPwd.Size = New System.Drawing.Size(76, 13)
        Me.lblSPFPwd.TabIndex = 37
        Me.lblSPFPwd.Text = "SPF Password"
        '
        'lblSPFUser
        '
        Me.lblSPFUser.AutoSize = True
        Me.lblSPFUser.Location = New System.Drawing.Point(9, 117)
        Me.lblSPFUser.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblSPFUser.Name = "lblSPFUser"
        Me.lblSPFUser.Size = New System.Drawing.Size(52, 13)
        Me.lblSPFUser.TabIndex = 36
        Me.lblSPFUser.Text = "SPF User"
        '
        'lblProjectCode
        '
        Me.lblProjectCode.AutoSize = True
        Me.lblProjectCode.Location = New System.Drawing.Point(10, 18)
        Me.lblProjectCode.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblProjectCode.Name = "lblProjectCode"
        Me.lblProjectCode.Size = New System.Drawing.Size(68, 13)
        Me.lblProjectCode.TabIndex = 34
        Me.lblProjectCode.Text = "Project Code"
        '
        'lblDelivery
        '
        Me.lblDelivery.AutoSize = True
        Me.lblDelivery.Location = New System.Drawing.Point(10, 48)
        Me.lblDelivery.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDelivery.Name = "lblDelivery"
        Me.lblDelivery.Size = New System.Drawing.Size(45, 13)
        Me.lblDelivery.TabIndex = 33
        Me.lblDelivery.Text = "Delivery"
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(378, 292)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(47, 23)
        Me.btnClose.TabIndex = 32
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(360, 242)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(52, 23)
        Me.btnSave.TabIndex = 31
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'cboProject
        '
        Me.cboProject.FormattingEnabled = True
        Me.cboProject.Location = New System.Drawing.Point(131, 15)
        Me.cboProject.Name = "cboProject"
        Me.cboProject.Size = New System.Drawing.Size(121, 21)
        Me.cboProject.TabIndex = 45
        '
        'lblDeliveryDesc
        '
        Me.lblDeliveryDesc.AutoSize = True
        Me.lblDeliveryDesc.Location = New System.Drawing.Point(2, 73)
        Me.lblDeliveryDesc.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDeliveryDesc.Name = "lblDeliveryDesc"
        Me.lblDeliveryDesc.Size = New System.Drawing.Size(63, 13)
        Me.lblDeliveryDesc.TabIndex = 35
        Me.lblDeliveryDesc.Text = " Description"
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtSPFDBInitalCatalog)
        Me.Panel1.Controls.Add(Me.btnSearch)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.lblDeliveryDesc)
        Me.Panel1.Location = New System.Drawing.Point(3, 4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(422, 282)
        Me.Panel1.TabIndex = 48
        '
        'txtDeliveryDesc
        '
        Me.txtDeliveryDesc.Location = New System.Drawing.Point(131, 78)
        Me.txtDeliveryDesc.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDeliveryDesc.Name = "txtDeliveryDesc"
        Me.txtDeliveryDesc.Size = New System.Drawing.Size(289, 20)
        Me.txtDeliveryDesc.TabIndex = 50
        '
        'txtDeliveryName
        '
        Me.txtDeliveryName.Location = New System.Drawing.Point(131, 45)
        Me.txtDeliveryName.Margin = New System.Windows.Forms.Padding(2)
        Me.txtDeliveryName.Name = "txtDeliveryName"
        Me.txtDeliveryName.Size = New System.Drawing.Size(289, 20)
        Me.txtDeliveryName.TabIndex = 49
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(1, 292)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(54, 23)
        Me.btnDelete.TabIndex = 51
        Me.btnDelete.Text = "Delivery List"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'txtSPFDBInitalCatalog
        '
        Me.txtSPFDBInitalCatalog.Location = New System.Drawing.Point(129, 212)
        Me.txtSPFDBInitalCatalog.Margin = New System.Windows.Forms.Padding(2)
        Me.txtSPFDBInitalCatalog.Name = "txtSPFDBInitalCatalog"
        Me.txtSPFDBInitalCatalog.Size = New System.Drawing.Size(287, 20)
        Me.txtSPFDBInitalCatalog.TabIndex = 53
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(5, 217)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(116, 13)
        Me.Label1.TabIndex = 52
        Me.Label1.Text = "SPF SQL Initial catalog"
        '
        'frmDelivery
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(437, 327)
        Me.Controls.Add(Me.btnDelete)
        Me.Controls.Add(Me.txtDeliveryDesc)
        Me.Controls.Add(Me.txtDeliveryName)
        Me.Controls.Add(Me.cboProject)
        Me.Controls.Add(Me.txtSPFDBConnection)
        Me.Controls.Add(Me.txtSPFDBPwd)
        Me.Controls.Add(Me.txtSPFDBUser)
        Me.Controls.Add(Me.lblSPFConnection)
        Me.Controls.Add(Me.lblSPFPwd)
        Me.Controls.Add(Me.lblSPFUser)
        Me.Controls.Add(Me.lblProjectCode)
        Me.Controls.Add(Me.lblDelivery)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "frmDelivery"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Delivery"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSearch As Button
    Friend WithEvents txtSPFDBConnection As TextBox
    Friend WithEvents txtSPFDBPwd As TextBox
    Friend WithEvents txtSPFDBUser As TextBox
    Friend WithEvents lblSPFConnection As Label
    Friend WithEvents lblSPFPwd As Label
    Friend WithEvents lblSPFUser As Label
    Friend WithEvents lblProjectCode As Label
    Friend WithEvents lblDelivery As Label
    Friend WithEvents btnClose As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents cboProject As ComboBox
    Friend WithEvents lblDeliveryDesc As Label
    Friend WithEvents Panel1 As Panel
    Friend WithEvents txtDeliveryDesc As TextBox
    Friend WithEvents txtDeliveryName As TextBox
    Friend WithEvents btnDelete As Button
    Friend WithEvents txtSPFDBInitalCatalog As TextBox
    Friend WithEvents Label1 As Label
End Class
