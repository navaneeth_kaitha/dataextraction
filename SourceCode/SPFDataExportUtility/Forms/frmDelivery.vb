﻿Imports System.Data.SqlClient
Imports Oracle.DataAccess.Client
Public Class frmDelivery
    Private Sub PopullateProjects()
        'Dim cmd As New OracleCommand
        'Dim adaProj As New OracleDataAdapter
        'Dim dtProj As New DataTable
        ''Dim dr As OracleDataReader

        'Dim DS As New DataSet
        'cmd.Connection = oraTargetConn
        'cmd.CommandText = "select PROJID,PROJNAME from zprojects  "
        'adaProj.SelectCommand = cmd
        'adaProj.Fill(dtProj)
        'cboProject.DisplayMember = "PROJNAME"
        'cboProject.ValueMember = "PROJID"
        'cboProject.DataSource = dtProj


        Dim cmdOrcl As New OracleCommand
        Dim cmdSql As New SqlCommand
        Dim adaProjOrcl As New OracleDataAdapter
        Dim adaProjSql As New SqlDataAdapter
        Dim dtProj As New DataTable
        'Dim dr As OracleDataReader

        Dim DS As New DataSet
        If Not IsNothing(oraTargetConn) Then
            cmdOrcl.Connection = oraTargetConn
            cmdOrcl.CommandText = "select PROJID,PROJNAME from zprojects "
            adaProjOrcl.SelectCommand = cmdOrcl
            adaProjOrcl.Fill(dtProj)
        Else
            cmdSql.Connection = SqlTargetConn
            cmdSql.CommandText = "select PROJID,PROJNAME from zprojects "
            adaProjSql.SelectCommand = cmdSql
            adaProjSql.Fill(dtProj)
        End If

        cboProject.DisplayMember = "PROJNAME"
        cboProject.ValueMember = "PROJID"
        cboProject.DataSource = dtProj



    End Sub
    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, Color.LightSteelBlue, ButtonBorderStyle.Solid)
    End Sub
    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim cmdTargetOrcl As New OracleCommand
        Dim cmdTargetSql As New SqlCommand
        Dim strProjectCode As String = ""
        strProjectCode = cboProject.SelectedValue
        If txtDeliveryName.Text = "" Then
            MessageBox.Show("Delivery Name is manadatory")
            Exit Sub
        ElseIf txtSPFDBUser.Text = "" Then
            MessageBox.Show("DB user is manadatory")
            Exit Sub
        ElseIf txtSPFDBPwd.Text = "" Then
            MessageBox.Show("DB Password is manadatory")
            Exit Sub
        ElseIf txtSPFDBConnection.Text = "" Then
            MessageBox.Show("DB Connection is manadatory")
            Exit Sub

        End If
        Dim strPwd As String = ""
        strPwd = objCls.EncryptPassword(txtSPFDBPwd.Text)
        If Not IsNothing(oraTargetConn) Then
            oraTargetConn.Open()
            cmdTargetOrcl.Connection = oraTargetConn
            ' txtSPFDBConnection.Text = "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 192.168.13.147)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = SPF19C)))"
            cmdTargetOrcl.CommandText = "Insert into ZDelivery (PROJID,DELIVERYNAME,DELIVERYDESC,SPFDBUSER,SPFDBPASSWORD,SPFDBCONNECTION,SPFDBInitalCatalog) " &
                " values('" & strProjectCode & "','" & txtDeliveryName.Text & "','" & txtDeliveryDesc.Text & "','" & txtSPFDBUser.Text & "','" & strPwd & "','" & txtSPFDBConnection.Text & "','" & txtSPFDBInitalCatalog.Text & "')"

            cmdTargetOrcl.ExecuteNonQuery()
        Else
            cmdTargetSql.Connection = SqlTargetConn
            cmdTargetSql.CommandText = "Insert into ZDelivery (PROJID,DELIVERYNAME,DELIVERYDESC,SPFDBUSER,SPFDBPASSWORD,SPFDBCONNECTION,SPFDBInitalCatalog) " &
                " values('" & strProjectCode & "','" & txtDeliveryName.Text & "','" & txtDeliveryDesc.Text & "','" & txtSPFDBUser.Text & "','" & strPwd & "','" & txtSPFDBConnection.Text & "','" & txtSPFDBInitalCatalog.Text & "')"

            cmdTargetSql.ExecuteNonQuery()
        End If


        MessageBox.Show("Delivery Created.")
        txtDeliveryName.Text = ""
        txtDeliveryDesc.Text = ""
        txtSPFDBUser.Text = ""
        txtSPFDBConnection.Text = ""
        txtSPFDBPwd.Text = ""
        txtSPFDBInitalCatalog.Text = ""

    End Sub

    Private Sub frmDelivery_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        PopullateProjects()
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim frmDelivery As New frmDeleteUpdateDelivery
        frmDelivery.Show()
    End Sub

    Private Sub lblSPFUser_Click(sender As Object, e As EventArgs) Handles lblSPFUser.Click

    End Sub
End Class