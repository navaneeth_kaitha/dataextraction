﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmProject
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtProjectCode = New System.Windows.Forms.TextBox()
        Me.txtProjectName = New System.Windows.Forms.TextBox()
        Me.txtProjectDesc = New System.Windows.Forms.TextBox()
        Me.btnClose = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.lblProjectName = New System.Windows.Forms.Label()
        Me.lblProjectCode = New System.Windows.Forms.Label()
        Me.lbllblProjectDesc = New System.Windows.Forms.Label()
        Me.btnSearch = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SuspendLayout()
        '
        'txtProjectCode
        '
        Me.txtProjectCode.Location = New System.Drawing.Point(135, 14)
        Me.txtProjectCode.Margin = New System.Windows.Forms.Padding(2)
        Me.txtProjectCode.Name = "txtProjectCode"
        Me.txtProjectCode.Size = New System.Drawing.Size(288, 20)
        Me.txtProjectCode.TabIndex = 0
        '
        'txtProjectName
        '
        Me.txtProjectName.Location = New System.Drawing.Point(135, 46)
        Me.txtProjectName.Margin = New System.Windows.Forms.Padding(2)
        Me.txtProjectName.Name = "txtProjectName"
        Me.txtProjectName.Size = New System.Drawing.Size(288, 20)
        Me.txtProjectName.TabIndex = 1
        '
        'txtProjectDesc
        '
        Me.txtProjectDesc.Location = New System.Drawing.Point(135, 79)
        Me.txtProjectDesc.Margin = New System.Windows.Forms.Padding(2)
        Me.txtProjectDesc.Name = "txtProjectDesc"
        Me.txtProjectDesc.Size = New System.Drawing.Size(288, 20)
        Me.txtProjectDesc.TabIndex = 2
        '
        'btnClose
        '
        Me.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.btnClose.Location = New System.Drawing.Point(361, 111)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(60, 23)
        Me.btnClose.TabIndex = 14
        Me.btnClose.Text = "Close"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(294, 111)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 23)
        Me.btnSave.TabIndex = 13
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'lblProjectName
        '
        Me.lblProjectName.AutoSize = True
        Me.lblProjectName.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblProjectName.Location = New System.Drawing.Point(9, 51)
        Me.lblProjectName.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblProjectName.Name = "lblProjectName"
        Me.lblProjectName.Size = New System.Drawing.Size(71, 13)
        Me.lblProjectName.TabIndex = 15
        Me.lblProjectName.Text = "Project Name"
        '
        'lblProjectCode
        '
        Me.lblProjectCode.AutoSize = True
        Me.lblProjectCode.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lblProjectCode.Location = New System.Drawing.Point(9, 19)
        Me.lblProjectCode.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblProjectCode.Name = "lblProjectCode"
        Me.lblProjectCode.Size = New System.Drawing.Size(68, 13)
        Me.lblProjectCode.TabIndex = 16
        Me.lblProjectCode.Text = "Project Code"
        '
        'lbllblProjectDesc
        '
        Me.lbllblProjectDesc.AutoSize = True
        Me.lbllblProjectDesc.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.lbllblProjectDesc.Location = New System.Drawing.Point(10, 83)
        Me.lbllblProjectDesc.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lbllblProjectDesc.Name = "lbllblProjectDesc"
        Me.lbllblProjectDesc.Size = New System.Drawing.Size(96, 13)
        Me.lbllblProjectDesc.TabIndex = 17
        Me.lbllblProjectDesc.Text = "Project Description"
        '
        'btnSearch
        '
        Me.btnSearch.Location = New System.Drawing.Point(225, 111)
        Me.btnSearch.Name = "btnSearch"
        Me.btnSearch.Size = New System.Drawing.Size(60, 23)
        Me.btnSearch.TabIndex = 24
        Me.btnSearch.Text = "Search"
        Me.btnSearch.UseVisualStyleBackColor = True
        Me.btnSearch.Visible = False
        '
        'Panel1
        '
        Me.Panel1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Panel1.Location = New System.Drawing.Point(4, 5)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(431, 134)
        Me.Panel1.TabIndex = 25
        '
        'frmProject
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HighlightText
        Me.ClientSize = New System.Drawing.Size(438, 142)
        Me.Controls.Add(Me.btnSearch)
        Me.Controls.Add(Me.lbllblProjectDesc)
        Me.Controls.Add(Me.lblProjectCode)
        Me.Controls.Add(Me.lblProjectName)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtProjectDesc)
        Me.Controls.Add(Me.txtProjectName)
        Me.Controls.Add(Me.txtProjectCode)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "frmProject"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Project"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtProjectCode As TextBox
    Friend WithEvents txtProjectName As TextBox
    Friend WithEvents txtProjectDesc As TextBox
    Friend WithEvents btnClose As Button
    Friend WithEvents btnSave As Button
    Friend WithEvents lblProjectName As Label
    Friend WithEvents lblProjectCode As Label
    Friend WithEvents lbllblProjectDesc As Label
    Friend WithEvents btnSearch As Button
    Friend WithEvents Panel1 As Panel
End Class
