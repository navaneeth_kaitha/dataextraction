﻿Imports System.Data.SqlClient
Imports Demo
Imports Oracle.DataAccess.Client
Imports Oracle.ManagedDataAccess.Client

Public Class frmProject
    Dim objCls As New clsCommon
    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub

    Private Sub Label5_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtProjectCode.Text = "" Then
            MessageBox.Show("Project Code Name is manadatory")
            Exit Sub
        ElseIf txtProjectName.Text = "" Then
            MessageBox.Show("Project Name is manadatory")
            Exit Sub
        End If
        SaveData()
        Me.Close()
    End Sub

    Sub SaveData()
        Dim cmdTargetOrcl As New OracleCommand
        Dim cmdTargetSql As New SqlCommand
        Dim strIsSPFConnection As String = "False"
        Dim strPlantName As String = ""
        Dim strSPFServer As String = ""

        If Not IsNothing(oraTargetConn) Then
            oraTargetConn.Open()
            cmdTargetOrcl.Connection = oraTargetConn
            cmdTargetOrcl.CommandText = "Insert into ZPROJECTS (PROJID,PROJNAME,PROJDESC) " &
                " values('" & txtProjectCode.Text & "','" & txtProjectName.Text & "','" & txtProjectDesc.Text & "')"
            cmdTargetOrcl.ExecuteNonQuery()
        Else
            cmdTargetSql.Connection = SqlTargetConn
            cmdTargetSql.CommandText = "Insert into ZPROJECTS (PROJID,PROJNAME,PROJDESC) " &
                " values('" & txtProjectCode.Text & "','" & txtProjectName.Text & "','" & txtProjectDesc.Text & "')"
            cmdTargetSql.ExecuteNonQuery()
        End If


        MessageBox.Show("Project Created")
        txtProjectCode.Text = ""
        txtProjectName.Text = ""
        txtProjectDesc.Text = ""
    End Sub

    Private Sub frmProjects_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        ControlPaint.DrawBorder(e.Graphics, e.ClipRectangle, Color.LightSteelBlue, ButtonBorderStyle.Solid)
    End Sub
End Class