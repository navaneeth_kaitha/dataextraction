﻿Imports System.Text
Imports Demo
'Imports Oracle.ManagedDataAccess.Client
Imports Oracle.DataAccess.Client
Imports SPF.Client.Administration
Imports SPFDataExportUtility.SPF.Client.APIs

Public Class ReadDataFromOperationEdms
    Dim objExecSql As New ExecuteSQLs
    Public objCls As New clsCommon
    Public Sub GetEquipsFromOperationEdms(pstrEquipments As StringBuilder, lobjSPERequest As SimpleRequest)

        Dim lobjServerReq As SimpleRequest

        Dim lobjNodeList As Xml.XmlNodeList
        ' lobjServerReq = objSPFSession.CreateRequest("ZADGetDocTagInfo")
        'lobjSPERequest.ServerAPI = "ZADGetDocStatus"

        lobjServerReq = lobjSPERequest
        lobjServerReq.ServerAPI = "ZADGetDocTagInfo"
        lobjSPERequest.UserName = "superuser" ' Environment.UserName
        lobjServerReq.AddQueryElement("EqpToFind", pstrEquipments.ToString)

        lobjServerReq.Execute()
        lobjNodeList = lobjServerReq.Response.SelectNodes("//OperationalEDMSItemsFound")

        ' lobjServerReq.AddQueryElement("EqpToFind", pstrEquipments.ToString)
        '  lobjServerReq.Execute()
        ' lobjNodeList = lobjServerReq.Response.SelectNodes("//OperationalEDMSItemsFound")
        AddTagToDatabble(lobjNodeList, "Eqp")
        'Dim lobjResultantEquipmentDataTable As DataTable = AddTagToDatabble(lobjNodeList, "Eqp")
        ' objExecSql.InsertDataTableToOracle(lobjResultantEquipmentDataTable)
    End Sub

    Public Function AddTagToDatabble_Original(pobjNodeList As Xml.XmlNodeList, strType As String) As DataTable
        Dim lojEquipmentData As DataTable = Nothing
        If strType = "Ins" Then
            lojEquipmentData = New DataTable("OPEDMS_INSTRUMENT")
        ElseIf strType = "Eqp" Then
            lojEquipmentData = New DataTable("OPEDMS_EQUIPMENT")
        ElseIf strType = "Line" Then
            lojEquipmentData = New DataTable("OPEDMS_LINE")
        End If
        lojEquipmentData.Columns.Add("Tag_Name")
        lojEquipmentData.Columns.Add("TagDescription")
        lojEquipmentData.Columns.Add("TagType")
        lojEquipmentData.Columns.Add("TagTypeDesc")
        '' Dim lcol = (From collection In lobjNodeList.AsParallel() Select collection.OuterXml.attribute("Name").Values).ToList
        Dim lobjEquips As Xml.XmlNode
        For Each lobjEquips In pobjNodeList
            Dim lobjnewRow = lojEquipmentData.NewRow()
            lobjnewRow("Tag_Name") = lobjEquips.Attributes("Name").Value.ToString()
            lobjnewRow("TagDescription") = lobjEquips.Attributes("Description").Value.ToString()
            lobjnewRow("TagType") = lobjEquips.Attributes("TagType").Value.ToString()
            lobjnewRow("TagTypeDesc") = lobjEquips.Attributes("TagTypeDesc").Value.ToString()
            lojEquipmentData.Rows.Add(lobjnewRow)
        Next
        Return lojEquipmentData
    End Function
    Public Sub AddTagToDatabble(pobjNodeList As Xml.XmlNodeList, strType As String)
        Dim lojEquipmentData As DataTable = Nothing
        Dim strTable As String = ""
        If strType = "Ins" Then
            lojEquipmentData = New DataTable("OPEDMS_INSTRUMENT")
            strTable = "OPEDMS_INSTRUMENT"
        ElseIf strType = "Eqp" Then
            lojEquipmentData = New DataTable("OPEDMS_EQUIPMENT")
            strTable = "OPEDMS_EQUIPMENT"
        ElseIf strType = "Line" Then
            lojEquipmentData = New DataTable("OPEDMS_LINE")
            strTable = "OPEDMS_LINE"
        End If
        'lojEquipmentData.Columns.Add("Tag_Name")
        'lojEquipmentData.Columns.Add("TagDescription")
        'lojEquipmentData.Columns.Add("TagType")
        'lojEquipmentData.Columns.Add("TagTypeDesc")
        OPEDMSdataCols(lojEquipmentData)
        '' Dim lcol = (From collection In lobjNodeList.AsParallel() Select collection.OuterXml.attribute("Name").Values).ToList
        Dim lobjEquips As Xml.XmlNode
        Dim intRow As Integer = 0
        Dim iBatchUpdate As Double

        Dim objTargetAdapter As OracleDataAdapter
        Dim objTargetDS As New DataSet
        Dim cmdTarget As New OracleCommand

        cmdTarget.Connection = oraTargetConn

        objTargetAdapter = New OracleDataAdapter
        cmdTarget.CommandText = "select * from " & strTable
        objTargetAdapter.SelectCommand = cmdTarget
        Dim cmdTargetBldr As OracleCommandBuilder
        For Each lobjEquips In pobjNodeList
            Dim lobjnewRow = lojEquipmentData.NewRow()
            If Not IsNothing(lobjEquips.Attributes("Name")) Then
                lobjnewRow("Tag_Name") = lobjEquips.Attributes("Name").Value.ToString()
            End If
            If Not IsNothing(lobjEquips.Attributes("Description")) Then
                lobjnewRow("TagDescription") = lobjEquips.Attributes("Description").Value.ToString()
            End If

            If Not IsNothing(lobjEquips.Attributes("TagType")) Then
                lobjnewRow("TagType") = lobjEquips.Attributes("TagType").Value.ToString()
            End If
            If Not IsNothing(lobjEquips.Attributes("TagTypeDesc")) Then
                lobjnewRow("TagTypeDesc") = lobjEquips.Attributes("TagTypeDesc").Value.ToString()
            End If

            lojEquipmentData.Rows.Add(lobjnewRow)

            intRow = intRow + 1
            iBatchUpdate = iBatchUpdate + 1


            If iBatchUpdate = 1000000 Then

                objTargetDS.Tables.Add(lojEquipmentData)

                objTargetAdapter.Fill(objTargetDS, strTable)
                cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

                objTargetAdapter.Update(objTargetDS, strTable)
                objTargetDS.AcceptChanges()
                objTargetDS.Tables.Remove(lojEquipmentData)
                lojEquipmentData = New DataTable(strTable)
                OPEDMSdataCols(lojEquipmentData)
                iBatchUpdate = 0


            End If
        Next

        If iBatchUpdate > 0 Then
            objTargetDS.Tables.Add(lojEquipmentData)
            objTargetAdapter.Fill(objTargetDS, strTable)
            cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

            objTargetAdapter.Update(objTargetDS, strTable)
            objTargetDS.AcceptChanges()
        End If
        '  Return lojEquipmentData
    End Sub


    Private Sub OPEDMSDocumenyCols(ByRef dt As DataTable)
        'Tag_Name
        Dim colDocName As New DataColumn
        colDocName.DataType = GetType(String)
        colDocName.ColumnName = "DOCNAME"
        dt.Columns.Add(colDocName)

        'DOCDESCRIPTION
        Dim colDocDesc As New DataColumn
        colDocDesc.DataType = GetType(String)
        colDocDesc.ColumnName = "DOCDESCRIPTION"
        dt.Columns.Add(colDocDesc)



        'SPFDOCUMENTCATEGORY
        Dim colDocCat As New DataColumn
        colDocCat.DataType = GetType(String)
        colDocCat.ColumnName = "SPFDOCUMENTCATEGORY"
        dt.Columns.Add(colDocCat)


        'SPFDOCTYPE
        Dim colDocType As New DataColumn
        colDocType.DataType = GetType(String)
        colDocType.ColumnName = "SPFDOCTYPE"
        dt.Columns.Add(colDocType)


        'SPFDOCSUBTYPE
        Dim colSubDocType As New DataColumn
        colSubDocType.DataType = GetType(String)
        colSubDocType.ColumnName = "SPFDOCSUBTYPE"
        dt.Columns.Add(colSubDocType)


        'SPFEXTERNALREVISION
        Dim colSPFEXTREV As New DataColumn
        colSPFEXTREV.DataType = GetType(String)
        colSPFEXTREV.ColumnName = "SPFEXTERNALREVISION"
        dt.Columns.Add(colSPFEXTREV)


        'DELIVERYID
        Dim colDocDELIVERYID As New DataColumn
        colDocDELIVERYID.DataType = GetType(String)
        colDocDELIVERYID.ColumnName = "DELIVERYID"
        dt.Columns.Add(colDocDELIVERYID)

        'PROJECTNAME
        Dim colPROJECTNAME As New DataColumn
        colPROJECTNAME.DataType = GetType(String)
        colPROJECTNAME.ColumnName = "PROJECTNAME"
        dt.Columns.Add(colPROJECTNAME)

        'PROJECTNAME
        Dim colZADDATESTAMP As New DataColumn
        colZADDATESTAMP.DataType = GetType(Date)
        colZADDATESTAMP.ColumnName = "ZADDATESTAMP"
        dt.Columns.Add(colZADDATESTAMP)

    End Sub
    Private Sub OPEDMSdataCols(ByRef dt As DataTable)
        'Tag_Name
        Dim colDocName As New DataColumn
        colDocName.DataType = GetType(String)
        colDocName.ColumnName = "Tag_Name"
        dt.Columns.Add(colDocName)

        'TagDescription
        Dim colDocDesc As New DataColumn
        colDocDesc.DataType = GetType(String)
        colDocDesc.ColumnName = "TagDescription"
        dt.Columns.Add(colDocDesc)

        'TagType
        Dim colDocRev As New DataColumn
        colDocRev.DataType = GetType(String)
        colDocRev.ColumnName = "TagType"
        dt.Columns.Add(colDocRev)

        'TagTypeDesc
        Dim colDocType As New DataColumn
        colDocType.DataType = GetType(String)
        colDocType.ColumnName = "TagTypeDesc"
        dt.Columns.Add(colDocType)


    End Sub
    Public Function AddDocsToDatable_Original(pobjNodeList As Xml.XmlNodeList) As DataTable
        Dim lojEquipmentData = New DataTable("OPEDMS_DOCUMENT")
        lojEquipmentData.Columns.Add("DocName")
        lojEquipmentData.Columns.Add("DocDescription")
        lojEquipmentData.Columns.Add("SPFExternalRevision")
        lojEquipmentData.Columns.Add("SPFDocumentCategory")
        lojEquipmentData.Columns.Add("SPFDocType")
        lojEquipmentData.Columns.Add("SPFDocSubType")
        '' Dim lcol = (From collection In lobjNodeList.AsParallel() Select collection.OuterXml.attribute("Name").Values).ToList
        Dim lobjEquips As Xml.XmlNode
        For Each lobjEquips In pobjNodeList
            Dim lobjnewRow = lojEquipmentData.NewRow()
            lobjnewRow("DocName") = lobjEquips.Attributes("Name").Value.ToString()
            lobjnewRow("DocDescription") = lobjEquips.Attributes("Description").Value.ToString()
            lobjnewRow("SPFExternalRevision") = lobjEquips.Attributes("SPFExternalRevision").Value.ToString()
            lobjnewRow("SPFDocumentCategory") = lobjEquips.Attributes("SPFDocumentCategory").Value.ToString()
            lobjnewRow("SPFDocType") = lobjEquips.Attributes("SPFDocType").Value.ToString()
            lobjnewRow("SPFDocSubType") = lobjEquips.Attributes("SPFDocSubType").Value.ToString()
            lojEquipmentData.Rows.Add(lobjnewRow)
        Next

        Return lojEquipmentData
    End Function


    Public Function AddDocsToDatable(pobjNodeList As Xml.XmlNodeList) As DataTable

        Dim strTable As String = "OPEDMS_DOCUMENT"
        Dim intRow As Integer = 0
        Dim iBatchUpdate As Double

        Dim objTargetAdapter As OracleDataAdapter
        Dim objTargetDS As New DataSet
        Dim cmdTarget As New OracleCommand

        cmdTarget.Connection = oraTargetConn

        objTargetAdapter = New OracleDataAdapter
        cmdTarget.CommandText = "select * from " & strTable
        objTargetAdapter.SelectCommand = cmdTarget
        Dim cmdTargetBldr As OracleCommandBuilder

        Dim lojEquipmentData = New DataTable("OPEDMS_DOCUMENT")
        OPEDMSDocumenyCols(lojEquipmentData)

        ' Dim intRow As Integer = 0
        '  Dim iBatchUpdate As Double

        'lojEquipmentData.Columns.Add("DocName")
        'lojEquipmentData.Columns.Add("DocDescription")
        'lojEquipmentData.Columns.Add("SPFExternalRevision")
        'lojEquipmentData.Columns.Add("SPFDocumentCategory")
        'lojEquipmentData.Columns.Add("SPFDocType")
        'lojEquipmentData.Columns.Add("SPFDocSubType")
        '  OPEDMSDocdataCols()
        '' Dim lcol = (From collection In lobjNodeList.AsParallel() Select collection.OuterXml.attribute("Name").Values).ToList
        Dim lobjEquips As Xml.XmlNode
        For Each lobjEquips In pobjNodeList
            Dim lobjnewRow = lojEquipmentData.NewRow()
            lobjnewRow("DocName") = lobjEquips.Attributes("Name").Value.ToString()
            lobjnewRow("DocDescription") = lobjEquips.Attributes("Description").Value.ToString()
            lobjnewRow("SPFExternalRevision") = lobjEquips.Attributes("SPFExternalRevision").Value.ToString()
            lobjnewRow("SPFDocumentCategory") = lobjEquips.Attributes("SPFDocumentCategory").Value.ToString()
            lobjnewRow("SPFDocType") = lobjEquips.Attributes("SPFDocType").Value.ToString()
            lobjnewRow("SPFDocSubType") = lobjEquips.Attributes("SPFDocSubType").Value.ToString()
            lojEquipmentData.Rows.Add(lobjnewRow)
            intRow = intRow + 1
            iBatchUpdate = iBatchUpdate + 1


            If iBatchUpdate = 1000000 Then

                objTargetDS.Tables.Add(lojEquipmentData)

                objTargetAdapter.Fill(objTargetDS, strTable)
                cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

                objTargetAdapter.Update(objTargetDS, strTable)
                objTargetDS.AcceptChanges()
                objTargetDS.Tables.Remove(lojEquipmentData)
                lojEquipmentData = New DataTable(strTable)
                OPEDMSDocumenyCols(lojEquipmentData)
                iBatchUpdate = 0


            End If
        Next

        If iBatchUpdate > 0 Then
            objTargetDS.Tables.Add(lojEquipmentData)
            objTargetAdapter.Fill(objTargetDS, strTable)
            cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

            objTargetAdapter.Update(objTargetDS, strTable)
            objTargetDS.AcceptChanges()
        End If

        Return lojEquipmentData
    End Function
    Public Sub GetInstrsFromOperationEdms(pstrInstruments As StringBuilder, lobjSPERequest As SimpleRequest)

        Dim lobjServerReq As SimpleRequest
        Dim lobjNodeList As Xml.XmlNodeList


        lobjServerReq = lobjSPERequest
        lobjServerReq.ServerAPI = "ZADGetDocTagInfo"
        lobjSPERequest.UserName = "superuser" ' Environment.UserName
        lobjServerReq.AddQueryElement("InsToFind", pstrInstruments.ToString)
        lobjServerReq.Execute()
        lobjNodeList = lobjServerReq.Response.SelectNodes("//OperationalEDMSItemsFound")
        AddTagToDatabble(lobjNodeList, "Ins")
        '  Dim lobjResultantInstrtDataTable As DataTable = AddTagToDatabble(lobjNodeList, "Ins")
        '   objExecSql.InsertDataTableToOracle(lobjResultantInstrtDataTable)
    End Sub
    Public Sub GetDocsFromOperationEdms(pstrInstruments As StringBuilder, lobjSPERequest As SimpleRequest)
        '  Dim objSPFSession = login()
        Dim lobjServerReq As SimpleRequest
        Dim lobjNodeList As Xml.XmlNodeList

        lobjServerReq = lobjSPERequest
        lobjServerReq.ServerAPI = "ZADGetDocTagInfo"
        lobjSPERequest.UserName = "superuser" ' Environment.UserName

        lobjServerReq.AddQueryElement("DocsToFind", pstrInstruments.ToString)
        lobjServerReq.Execute()
        lobjNodeList = lobjServerReq.Response.SelectNodes("//OperationalEDMSItemsFound")
        'Dim lobjResulatantDocTable As DataTable = AddDocsToDatable(lobjNodeList)
        'objExecSql.InsertDataTableToOracle(lobjResulatantDocTable)
        AddDocsToDatable(lobjNodeList)
    End Sub
    Public Sub GetLinesFromOperationEdms(pstrLines As StringBuilder, lobjSPERequest As SimpleRequest)
        '  Dim objSPFSession = login()
        Dim lobjServerReq As SimpleRequest
        Dim lobjNodeList As Xml.XmlNodeList

        lobjServerReq = lobjSPERequest
        lobjServerReq.ServerAPI = "ZADGetDocTagInfo"
        lobjSPERequest.UserName = "superuser"

        lobjServerReq.AddQueryElement("LinesToFind", pstrLines.ToString)
        lobjServerReq.Execute()
        lobjNodeList = lobjServerReq.Response.SelectNodes("//OperationalEDMSItemsFound")
        '   Dim lobjResultantLinesDataTable As DataTable = AddTagToDatabble(lobjNodeList, "Line")
        '  objExecSql.InsertDataTableToOracle(lobjResultantLinesDataTable)
        AddTagToDatabble(lobjNodeList, "Line")
    End Sub

    Public Function login(strProject As String) As SimpleRequest


        Dim strSPFSessionID As String
        Dim strSpfServerName As String = "" '"VMSPFAPP10"
        Dim strSpfWebDirectory As String = "" '"ZADPSServer"
        Dim strSpfUserName As String = "" ' Environment.UserName '"superuser1"
        Dim strSpfPassword As String = ""
        Dim lstrServerName As String = System.Configuration.ConfigurationManager.AppSettings.Get("OEDMSServerName")
        Dim lstrWebDirectory As String = System.Configuration.ConfigurationManager.AppSettings.Get("OEDMSWebDirectory")
        Dim Plant As String = System.Configuration.ConfigurationManager.AppSettings.Get("Plant")
        Dim Port As String = System.Configuration.ConfigurationManager.AppSettings.Get("Port")
        Dim IsSecure As String = System.Configuration.ConfigurationManager.AppSettings.Get("IsSecure")
        strSpfUserName = "" '"superuser"
        Dim lobjCustomLogin As ZADCOSPFLogin = New ZADCOSPFLogin(lstrServerName, lstrWebDirectory, Port, strSpfUserName, "", IsSecure)

        strSPFSessionID = lobjCustomLogin.GetSessionID(strSpfUserName, Plant, strProject)
        ' strSpfUserName = "superuser"

        Dim mstrSecure As String = CStr(IIf(IsSecure, "https://", "http://"))
        Dim mstrSPEURL As String = mstrSecure & lstrServerName & "/" & lstrWebDirectory & "/ServerRequest.asmx"
        Dim lobjSPERequest As New SimpleRequest(mstrSPEURL, strSPFSessionID)
        Return lobjSPERequest
        'lobjSPERequest.ServerAPI = "ZADGetDocStatus"
    End Function

End Class
