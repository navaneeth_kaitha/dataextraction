﻿Imports System.Data.OleDb
Imports Oracle.DataAccess.Client
'Imports Oracle.ManagedDataAccess.Client

Public Class ReadDocTypesvsTagTypeExcel
    Public Sub LoadExcel()
        Dim szFilePath As String = "D:\DocTypesVsTagTypes-All1.xlsx"
        Dim szConnectionString As String = "Provider=Microsoft.ACE.OLEDB.12.0; Data Source='" + szFilePath + "';Extended Properties='Excel 12.0;HDR=YES';"
        Dim cmd As OleDbDataAdapter
        Dim ds As New DataSet()
        Dim cn As OleDbConnection
        cn = New System.Data.OleDb.OleDbConnection(szConnectionString)
        cmd = New System.Data.OleDb.OleDbDataAdapter("select * from [01.Eq Types$]", cn)
        cn.Open()
        cmd.Fill(ds, "EquipmentTable")
        cmd = New System.Data.OleDb.OleDbDataAdapter("select * from [02.Instr Type$]", cn)
        cmd.Fill(ds, "InstrumentTable")
        cn.Close()
        ReadDatafromExcel(ds)
    End Sub

    Public Sub ReadDatafromExcel(pobjDataset As DataSet)
        Dim lobjEquipmentDataTable As New DataTable("TAGTYPEVSDOCTYPE")
        Dim lobInstrumentDataTable As New DataTable("TAGTYPEVSDOCTYPE")

        lobjEquipmentDataTable.Columns.Add("TagType")


        lobjEquipmentDataTable.Columns.Add("TagTypeDesc")

        lobjEquipmentDataTable.Columns.Add("DocType")
        lobjEquipmentDataTable.Columns.Add("DocSubType")
        lobjEquipmentDataTable.Columns.Add("IsManadatory")

        lobInstrumentDataTable.Columns.Add("Tag_Name")
        lobInstrumentDataTable.Columns.Add("TagType")
        lobInstrumentDataTable.Columns.Add("TagTypeDesc")
        lobInstrumentDataTable.Columns.Add("DocType")
        lobInstrumentDataTable.Columns.Add("DocSubType")
        lobInstrumentDataTable.Columns.Add("IsManadatory")


        Dim lobjListofInstrTpes As New List(Of InstrumentType)
        Dim lobjListofEquipTpes As New List(Of EquipmentTpe)
        Dim lobjEquipTable As DataTable = pobjDataset.Tables(0)
        Dim lobjInstrTable As DataTable = pobjDataset.Tables(1)
        Dim lintColumCount As Int64 = lobjInstrTable.Columns.Count
        Dim lintrowsCount As Int64 = lobjInstrTable.Rows.Count
        Dim colindex As Integer = 0
        For index As Int64 = 2 To lintrowsCount - 3
            Dim lobjInstrumentType As New InstrumentType
            Dim InstrumentType As String = lobjInstrTable.Rows(index).Item(0)
            Dim InstrumentTypeDesc As String = lobjInstrTable.Rows(index).Item(1)

            Dim lobj As New List(Of DocumentType)
            For colindex = 4 To lintColumCount - 1
                If lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper() = "Y" Or lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper() = "X" Or lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper() = "O" Then
                    Dim row As DataRow = lobInstrumentDataTable.NewRow
                    Dim lobjDocumentType As New DocumentType
                    lobjDocumentType.DocType = lobjInstrTable.Columns(colindex).ToString
                    lobjDocumentType.DocSubType = lobjInstrTable.Rows(0).Item(colindex)
                    lobjDocumentType.isMandatory = lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper()
                    row("TagType") = InstrumentType
                    row("TagTypeDesc") = InstrumentTypeDesc
                    row("DocType") = lobjDocumentType.DocType
                    row("DocSubType") = lobjDocumentType.DocSubType
                    row("IsManadatory") = lobjDocumentType.isMandatory
                    lobInstrumentDataTable.Rows.Add(row)

                End If

            Next

        Next



        lintColumCount = lobjEquipTable.Columns.Count
        lintrowsCount = lobjEquipTable.Rows.Count
        colindex = 0
        For index As Int64 = 2 To lintrowsCount - 3
            Dim lobjequipType As New EquipmentTpe
            Dim EquipType As String = lobjEquipTable.Rows(index).Item(2)
            Dim EquipTypeDesc As String = lobjEquipTable.Rows(index).Item(3)

            Dim lobj As New List(Of DocumentType)
            For colindex = 5 To lintColumCount - 6
                If lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper() = "Y" Or lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper() = "X" Or lobjInstrTable.Rows(index).Item(colindex).ToString.ToUpper() = "O" Or lobjInstrTable.Rows(index).Item(colindex).ToString = "ü" Then
                    Dim row As DataRow = lobInstrumentDataTable.NewRow
                    Dim lobjDocumentType As New DocumentType
                    lobjDocumentType.DocType = lobjEquipTable.Columns(colindex).ToString
                    lobjDocumentType.DocSubType = lobjEquipTable.Rows(0).Item(colindex)
                    lobjDocumentType.isMandatory = lobjEquipTable.Rows(index).Item(colindex).ToString.ToUpper()
                    row("TagType") = EquipType
                    row("TagTypeDesc") = EquipTypeDesc
                    row("DocType") = lobjDocumentType.DocType
                    row("DocSubType") = lobjDocumentType.DocSubType
                    If lobjInstrTable.Rows(index).Item(colindex).ToString = "ü" Then
                        row("IsManadatory") = "O"
                    Else
                        row("IsManadatory") = lobjDocumentType.isMandatory
                    End If

                    lobInstrumentDataTable.Rows.Add(row)
                End If
            Next

        Next
        InsertDatatoDB(lobjEquipmentDataTable, lobInstrumentDataTable)
    End Sub
    Public Sub InsertDatatoDB(lobjEquipmentDataTable As DataTable, lobInstrumentDataTable As DataTable)
        Dim objTarget As New DataSet
        Dim cmdTarget As New OracleCommand
        Dim objTargetAdapter As New OracleDataAdapter
        cmdTarget.Connection = oraTargetConn
        Dim objTargetDS As New DataSet
        Dim cmdTargetBldr As OracleCommandBuilder


        cmdTargetBldr = New OracleCommandBuilder(objTargetAdapter)

        objTargetAdapter.Update(lobInstrumentDataTable)

        objTargetDS.AcceptChanges()


    End Sub
End Class

Public Class DocumentType
    Public DocType As String
    Public DocSubType As String
    Public isMandatory As String
End Class
Public Class EquipmentTpe
    Public EquipType As String
    Public EquipTypeDesc As String
    Public DocumentTypeCollection As List(Of DocumentType)
End Class
Public Class InstrumentType
    Public InsType As String
    Public InsTypeDesc As String
    Public DocumentTypeCollection As List(Of DocumentType)
End Class
