﻿Imports System.Xml
Imports SPF
Imports SPF.Utilities
Public Class ZADCOSPFLogin
#Region "Members"

    Private mstrSPEURL As String
    Private mstrSPESessionID As String
    Private mstrHost As String
    Private mstrServer As String
    Private mstrSecure As String
    Private mstrPort As String
    Private mstrUserName As String
    Private mstrFromPC As String

#End Region
    Public Sub New(pstrHost As String, pstrServer As String, pstrPort As String, pstrUserName As String, pstrFromPC As String, Optional pblnSecure As Boolean = True)

        mstrHost = pstrHost
        mstrServer = pstrServer
        mstrSecure = CStr(IIf(pblnSecure, "https://", "http://"))
        mstrPort = pstrPort
        mstrHost = mstrHost & ":" & mstrPort
        mstrUserName = pstrUserName
        mstrFromPC = pstrFromPC

    End Sub
    Public Function RedirectionURL(pstrName As String, pstrClassClue As String) As String

        Dim lstrRoles As New List(Of String)
        GetSessionID("", "", "")
        Dim lobjSPERequest As New SimpleRequest(mstrSPEURL, mstrSPESessionID)

        lobjSPERequest.ServerAPI = "ZADSDABridge"
        With lstrRoles.GetEnumerator
            While .MoveNext
                lobjSPERequest.AddRole(.Current)
            End While
        End With

        lobjSPERequest.UserName = mstrUserName  ' mstrUserName ' Environment.UserName '"superuser" ' HARDCODE FOR EASE OF ACCESS CoreModule.LoginUser.SPFLoginName
        lobjSPERequest.AddQueryElement("TagFromOtherSystem", pstrName)
        lobjSPERequest.AddQueryElement("ClassClue", pstrClassClue)
        lobjSPERequest.Execute()

        Return lobjSPERequest.Response.SelectSingleNode("//ZADRedirectionURL").InnerText

    End Function


    Public Function GetSessionID(pstrUserName As String, pstrPlantName As String, pstrProjectName As String) As String 'pstrLoginUser As String, pstrClinetLocale As String, pstrHost As String) As String

        Try
            mstrSPEURL = mstrSecure & mstrHost & "/" & mstrServer & "/ServerRequest.asmx" '"http://ZADOPX/ZDOP/ServerRequest.asmx" '"https://WSPF6HQ/OpEDMSServer/ServerRequest.asmx"
            Dim lobjSPERequest As New SimpleRequest(mstrSPEURL)
            Dim pdomQuery As XmlDocument = lobjSPERequest.Request
            pdomQuery.RemoveAll()
            Dim node As XmlNode = pdomQuery.AppendChild(pdomQuery.CreateElement("Query"))
            Dim element As XmlElement = DirectCast(node.AppendChild(pdomQuery.CreateElement("SessionInfo")), XmlElement)
            element.AppendChild(pdomQuery.CreateElement("OrigMethodName")).InnerText = "::"
            element.AppendChild(pdomQuery.CreateElement("CalledFrom")).InnerText = "VBCLIENT" ' Environment.UserName ' "vwspf52dev" 'Me.CalledFrom
            element.AppendChild(pdomQuery.CreateElement("CalledFromDialog")).InnerText = ""
            element.AppendChild(pdomQuery.CreateElement("ClientLocale")).InnerText = "en-US" ' CoreModule.Request.SelectSingleNode("//ClientLocale").InnerText ' CultureInfo.CurrentCulture.Name
            node.AppendChild(pdomQuery.CreateElement("Method")).InnerText = "ValidateUser"
            node.AppendChild(pdomQuery.CreateElement("UserName")).InnerText = pstrUserName ' mstrUserName '"spftest" 'Environment.UserName 'CoreModule.LoginUser.SPFLoginName
            node.AppendChild(pdomQuery.CreateElement("Password")).InnerText = "" 'SecurityUtilities.CreateHashFromPassword(CoreModule.LoginUser.SPFPassword)
            node.AppendChild(pdomQuery.CreateElement("HostName")).InnerText = Environment.MachineName ' mstrFromPC ' Environment.MachineName  ' "vwspf52dev" 'Me.HostName
            node.AppendChild(pdomQuery.CreateElement("NTUserName")).InnerText = ""
            node.AppendChild(pdomQuery.CreateElement("PlantName")).InnerText = pstrPlantName
            node.AppendChild(pdomQuery.CreateElement("ProjectName")).InnerText = pstrProjectName
            node.AppendChild(pdomQuery.CreateElement("ForceAutoLogin")).InnerText = "TRUE"
            node.AppendChild(pdomQuery.CreateElement("b_use_token")).InnerText = "1"

            lobjSPERequest.Execute()
            mstrSPESessionID = lobjSPERequest.Response.SelectSingleNode("//SessionID").InnerText
            'With lobjSPERequest.Response.SelectNodes("//SPFRole").GetEnumerator
            '    While .MoveNext
            '        Dim lobjEachRole As XmlNode = CType(.Current, XmlNode)
            '        pstrRoles.Add(lobjEachRole.SelectSingleNode(".//IObject").Attributes("UID").Value)
            '    End While
            'End With
        Catch ex As Exception
            'MessageBox.Show("Failed to reserve session. Please contact EDMS Administrator :: " & vbNewLine & ex.Message & vbNewLine & ex.StackTrace)
        End Try
        Return mstrSPESessionID

    End Function
End Class
