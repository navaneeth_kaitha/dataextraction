﻿Option Explicit On
'Option Strict On
Imports SPF

Imports SPF.Client




Imports System.Xml
Imports System.Xml.XPath
Imports System.IO
Imports System.Windows.Forms
Imports SPF.Client.Administration

Namespace SPF.Client.APIs
    Public Class ZADSPFLogin
        Inherits SPFWindowsClient

#Region " Members "

#End Region

#Region " Properties "

#End Region

        Public Sub New()
            MyBase.New()


        End Sub
        Public Function LogIn(pstrSpfServerName As String, pstrSpfWebDirectory As String, pstrSpfUserName As String, pstrSpfPassword As String, pstrPlant As String, strPrj As String) As SPFSession
            Try

                Dim pobjSPFWindowsFormsClient As New SPFWindowsFormsClient

                CType(pobjSPFWindowsFormsClient.SPFConnection, System.ComponentModel.ISupportInitialize).BeginInit()
                pobjSPFWindowsFormsClient.SPFConnection.CalledFrom = "VBClient"
                pobjSPFWindowsFormsClient.SPFConnection.Sessions = Nothing
                CType(pobjSPFWindowsFormsClient.SPFConnection, System.ComponentModel.ISupportInitialize).EndInit()
                Dim srvIdx As Integer = pobjSPFWindowsFormsClient.SPFConnection.Servers.Add(pstrSpfServerName & ":" & pstrSpfWebDirectory, pstrSpfServerName, pstrSpfWebDirectory)
                pobjSPFWindowsFormsClient.SPFConnection.Servers.SelectedIndex = srvIdx

                Try
                    Me.SPFSession = pobjSPFWindowsFormsClient.SPFConnection.Login(pstrSpfUserName, pstrSpfPassword, pstrPlant, strPrj, "", "")
                    Me.SPFSession.IgnoreConfig = False

                    Return Me.SPFSession
                Catch lobjException As Exception

                    While lobjException.InnerException IsNot Nothing
                        lobjException = lobjException.InnerException
                    End While

                    Throw New Exception(lobjException.Message, lobjException)
                End Try

            Catch lobjException As Exception
                Throw New Exception(lobjException.Message, lobjException)
            End Try
        End Function
    End Class
End Namespace


'Public Class AtheebLogin
'    Public Function LogIn(ByVal pstrSpfServerName As String, ByVal pstrSpfWebDirectory As String, ByVal pstrSpfUserName As String, ByVal pstrSpfPassword As String, ByVal pstrPlant As String) As SPFSession
'        Dim lobjCustomLogin As ZADSPFLogin = New ZADSPFLogin()
'        Return lobjCustomLogin.LogIn(pstrSpfServerName, pstrSpfWebDirectory, pstrSpfUserName, pstrSpfPassword, pstrPlant)
'    End Function
'End Class

