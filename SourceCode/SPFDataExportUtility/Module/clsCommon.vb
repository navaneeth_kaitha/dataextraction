﻿Imports System.Xml

Imports System.IO
Imports System.IO.File
Imports System

Imports System.Security.Cryptography
Imports System.Text
Imports Oracle.DataAccess.Client
Imports System.Data.SqlClient

Public Class clsCommonDelete



    Public Function OpenOracleConnection(strDataSource As String, strUser As String, strPwd As String) As OracleConnection
        Dim strOraDB As String
        Dim oraCon As OracleConnection = Nothing
        strOraDB = "data source= " & strDataSource & "; user id= " & strUser & ";password = " & strPwd & ";"

        oraCon = New OracleConnection(strOraDB)

        Try
            oraCon.Open()

            Return oraCon
        Catch ex As Exception
            Throw ex

            If oraCon.State = ConnectionState.Open Then
                oraCon.Close()
            End If

        Finally

        End Try
        Return oraCon
    End Function
    Public Function OpenSqlConnection(strDataSource As String, strInitialCatalog As String, strUser As String, strPwd As String) As SqlConnection
        Dim strsqlDB As String
        Dim sqlConn As SqlConnection = Nothing


        strsqlDB = "Data Source=" & strDataSource & ";Initial Catalog=" & strInitialCatalog & ";Persist Security Info=True;User ID=" & strUser & ";Password=" & strPwd & "; MultipleActiveResultSets=True;"


        sqlConn = New SqlConnection(strsqlDB)

        Try
            sqlConn.Open()

            Return sqlConn
        Catch ex As Exception
            Throw ex

            If sqlConn.State = ConnectionState.Open Then
                sqlConn.Close()
            End If

        Finally

        End Try
        Return sqlConn
    End Function
    Public Sub WriteLog(strLog As String)
        Dim swDataExportLog As StreamWriter = AppendText(gstrFileName)
        swDataExportLog.WriteLine(Now() & " " & strLog)
        swDataExportLog.Close()
    End Sub

    Public Function EncryptPassword(ByVal pstrPassword As String) As String
        Dim clearBytes As Byte() = Encoding.Unicode.GetBytes(pstrPassword)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(mstrKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write)
                    cs.Write(clearBytes, 0, clearBytes.Length)
                    cs.Close()
                End Using
                pstrPassword = Convert.ToBase64String(ms.ToArray())
            End Using
        End Using
        Return pstrPassword
    End Function

    Public Function DecryptPassword(ByVal pstrPassword As String) As String
        Dim cipherBytes As Byte() = Convert.FromBase64String(pstrPassword)
        Using encryptor As Aes = Aes.Create()
            Dim pdb As New Rfc2898DeriveBytes(mstrKey, New Byte() {&H49, &H76, &H61, &H6E, &H20, &H4D, &H65, &H64, &H76, &H65, &H64, &H65, &H76})
            encryptor.Key = pdb.GetBytes(32)
            encryptor.IV = pdb.GetBytes(16)
            Using ms As New MemoryStream()
                Using cs As New CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write)
                    cs.Write(cipherBytes, 0, cipherBytes.Length)
                    cs.Close()
                End Using
                pstrPassword = Encoding.Unicode.GetString(ms.ToArray())
            End Using
        End Using
        Return pstrPassword
    End Function

    Public Function FetchSPFAPIConnections(strSPFSystemName As String, ByRef strSPFServer As String, ByRef strSPFVirtualDirectory As String) As Boolean
        Dim xmlDoc As New XmlDocument()
        Dim blnRet As Boolean = False
        Dim strSPFSystem As String

        xmlDoc.Load(gstrSPFConfig) '
        Dim nodes As XmlNodeList = xmlDoc.DocumentElement.SelectNodes("/SPFSystems/SPFSystem")
        For Each node As XmlNode In nodes
            strSPFSystem = node.SelectSingleNode("Name").InnerText
            If strSPFSystem = strSPFSystemName Then
                strSPFServer = node.SelectSingleNode("SpfServerName").InnerText
                strSPFVirtualDirectory = node.SelectSingleNode("SpfWebDirectory").InnerText
                blnRet = True
                Return blnRet
            End If
        Next
        Return blnRet
    End Function
End Class
